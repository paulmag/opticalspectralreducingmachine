.. OpticalSpectralReducingMachine documentation master file, created by
   sphinx-quickstart on Wed Nov 19 16:53:47 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OpticalSpectralReducingMachine's documentation!
==========================================================

Contents:

.. toctree::
   :maxdepth: 2

   tutorial



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

