The static calibration files contain the atmospheric extinction for several observatories.
The first column gives the wavelength in nanometers, and the secon column kappa, the extinction per airmass in mag/airmass

New atmospheric extinction correction tables can easily be added following the same format.
They must be named after the observatory's name, as defined by the <earth-location> tag of the instrument configuration XML file. For example, GTC is located in La Palma. The corresponding observatory's name in the XML file is "earth-location-lapalma", and the static calibration file containing the atmospheric extinction table is called "lapalma_extinction.csv".
SPEXtractor will scan the static calibration folder and automatically select the appropriate file.
If no file is found for a given observatory, no extinction correction is applied even if the <apply-atmospheric-extinction> is set to "yes".

Please send us your extinction correction tables!
