# README #

## What is this? ##

A star spectrum analysis tool written in Python that is being developed for the ESAC student trainee project "A Coherent Optical Spectral Library of T-Tauri stars".

## Content ##

* A module for each step in the process of spectrum reduction from raw data
* A main program that uses all the modules with the input of an XML-file
* A sample XML-file that shows what kind of input the program expects
