import numpy as np
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter1d, median_filter
from scipy.integrate import quad
import matplotlib.pyplot as plt

from astropy.io import fits

from Functions import moffat, gauss_m, polynomial, tmedian, treshold, BIC, get_best_polymodel



class Reduction:

    UNITS = "FWHM", "pixels"


    def __init__(self, filename="filename not given", axis_spec=None):
        self.weights = None
        self.filename = filename
        if axis_spec is not None:
            self.axis_spec = axis_spec
        if self.axis_spec not in (0, 1):
            raise ValueError(
                "<dispaxis> must be '0' or '1'. "
                "Which way is the spectrum trace oriented?"
            )


    def set_image(self, image, smooth_mode="median", smooth_size=20):
        """
        Expects a 2D np.array representing the image of a spectrum line.
        Automatically smooths image for further use.
        image: (float, 2d-array)
        smooth_mode: (string) Can be "median" or "gauss".
        smooth_size: (int) Size of the smoothing filter. Becomes sigma if
            gaussian filter is used.
        """

        if self.axis_spec == 1:
            image = image.transpose()
        self.image = image

        if smooth_mode in ("median"):
            self.image_smooth = median_filter(
                image,
                size=(smooth_size, 1),
                mode="nearest",
            )
        elif smooth_mode in ("gauss", "gaussian"):
            self.image_smooth = gaussian_filter1d(
                image,
                sigma=smooth_size,
                axis=0,
                mode="nearest",
                truncate=3.0,
            )
        else:
            # No smoothing.
            self.image_smooth = image

        self.N = np.shape(self.image) # (y,x)
        self.pix_vals = (
            np.linspace(0, self.N[0]-1, self.N[0]).astype(int),
            np.linspace(0, self.N[1]-1, self.N[1]).astype(int),
        )


    def set_weights(self, weights):
        if self.axis_spec == 1:
            weights = weights.transpose()
        self.weights = weights


    def set_aperture_radius(self, radius, unit="FWHM"):
        self.aperture_radius = radius
        if unit in self.UNITS:
            self.aperture_unit = unit
        else:
            raise TypeError(
                "'%s' is an unknown unit. Expected one of %s."
                % (unit, str(self.UNITS))
            )


    def set_sky_radius(self,
        (radius_in, radius_out),
        (unit_in, unit_out) = ("FWHM", "FWHM"),
    ):
        self.sky_radius = radius_in, radius_out
        if unit_in in self.UNITS and unit_in in self.UNITS:
            self.sky_unit = unit_in, unit_out
        else:
            raise TypeError(
                "'%s' and/or '%s' is an unknown unit. Expected one of %s."
                % (unit_in, unit_out, str(self.UNITS))
            )


    def show_fit(self, y, smooth=False):
        """
        Plots a single slice of the image orthogonal to the spectrum line,
        with the fitted moffat or gaussian curve on top and the residuals
        plotted below and also the trimmed median for comparison.
        This should be useful for determining how good a fit has been and to
        manually detect artifacts which may have influenced the fit.

        If this method is called several times you can only view one figure at
        a time.
        """
        if smooth:
            row = self.image_smooth[y,:]
        else:
            row = self.image[y,:]
        p0 = [np.max(row), np.argmax(row), 2, 1, np.median(row), 0]# initial guess
        moffat_opt, p_cov = curve_fit(
            moffat,
            self.pix_vals[1],
            row,
            p0=p0,
        )

        # Make the linear atmosphere/background part of the fit to the box
        # regions only:
        atmosphere = np.hstack([
            row[
                np.rint(moffat_opt[1] - self.sky_radius[1] * moffat_opt[2]) :
                np.rint(moffat_opt[1] - self.sky_radius[0] * moffat_opt[2])
            ],
            row[
                np.rint(moffat_opt[1] + self.sky_radius[0] * moffat_opt[2]) :
                np.rint(moffat_opt[1] + self.sky_radius[1] * moffat_opt[2])
            ],
        ])
        x_vals = np.hstack([
            self.pix_vals[1][
                np.rint(moffat_opt[1] - self.sky_radius[1] * moffat_opt[2]) :
                np.rint(moffat_opt[1] - self.sky_radius[0] * moffat_opt[2])
            ],
            self.pix_vals[1][
                np.rint(moffat_opt[1] + self.sky_radius[0] * moffat_opt[2]) :
                np.rint(moffat_opt[1] + self.sky_radius[1] * moffat_opt[2])
            ],
        ])
        p_opt_poly = np.polyfit(x_vals, atmosphere, 1)

        # Make datasets for the fits:
        row_tmedian = tmedian(row)
        row_moffat  = moffat(
            self.pix_vals[1],
            moffat_opt[0],      moffat_opt[1],
            moffat_opt[2],      moffat_opt[3],
            p_opt_poly[1], p_opt_poly[0],
        )

        # Make figure:
        fig = plt.figure()
        ax = [fig.add_subplot(2,1,1), fig.add_subplot(2,1,2)]

        ax_size = 0.875,
        ax[0].set_position([0.075, 0.55, ax_size[0], 0.40])
        ax[1].set_position([0.075, 0.04, ax_size[0], 0.40])

        ax[0].set_title("Image slice with fit, y=%d" % y)
        ax[1].set_title("Residuals between data and fit")

        # Data and fit plots:
        ax[0].plot(   row,         color="k", label="flux")
        ax[0].axhline(row_tmedian, color="r", label="tmedian")
        ax[0].plot(   row_moffat,  color="c", label="moffat fit")
        ax[0].legend()

        # Residual plots:
        ax[1].axhline(0,              color="k", label="ideal(0)")
        ax[1].plot(row - row_tmedian, color="r", label="tmedian")
        ax[1].plot(row - row_moffat,  color="c", label="moffat fit")
        ax[1].legend()

        plt.show()


    def locate_spectrum(self, position_spec=None, method="both", show=False):
        """Extract the spectrum from the image with these steps:
        1. Bellfit (moffat/gauss)
        2. Polyfit (just position)
        3. Refined bellfit
        4. Sanity check
        position_spec: (float) A predefined rough position of the spectrum
            line. If None, the max intensity of each row will be used as an
            initial guess instead.
        show: (bool) If True, plot some information about the progress.
        """

        if method in ("both", "all", "fit", "bellfit"):
            method = ("gauss", "moffat")
        elif method in "box":
            method = "moffat"

        if self.weights is None:
            self.weights = np.ones(self.image.shape)
        self.mesh_vals = np.meshgrid(self.pix_vals[1], self.pix_vals[0])
            # pixel value grid

        BIC_total_gauss  = 0.
        BIC_total_moffat = 0.

        ### First guess:
        if "moffat" in method:
            moffat_opt = np.zeros((self.N[0], 6))
            moffat_error = np.zeros((self.N[0], 6))
        if "gauss" in method:
            gauss_opt = np.zeros((self.N[0], 5))
            gauss_error = np.zeros((self.N[0], 5))
        if position_spec is None:
            if "moffat" in method:
                moffat_opt[:,1] = np.argmax(self.image, axis=1)
            if "gauss" in method:
                gauss_opt[:,1] = np.argmax(self.image, axis=1)
        else: # use argmax if position of spectrum not given
            if "moffat" in method:
                moffat_opt[:,1] = position_spec          # position of peak
            if "gauss" in method:
                gauss_opt[:,1] = position_spec
        if "moffat" in method:
            moffat_opt[:,2] = 2                          # HWHM
            moffat_opt[:,3] = 1                          # Moffat index
            moffat_opt[:,4] = np.median(self.image, axis=1) # constant (atmosphere)
            moffat_opt[:,5] = 0 # "sloping" of atmosphere
            moffat_opt[:,0] = np.max(self.image, axis=1) - moffat_opt[:,4]
                # amplitude of peak
        if "gauss" in method:
            gauss_opt[:,2] = 2                          # HWHM
            gauss_opt[:,3] = np.median(self.image, axis=1)
                # constant (atmosphere)
            gauss_opt[:,4] = 0 # "sloping" of atmosphere
            gauss_opt[:,0] = np.max(self.image, axis=1) - moffat_opt[:,4]
                # amplitude of peak

        ### Find first fit:
        fit_radius = 200 #TODO: Don't know what is ideal/most efficient.
        range_spec = np.rint(np.array([
            # moffat_opt1[:,1] - fit_radius,
            # moffat_opt1[:,1] + fit_radius,
            position_spec - fit_radius,
            position_spec + fit_radius,
        ])).astype(int)
        range_spec = treshold(range_spec, low=0, high=self.N[1]-1)

        if "gauss" in method:
            no_fit_mask = []
            # Fit gaussian to each row (each wavelength).
            for i in range(self.N[0]):
                try:
                    gauss_opt[i], p_cov = curve_fit(
                        gauss_m,
                        self.pix_vals[1][range_spec[0]:range_spec[1]],
                        self.image_smooth[i, range_spec[0]:range_spec[1]],
                        p0 = gauss_opt[i],
                    )
                    if np.isinf(p_cov).all():
                        # gauss_error[i, :] = np.inf
                        no_fit_mask.append(i)
                    else:
                        gauss_error[i] = np.diag(p_cov)
                except RuntimeError:
                    print "RuntimeError occured at i =", i
                    no_fit_mask.append(i)
                except TypeError:
                    print "TypeError occured at i =", i
                    no_fit_mask.append(i)
            gauss_error[no_fit_mask] = np.inf
            gauss_uncertainty = gauss_opt[:,1].copy()
            gauss_uncertainty[no_fit_mask] = np.inf
            gauss_opt[:,2] = np.abs(gauss_opt[:,2]) # HWHM must be positive
            gauss_opt = self.fix_bad_fit_points(gauss_opt)

        if "moffat" in method:
            no_fit_mask = []
            # Fit moffat to each row (each wavelength).
            for i in range(self.N[0]):
                try:
                    moffat_opt[i], p_cov = curve_fit(
                        moffat,
                        self.pix_vals[1][range_spec[0]:range_spec[1]],
                        self.image_smooth[i, range_spec[0]:range_spec[1]],
                        p0 = moffat_opt[i],
                    )
                    if np.isinf(p_cov).all():
                        # moffat_error[i, :] = np.inf
                        no_fit_mask.append(i)
                    else:
                        moffat_error[i] = np.diag(p_cov)
                except RuntimeError:
                    print "RuntimeError occured at i =", i
                    no_fit_mask.append(i)
                except TypeError:
                    print "TypeError occured at i =", i
                    no_fit_mask.append(i)
            moffat_error[no_fit_mask] = np.inf
            moffat_uncertainty = moffat_opt[:,1].copy()
            moffat_uncertainty[no_fit_mask] = np.inf
            moffat_opt[:,2] = np.abs(moffat_opt[:,2]) # std must be positive
            moffat_opt = self.fix_bad_fit_points(moffat_opt)

        ### Polyfit:
        #TODO It just uses moffat if both gauss and moffat is in method. That
        # is ok for now.

        if "moffat" in method:
            # Filter:
            pos_opt_filtered = median_filter(
                moffat_opt[:,1],
                size=25,
                mode="nearest",
            )
            hwhm_opt_filtered = median_filter(
                moffat_opt[:,2],
                size=25,
                mode="nearest",
            )
            mi_opt_filtered = median_filter(
                moffat_opt[:,3],
                size=25,
                mode="nearest",
            )
            # Fit:
            p_opt_pospoly = get_best_polymodel(
                self.pix_vals[0],
                pos_opt_filtered,
                yerror = moffat_uncertainty,
            )
            p_opt_hwhmpoly = get_best_polymodel(
                self.pix_vals[0],
                hwhm_opt_filtered,
                yerror = moffat_uncertainty,
            )
            p_opt_mipoly = get_best_polymodel(
                self.pix_vals[0],
                mi_opt_filtered,
                yerror = moffat_uncertainty,
            )

        elif "gauss" in method:
            # Filter:
            pos_opt_filtered = median_filter(
                gauss_opt[:,1],
                size=25,
                mode="nearest",
            )
            hwhm_opt_filtered = median_filter(
                gauss_opt[:,2],
                size=25,
                mode="nearest",
            )
            mi_opt_filtered = median_filter(
                gauss_opt[:,3],
                size=25,
                mode="nearest",
            )
            # Fit:
            p_opt_pospoly = get_best_polymodel(
                self.pix_vals[0],
                pos_opt_filtered,
                yerror = gauss_uncertainty,
            )
            p_opt_hwhmpoly = get_best_polymodel(
                self.pix_vals[0],
                hwhm_opt_filtered,
                yerror = gauss_uncertainty,
            )
            p_opt_mipoly = get_best_polymodel(
                self.pix_vals[0],
                mi_opt_filtered,
                yerror = gauss_uncertainty,
            )

        # Make curves:
        pospoly  = polynomial(self.pix_vals[0], p_opt_pospoly)
        hwhmpoly = polynomial(self.pix_vals[0], p_opt_hwhmpoly)
        mipoly   = polynomial(self.pix_vals[0], p_opt_mipoly)
        # TODO: Should probably use RANSAC here instead:
        hwhmpoly = np.median(hwhm_opt_filtered) * np.ones(hwhm_opt_filtered.size)
        mipoly   = np.median(mi_opt_filtered) * np.ones(mi_opt_filtered.size)

        ### Display:
        if show:
            fig = plt.figure()
            fig.suptitle("'%s'" % self.filename, size=18)
            ax1 = fig.add_subplot(3, 1, 1)
            ax1.set_title("Position")
            ax1.plot(self.pix_vals[0], moffat_opt[:, 1], "r+")
            ax1.plot(self.pix_vals[0], pos_opt_filtered, "b+")
            ax1.plot(self.pix_vals[0], pospoly, "g-")
            ax1 = fig.add_subplot(3, 1, 2)
            ax1.set_title("FWHM")
            ax1.plot(self.pix_vals[0], 2 * moffat_opt[:, 2], "r+")
            ax1.plot(self.pix_vals[0], 2 * hwhm_opt_filtered, "b+")
            ax1.plot(self.pix_vals[0], 2 * hwhmpoly, "g-")
                # 2 * HWHM = FWHM
            ax1 = fig.add_subplot(3, 1, 3)
            ax1.set_title("Moffat Index")
            ax1.plot(self.pix_vals[0], moffat_opt[:, 3], "r+")
            ax1.plot(self.pix_vals[0], mi_opt_filtered, "b+")
            ax1.plot(self.pix_vals[0], mipoly, "g-")
            plt.show()

        ### Find second fit:
        if "moffat" in method:
            moffat_opt1 = moffat_opt.copy()
            moffat_opt1 = np.hstack((moffat_opt1[:, :1], moffat_opt1[:, 2:]))
        if "gauss" in method:
            gauss_opt1 = gauss_opt.copy()
            gauss_opt1 = np.hstack((gauss_opt1[:, :1], gauss_opt1[:, 2:]))
        if self.sky_unit[1] == "FWHM":
            radius_out = 0.5 * self.sky_radius[1] * hwhmpoly
            fit_radius = treshold(
                radius_out,
                low = 20,
                high = fit_radius,
            )
            # Defines the radius for the fit to work on with a minimum to avoid
            # TypeError in curve_fit and a maximum since the spectrum should not
            # be that large anyway, so a too large area will only increase the
            # risk of being biased by something else.
        elif self.sky_unit[1] == "pixels":
            radius_out = self.sky_radius[1]
            fit_radius = radius_out
        range_spec = np.rint(np.array([
            pospoly - fit_radius,
            pospoly + fit_radius,
        ])).astype(int)
        range_spec = treshold(range_spec, low=0, high=self.N[1]-1)
            # Keep range within the boundaries of the image.

        if "moffat" in method:
            del moffat_opt
            moffat_opt = np.zeros((self.N[0], 5))
        if "gauss" in method:
            del gauss_opt
            gauss_opt = np.zeros((self.N[0], 4))

        if "gauss" in method:
            count = [0, 0]
            for i in range(self.N[0]):
                xpoints = self.pix_vals[1][range_spec[0, i] : range_spec[1, i]]
                ypoints = self.image[i, range_spec[0, i] : range_spec[1, i]]
                def gauss_locked(x, A, HWHM, p0, p1):
                    return gauss_m(x, A, pospoly[i], HWHM, p0, p1)
                p_opt_poly = np.polyfit(
                    xpoints,
                    ypoints,
                    1,
                )
                A0 = np.max(ypoints[
                    np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                    np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                ]) - tmedian(ypoints)
                    # A0 is the initial amplitude guess. The highest
                    # point within 1*sigma from x0 (hwhmpoly[i]).
                p0 = [
                    A0,
                    hwhmpoly[i],
                    tmedian(ypoints),
                    gauss_opt1[i][3]
                ]
                remove1 = get_cray(
                    ypoints[: np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0])  ],
                    A0,
                ) + int(xpoints[0])
                remove2 = get_cray(
                    ypoints[  np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0]) :],
                    A0,
                ) + int(np.ceil( pospoly[i] + hwhmpoly[i]))
                if remove1.size > 0:
                    self.weights[i][remove1] = 0
                if remove2.size > 0:
                    self.weights[i][remove2] = 0
                sigma = self.weights[i, range_spec[0, i]:range_spec[1, i]]
                try:
                    gauss_opt[i], p_cov = curve_fit(
                        gauss_locked,
                        xpoints,
                        ypoints,
                        p0 = p0,
                        sigma = sigma,
                    )
                except RuntimeError:
                    print "RuntimeError occured at i =", i
                except TypeError:
                    print "TypeError occured at i =", i
                BIC_poly = BIC(polynomial, p_opt_poly, xpoints, ypoints, 1./sigma)
                BIC_gauss = BIC(
                    gauss_m,
                    np.hstack((gauss_opt[i, :1], pospoly[i], gauss_opt[i, 1:])),
                    xpoints,
                    ypoints,
                    1./sigma,
                )

                if show and i % 2 == 0: # Not show all.
                    _p_opt = np.hstack((gauss_opt[i, :1], pospoly[i], gauss_opt[i, 1:]))
                    print "Now testing i=%d" % i
                    print "BIC: poly: %g, gauss: %g" % (BIC_poly, BIC_gauss)
                    if BIC_poly < BIC_gauss:
                        print "Polyfit is better.", len(p_opt_poly)
                        fig = plt.figure()
                        ax1 = fig.add_subplot(2,1,1)
                        ax1.set_title(
                            "'%s'\n"
                            "%d Poly opt: %s\n"
                            "Gauss guess: %s\n"
                            "Gauss opt: %s"
                            % (self.filename, i, str(p_opt_poly), str(p0), str(_p_opt))
                        )
                        ax1.plot(xpoints, ypoints, color="b", label="data")
                        ax1.plot(xpoints, gauss_m(xpoints, params=_p_opt), color="r", label="gauss")
                        ax1.plot(xpoints, polynomial(xpoints, params=p_opt_poly), color="g", label="poly")
                        ax1.axhline(tmedian(ypoints), color="k", label="tmedian")
                        ax1.plot(
                            xpoints[
                                np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                                np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                            ],
                            ypoints[
                                np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                                np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                            ],
                            "ko",
                            label = "peak",
                        )
                        ax1.plot(
                            xpoints,
                            sigma / np.max(sigma) * A0 + tmedian(ypoints),
                            color = "m",
                            label = "weights"
                        )
                        ax1.legend()
                        # Residual plot:
                        ax2 = fig.add_subplot(2,1,2)
                        ax2.set_title("%d Poly: %s,  Gauss: %s" % (i, str(BIC_poly), str(BIC_gauss)))
                        ax2.plot(xpoints, ypoints-ypoints, "b-", label="ideal")
                        ax2.plot(xpoints, (moffat(xpoints, params=_p_opt)-ypoints)**2, "ro", label="gauss")
                        ax2.plot(xpoints, (polynomial(xpoints, params=p_opt_poly)-ypoints)**2, "go", label="poly")
                        ax2.legend()
                        plt.show()
                    else:
                        print "Gauss is better.", len(_p_opt)
                        if gauss_opt[i, 0] < 0:
                            print "    NEGATIVE!"

            BIC_total_gauss += BIC_gauss
            if BIC_poly < BIC_gauss:
                print "%d: POLYFIT is better." % i
                count[0] += 1
                moffat_opt[i, 0] = 0  # Does not work so well, so ignore it for now.
            else:
                print "%d: GAUSS is better." % i
                count[1] += 1
                if gauss_opt[i, 0] < 0:
                    print "    NEGATIVE:", gauss_opt[i]
                    gauss_opt[i, 0] = 0

            print "COUNT result:", count

            gauss_opt[:,2] = np.abs(gauss_opt[:,2]) # std must be positive
            gauss_opt = np.hstack((gauss_opt[:, :1], pospoly[:, None], gauss_opt[:, 1:]))
            self.gauss_opt = gauss_opt

        if "moffat" in method:
            count = [0, 0]
            for i in range(self.N[0]):
                xpoints = self.pix_vals[1][range_spec[0, i] : range_spec[1, i]]
                ypoints = self.image[i, range_spec[0, i] : range_spec[1, i]]
                def moffat_locked(x, A, HWHM, MI, p0, p1):
                    return moffat(x, A, pospoly[i], HWHM, MI, p0, p1)
                p_opt_poly = np.polyfit(
                    xpoints,
                    ypoints,
                    1,
                    #TODO This is the reported "line 569" in the traceback
                    # of "Bug #303: New, mystical TypeError".
                )
                try:
                    A0 = np.max(ypoints[
                        np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                        np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                    ]) - tmedian(ypoints)
                        # A0 is the initial amplitude guess. The highest
                        # point within 1*sigma from x0 (hwhmpoly[i]).
                except ValueError:
                    try:
                        A0 = np.max(ypoints[
                            np.floor(pospoly[i] - 2*hwhmpoly[i] - xpoints[0]) :
                            np.ceil( pospoly[i] + 2*hwhmpoly[i] - xpoints[0])
                        ]) - tmedian(ypoints)
                    except ValueError:
                        try:
                            A0 = np.max(ypoints[
                                np.floor(pospoly[i] - 2*hwhmpoly[i]-1 - xpoints[0]) :
                                np.ceil( pospoly[i] + 2*hwhmpoly[i]+1 - xpoints[0])
                            ]) - tmedian(ypoints)
                        except ValueError:
                            A0 = 0.
                p0 = [
                    A0,
                    hwhmpoly[i],
                    mipoly[i],
                    tmedian(ypoints),
                    moffat_opt1[i][4]
                ]
                remove1 = get_cray(
                    ypoints[: np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0])  ],
                    A0,
                ) + int(xpoints[0])
                remove2 = get_cray(
                    ypoints[  np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0]) :],
                    A0,
                ) + int(np.ceil( pospoly[i] + hwhmpoly[i]))
                if remove1.size > 0:
                    self.weights[i][remove1] = 0
                if remove2.size > 0:
                    self.weights[i][remove2] = 0
                sigma = self.weights[i, range_spec[0, i]:range_spec[1, i]]
                try:
                    moffat_opt[i], p_cov = curve_fit(
                        moffat_locked,
                        xpoints,
                        ypoints,
                        p0 = p0,
                        sigma = sigma,
                    )
                except RuntimeError:
                    print "RuntimeError occured at i =", i
                except TypeError:
                    print "TypeError occured at i =", i
                BIC_poly = BIC(polynomial, p_opt_poly, xpoints, ypoints, 1./sigma)
                BIC_moffat = BIC(
                    moffat,
                    np.hstack((moffat_opt[i, :1], pospoly[i], moffat_opt[i, 1:])),
                    xpoints,
                    ypoints,
                    1./sigma,
                )

                if show and i % 2 == 0: # Not show all.
                    _p_opt = np.hstack((moffat_opt[i, :1], pospoly[i], moffat_opt[i, 1:]))
                    print "Now testing i=%d" % i
                    print "BIC: poly: %g, moffat: %g" % (BIC_poly, BIC_moffat)
                    if BIC_poly < BIC_moffat:
                        print "Polyfit is better.", len(p_opt_poly)
                        fig = plt.figure()
                        ax1 = fig.add_subplot(2,1,1)
                        ax1.set_title(
                            "'%s'\n"
                            "%d Poly opt: %s\n"
                            "Moffat guess: %s\n"
                            "Moffat opt: %s"
                            % (self.filename, i, str(p_opt_poly), str(p0), str(_p_opt))
                        )
                        ax1.plot(xpoints, ypoints, color="b", label="data")
                        ax1.plot(xpoints, moffat(xpoints, params=_p_opt), color="r", label="moffat")
                        ax1.plot(xpoints, polynomial(xpoints, params=p_opt_poly), color="g", label="poly")
                        ax1.axhline(tmedian(ypoints), color="k", label="tmedian")
                        ax1.plot(
                            xpoints[
                                np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                                np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                            ],
                            ypoints[
                                np.floor(pospoly[i] - hwhmpoly[i] - xpoints[0]) :
                                np.ceil( pospoly[i] + hwhmpoly[i] - xpoints[0])
                            ],
                            "ko",
                            label = "peak",
                        )
                        ax1.plot(
                            xpoints,
                            sigma / np.max(sigma) * A0 + tmedian(ypoints),
                            color = "m",
                            label = "weights"
                        )
                        ax1.legend()
                        # Residual plot:
                        ax2 = fig.add_subplot(2,1,2)
                        ax2.set_title("%d Poly: %s,  Moffat: %s" % (i, str(BIC_poly), str(BIC_moffat)))
                        ax2.plot(xpoints, ypoints-ypoints, "b-", label="ideal")
                        ax2.plot(xpoints, (moffat(xpoints, params=_p_opt)-ypoints)**2, "ro", label="moffat")
                        ax2.plot(xpoints, (polynomial(xpoints, params=p_opt_poly)-ypoints)**2, "go", label="poly")
                        ax2.legend()
                        plt.show()
                    else:
                        print "Moffat is better.", len(_p_opt)
                        if moffat_opt[i, 0] < 0:
                            print "    NEGATIVE!"

            BIC_total_moffat += BIC_moffat
            if BIC_poly < BIC_moffat:
                print "%d: POLYFIT is better." % i
                count[0] += 1
                moffat_opt[i, 0] = 0  # Does not work so well, so ignore it for now.
            else:
                print "%d: MOFFAT is better." % i
                count[1] += 1
                if moffat_opt[i, 0] < 0:
                    print "    NEGATIVE:", moffat_opt[i]
                    moffat_opt[i, 0] = 0

            print "COUNT result:", count

            moffat_opt[:,2] = np.abs(moffat_opt[:,2]) # std must be positive
            moffat_opt = np.hstack((moffat_opt[:, :1], pospoly[:, None], moffat_opt[:, 1:]))
            self.moffat_opt = moffat_opt

        ### Save:
        #TODO This is just a temporary way to store a result.
        if BIC_total_moffat < BIC_total_gauss or "gauss" not in method:
            self.best_bell = "moffat"
        else:
            self.best_bell = "gauss"
        if "moffat" in method:
            self.moffat_opt = moffat_opt
        if "gauss" in method:
            self.gauss_opt = gauss_opt
        self.pospoly = pospoly
        self.hwhmpoly = hwhmpoly
        self.range_spec = range_spec


    def get_spectrum(self, method="fit", method_atmosphere="box", show=True):
        """Get the spectrum by integrating image. Range based on fit.
        Assumes find_spectrum() is called.
        Calls remove_atmosphere() if needed, which is only when method="box".
        """
        spectrum = np.zeros(self.N[0])

        if method in ("fit", "bell", "bellfit"):
            method = self.best_bell

        if "gauss" in method:
            for i in self.pix_vals[0]:
                spectrum[i] = quad(
                    gauss_m,
                    -np.inf,
                    +np.inf,
                    args = (
                        self.gauss_opt[i, 0],
                        0,  # The position x0 does not matter for the sum.
                        self.gauss_opt[i, 2],
                    ),
                )[0]
            for i, value in enumerate(spectrum):
                if value <= 0:
                    print i, value, self.gauss_opt[i]
        elif "moffat" in method:
            for i in self.pix_vals[0]:
                spectrum[i] = quad(
                    moffat,
                    -np.inf,
                    +np.inf,
                    args = (
                        self.moffat_opt[i, 0],
                        0,  # The position x0 does not matter for the sum.
                        self.moffat_opt[i, 2],
                        self.moffat_opt[i, 3],
                    ),
                )[0]
        elif "box" in method:
            self.remove_atmosphere(method_atmosphere)
            # if self.sky_unit == "FWHM":
                # radius  = 0.5 * self.sky_radius[0] * self.hwhmpoly
            # elif self.sky_unit == "pixels":
                # radius  = self.sky_radius[0]

            for i in range(self.N[0]):
                spectrum[i] = np.sum(self.image[
                    i,
                    self.range_spec[0, i] : self.range_spec[1, i],
                    # self.pospoly[i] - 15 :
                    # self.pospoly[i] + 15,
                ])
        else:
            raise TypeError(
                "'%s' is an unknown method. Expected one of %s."
                % (method, str(("moffat", "gauss", "box")))
            )
            return False

        # Negative flux is unphysical.
        #TODO The fitting should be improved instead.
        spectrum[np.where(spectrum < 0)] = 0

        if show:
            plt.figure()
            plt.plot(spectrum)
            plt.title("'%s', method=%s" % (self.filename, method))
            plt.show()

        return spectrum


    def remove_atmosphere(self, method="box"):
        """Removes the atmosphere by subtracting the linear fit or by the
        box method. Assumes the spectrum position is determined.
        """

        if method == "fit":
            for i in range(self.N[0]):
                self.image[i] -= polynomial(
                    self.pix_vals[1],
                    [self.moffat_opt[i, 5], self.moffat_opt[i, 4]],
                )

        elif method == "box":
            if self.sky_unit[0] == "FWHM":
                radius_in  = 0.5 * self.sky_radius[0] * self.hwhmpoly
                radius_out = 0.5 * self.sky_radius[1] * self.hwhmpoly
            elif self.sky_unit[0] == "pixels":
                radius_in  = self.sky_radius[0] * np.ones(self.N[0])
                radius_out = self.sky_radius[1] * np.ones(self.N[0])
            for i in range(self.N[0]):
                atmosphere_x = np.hstack([
                    self.pix_vals[1][
                        self.pospoly[i] - radius_out[i] :
                        self.pospoly[i] - radius_in[i]
                    ],
                    self.pix_vals[1][
                        self.pospoly[i] + radius_in[i] :
                        self.pospoly[i] + radius_out[i]
                    ],
                ])
                atmosphere_y = np.hstack([
                    self.image[i][
                        self.pospoly[i] - radius_out[i] :
                        self.pospoly[i] - radius_in[i]
                    ],
                    self.image[i][
                        self.pospoly[i] + radius_in[i] :
                        self.pospoly[i] + radius_out[i]
                    ],
                ])
                weights = np.hstack([
                    self.weights[i][
                        self.pospoly[i] - radius_out[i] :
                        self.pospoly[i] - radius_in[i]
                    ],
                    self.weights[i][
                        self.pospoly[i] + radius_in[i] :
                        self.pospoly[i] + radius_out[i]
                    ],
                ])
                p_opt = np.polyfit(
                    atmosphere_x,
                    atmosphere_y,
                    deg = 1,
                    w = weights,
                )
                self.image[i] -= polynomial(self.pix_vals[1], p_opt)

        else:
            raise TypeError(
                "'%s' is not a recognized method for removing the "
                "background/atmosphere. Expected one of %s."
                % (str(method), str(("fit", "box")))
            )


    def fix_bad_fit_points(self, moffat_opt, n=10):
        """Detect and fix bad fit points after a fit by splitting the fits
        up in n sections and passing each of them on to
        fix_bad_fit_points_section.
        """
        section_size = self.N[0] / n
        for i in range(n):
            a = i * section_size
            b = (i+1) * section_size
            moffat_opt[a:b] = self.fix_bad_fit_points_section(moffat_opt[a:b])
        return moffat_opt


    def fix_bad_fit_points_section(self, moffat_opt):
        """Detect and fix bad fit points after a fit. Assumes a section of
        all the fits which should have roughly the same position.
        """

        N = moffat_opt.shape[0]
        x = moffat_opt[:,1]
        x[np.logical_not(np.isnan(x))] # not nan
        x0 = np.median(x)
        x = np.sort( moffat_opt[:,1] )
        x = x[int(0.05*N) : int(-0.05*N)]
        # Trims away potential outliers.
        std = x.std()

        # List only intensities within 3*std of the median (removes the
        # outliers):
        x_trimmed = moffat_opt[:,1].tolist()
        for i in range(N):
            if not (x0 - 3 * std < moffat_opt[i,1] < x0 + 3 * std):
                x_trimmed[i] = np.nan
                # A new value has to be found.

        # Make sure the first and last point is not bad:
        if np.isnan(x_trimmed[0]):
            for ii in range(1, N):
                # In case the next one is also bad, look for the first good one.
                if np.isnan(x_trimmed[ii]):
                    x_trimmed[0] = x_trimmed[ii]
                    # Set it to the nearest good value.
                    before = moffat_opt[0,1]
                    moffat_opt[0] = moffat_opt[ii]
                    after  = moffat_opt[0,1]
                    print "Changed %f to %f on row %4d" % (before, after, 0)
                    break
        if np.isnan(x_trimmed[-1]):
            for ii in range(N-1, 0-1, -1):
                # In case the second last one is also bad, look for the
                # nearest good one.
                if np.isnan(x_trimmed[ii]):
                    x_trimmed[-1] = x_trimmed[ii]
                    # Set it to the nearest good value.
                    before = moffat_opt[-1,1]
                    moffat_opt[-1] = moffat_opt[ii]
                    after  = moffat_opt[-1,1]
                    print "Changed %f to %f on row %4d (last row)" % (before, after, N)
                    break

        # Fix everything in between:
        for i in range(1, N-1): # Exclude first and last one.
            if np.isnan(x_trimmed[i]):
                # Need to find a new value since this one is bad.
                for ii in range(i+1, N):
                    # In case the next one is also bad, look for the next
                    # good one.
                    if np.isnan(x_trimmed[ii]):
                        x_trimmed[i] = (x_trimmed[i-1] + x_trimmed[ii]) / 2.
                        # Change the bad point to the average of the nearest
                        # good one in each direction. This is low quality
                        # interpolation for now.
                        before = moffat_opt[i,1]
                        moffat_opt[i] = (moffat_opt[i-1] + moffat_opt[ii]) / 2.
                        after  = moffat_opt[i,1]
                        print "Changed %f to %f on row %4d" % (before, after, i)
                        break

        return moffat_opt


    def write_to_file(self, path, method="fit"):
        #TODO get_spectrum is called two times and does the same calculation
        # twice!
        # Automatically overwrites outfile if it already exists.
        outfile = open(path, "w")
        spectrum = self.get_spectrum(method=method, show=False)
        for i, y in zip(self.pix_vals[0], spectrum):
            outfile.write("%d %s\n" % (i, repr(y)))
        outfile.close()


def get_cray(data, limit):
    base = tmedian(data)
    bad = []
    for i, value in enumerate(data):
        if abs(value - base) > limit:
            bad.append(i)
    return np.array(bad)


if __name__ == "__main__":
    """
    Sample use of this module with given data.
    """

    infile = fits.open("../../practice/GTC59-14A/OB0001/stds/0000692609-20140721-OSIRIS-OsirisLongSlitSpectroscopy.fits")
    # infile = fits.open("../../practice/GTC59-14A/OB0001/object/0000692699-20140721-OSIRIS-OsirisLongSlitSpectroscopy.fits")
    data = infile[2].data[:, 150:450]
    # data = infile[2].data[:, 260:300]

    reducer = Reduction()
    reducer.set_image(data)
    reducer.set_aperture_radius(5)   # The wings of the peak is so large that
    reducer.set_sky_radius((12, 25)) # this large range is necessary.

    # reducer.show_fit( 500)
    # reducer.show_fit(1000)
    # reducer.show_fit(1500)

    reducer.locate_spectrum(100) # 100 => x=251 on full image

    fig = plt.figure()
    # ax = [ fig.add_subplot(3,1,1),
           # fig.add_subplot(3,2,1),
           # fig.add_subplot(3,3,1),
            # ]
    # ax[0].imshow(np.log(reducer.image), interpolation='nearest', cmap=plt.cm.gray)
    # ax[1].imshow(np.log(reducer.fit1),  interpolation='nearest', cmap=plt.cm.gray)
    # ax[2].imshow(np.log(reducer.fit2),  interpolation='nearest', cmap=plt.cm.gray)

    for i in range(reducer.N[0]):
        reducer.image[i, int(np.rint(reducer.pos[i]))] = 0#np.max(reducer.image)
        # Draw a black line where the position of the fit is on the image, so
        # we can check the quality of the fit.
    plt.imshow(np.log(reducer.image), interpolation='nearest', cmap=plt.cm.gray)
    plt.show()

    plt.imshow(np.log(reducer.image), interpolation='nearest', cmap=plt.cm.gray)
    plt.show()

    plt.plot(reducer.get_spectrum())
    plt.show()

    #fits.PrimaryHDU(reducer.image_stacked).writeto(
    #    "../../practice/GTC39-11B/OB0010/image_std03_stacked.fits")
