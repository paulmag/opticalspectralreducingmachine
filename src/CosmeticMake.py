import os
import numpy as np
from scipy import stats, ndimage, misc
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
pylab.rcParams['figure.figsize'] = 16, 12

from astropy.io import fits
from skimage import transform
from skimage.morphology import erosion, square, disk

import Functions as func


class CosmeticMake:

    def __init__(self):
        self.biases  = []
        self.flats   = []
        self.default_method = "tmean"

        self.bias_master = None
        self.flat_master = None


    def set_path(self, path):
        """Path of the folder that is to be made to contain the master
        calibrations files.
        """
        self.path = path
        func.make_folder(path)


    def set_method(self, default_method):
        """
        Measure of central tendency / stacking method: Mean, median, tmean
        (mean+median) or mode.  Mode is not supported yet.  If this is not
        stated then the default given in __init__ is used.  Even if a new
        method is stated here it can still be overruled when make_master is
        called.
        """
        self.default_method = default_method


    def make_master(self, master_type="all", method=None, n=1, show=False):
        """General method for creating all masters.  'bias' or 'flat' has to
        be specified.  Or if you want to use the same stacking method for
        both types, you can give the argument 'both' or 'all'.  The default
        is 'all'.  It is assumed that a master bias is already made when the
        master flat is to be made.
        """

        if method is None:
            method = self.default_method
        if master_type in ("both", "all"):
            self.make_master("bias", method, n)
            self.make_master("flat", method, n)
            return

        ### Generic variable for the frametype to be used ###
        if master_type in ("bias"):
            frames = func.transpose(self.biases)
        elif master_type in ("flat"):
            frames = func.transpose(self.flats)
            # Subtract bias_master and normalise to 1 for all flats:
            for j, CCDs in enumerate(frames):
                frames[j] -= self.bias_master[j]
                for i, CCD in enumerate(CCDs):
                    frames[j][i] /= CCD.mean()
        elif master_type == "dark":
            print "Error: Dark frames currently not supported."
            return
        else:
            print "Error: Unknown master type. Select 'bias' or 'flat'."
            return

        ### Perform the chosen stacking method ###
        if method in ("mean", "average", "avg"):
            master = [np.mean(CCDs, axis=0) for CCDs in frames]
        elif method in ("median", "med"):
            master = [np.median(CCDs, axis=0) for CCDs in frames]
        elif method in ("mode"):
            from scipy.stats import mode
            master = [np.mean(CCDs, axis=0) for CCDs in frames][0][0]
                #TODO Currently does not work.
        elif method in ("tmean", "trimmedmean", "t mean", "trimmed mean"):
            if 2 * n >= frames.shape[1]:
                print "Error: You trimmed away ALL the flats!"
                return
            if 2 * n == frames.shape[1] + 1:
                print "Warning: Just one flat remaining, i.e. median is used."
            master = [
                np.mean(np.sort(frames, axis=0)[n:-n], axis=0)
                for CCDs in frames
            ]
        else:
            print "Error: Unknown method type. Select 'mean', 'median', 'tmean' or 'mode'."
            return

        ### Store the resulting master ###
        if master_type == "bias":
            self.bias_master = master

        elif master_type == "flat":
            self.flat_master = master
            self._pyramid(show=show)
            self._set_bad_pixels(show=show)
            self.flat_master = self.flat_diffs

            if show:
                fig, axes = plt.subplots(ncols=len(self.flat_master))
                fig.suptitle("Master flat")
                for i, CCD_flat in enumerate(self.flat_master):
                    axes[i].imshow(
                        CCD_flat,
                        cmap=plt.cm.gray,
                        vmin=np.min(CCD_flat),
                        vmax=np.max(CCD_flat),
                    )
                    axes[i].set_title('CCD no %d' % i)
                plt.show()

                fig, axes = plt.subplots(ncols=len(self.mask))
                fig.suptitle("Mask")
                for i, CCD_flat in enumerate(self.mask):
                    axes[i].imshow(
                        CCD_flat,
                        cmap=plt.cm.gray,
                        vmin=np.min(CCD_flat),
                        vmax=np.max(CCD_flat),
                    )
                    axes[i].set_title('CCD no %d' % i)
                plt.show()


    def _pyramid(self, n=4, show=False):
        # Multi-scale background estimator
        # We use a pyramidal transform from the fantastic pyramid_reduce function of skimage
        # The bad pixels are estimated from the n th scale
        # n should be a user's defined parameter in the instrument XML file. Its default value should be set to 4.

        self.flat_diffs = []
        for master in self.flat_master:

            # Flat must be normalize for skimage to work
            maxi = np.max(master)
            flat = master / maxi

            # Then we do the pyramidal transform over n scales
            for i in range(n):
                # downscale and smooth
                downsmooth = transform.pyramid_reduce(
                    flat,
                    downscale = 2,
                    sigma = 3,
                    order = 5,
                    mode = 'nearest',
                )
                # use the result as new input
                flat = downsmooth

            # Resample the n'th scale of the pyramidal transform - which is our
            # background estimate - to the original size, using bicubic
            # interpolation.
            background = transform.resize(
                flat,
                master.shape,
                order = 5,
                mode='nearest',
            )

            # Compute the difference between the flat field and the background.
            # We must multiply again by maxi to get back to the same scale!
            diff = master - (background * maxi)

            if show:
                #TODO This hist is displayed in a very strange way.
                plt.hist(
                    np.abs(diff[np.isfinite(diff)].flatten()),
                    500,
                    normed = True,
                    color = 'r',
                    alpha = 0.5,
                    histtype = 'stepfilled',
                )
                plt.xlim(
                    0,
                    max(diff[np.isfinite(diff)].flatten()),
                )
                plt.xlabel('Value')
                plt.ylabel('Number of pixels')
                plt.axvline(n * np.std(diff))
                plt.yscale('log')
                plt.show()

            self.flat_diffs.append(diff)


    def _set_bad_pixels(self, treshold=None, n_erode=None, show=False):

        if treshold is None:
            treshold = self.xml_i.badpixel_treshold
        if n_erode is None:
            n_erode = self.xml_i.badpixel_dilateradius

        self.mask = []
        for diff in self.flat_diffs:

            mask = np.abs(diff) < treshold * np.std(diff)
            selem = disk(n_erode)
            mask_eroded = erosion(mask, selem)  # 255  and 0
            mask_eroded = mask_eroded != False  # True and False
            diff += 1  # Center around 1.

            if show:
                fig, (ax1, ax2) = plt.subplots(ncols=2)
                ax1.imshow(mask, cmap=plt.cm.gray)
                ax1.set_title('mask')
                ax2.imshow(mask_eroded, cmap=plt.cm.gray)
                ax2.set_title('erosion')
                plt.show()

            self.mask.append(mask)


    def write_to_file(self, master_type, path=None):
        """Save one or both master as FITS-files, thereby making them
        survive after runtime for later use or examination.
        """

        if path is None:
            if self.path is None:
                raise IOError("No output path specified.")
            path = self.path + "/master_" + master_type

        if master_type in ["both", "all"]:
            self.write_to_file("bias")
            self.write_to_file("flat")
            self.write_to_file("weights")

        elif master_type in ["bias"]:
            hdus = \
                [fits.PrimaryHDU(None)] + \
                [fits.ImageHDU(CCD) for CCD in self.bias_master]
            hdulist = fits.HDUList(hdus)
            if os.path.exists(path):
                # If the filename already existed the file will be overwritten:
                os.remove(path)
            hdulist.writeto(path)

        elif master_type in ["flat"]:
            hdus = \
                [fits.PrimaryHDU(None)] + \
                [fits.ImageHDU(CCD) for CCD in self.flat_master]
            hdulist = fits.HDUList(hdus)
            if os.path.exists(path):
                os.remove(path)
            hdulist.writeto(path)

        elif master_type in ["mask"]:
            hdus = \
                [fits.PrimaryHDU(None)] + \
                [fits.ImageHDU(CCD.astype(int)) for CCD in self.mask]
            hdulist = fits.HDUList(hdus)
            if os.path.exists(path):
                os.remove(path)
            hdulist.writeto(path)

        else:
            raise TypeError(
                "'%s' is not a recognized master_type. "
                "Use 'bias', 'master' (or 'all')."
                % master_type
            )



if __name__ == "__main__":
    """Sample run."""

    import os

    cleaner = CosmeticMake()
    cleaner.set_path(
        os.path.expanduser("~/practice/test_results/"),
    )
