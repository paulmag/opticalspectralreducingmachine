import sys
import os
import numpy as np
from scipy.optimize import curve_fit
import re

from astropy.io import fits
from astropy.time import Time
from astropy.coordinates import Angle
import xmltodict
import Tkinter
import tkMessageBox
Tkinter.Tk().withdraw()

import Functions as func
from ReductionBlock import ReductionBlock


def dict_to_RBs(RB_dict):
    """
    Expects a nested dict representing reduction Blocks. Each leaf is a RB.
    Returns a list of ReductionBlock instances.
    The 6 keys are (in ranked order):
    * objtype
    * objname
    * jdate
    * gain
    * grism
    * maskname
    * airmass
    * ra
    * dec
    If a key is not used for an object, all the keys below it is also
    not used.
    """
    keylist = [None] * 9
    RBs = []
    def iterate(RB_dict, depth=0):
        for key, value in RB_dict.iteritems():
            if isinstance(value, dict):
                keylist[depth] = key
                iterate(value, depth+1)
            else:
                keylist[depth] = key
                for i in range(depth+1, 9):
                    keylist[i] = None
                new_RB = ReductionBlock(keylist=keylist, path=self.path_rbs)
                new_RB.add_filenames(value)
                RBs.append(new_RB)
    iterate(RB_dict)
    return RBs



class RBMake:
    """Finds all FITS-files in the destination folder, looks in their
    headers to find out what kind of data they contain and sort them
    accordingly into reduction blocks according to the reduction block rules
    that will originally come from a XML-file.
    """

    def __init__(self):
        self.infiles = []
        self.RBs = {}
        self.root = None # root of the XML-tree


    def set_path_raw(self, p):
        """
        p: Path of folder containing input FITS-files.
        """
        self.path_raw = p

    def set_path_xml(self, p):
        """
        p: Path of the instrument XML-file
        """
        self.path_xml = p

    def set_path_rbs(self, p):
        """p: Path of the folder that is to be made to contain the
        reduction blocks.
        """
        self.path_rbs = p


    def read_xml(self, p=False):
        """
        Makes a dictionairy containting the entire XML-file.
        The direct path to this file can be given as an argument, but it's not
        necessary if the path is already set.
        """
        if not p:
            p = self.path_xml

        infile = open(p, "r")
        tree = xmltodict.parse(infile)
        infile.close()
        self.root = tree["spextractor"]


    def find_keywords(self):
        """
        Extracts necessary keywords from the XML-tree. This isn't strictly necessary to do, but the names would be very long and confusing if not.
        """
        #TODO Make placeholders for all these variables in __init__? The
        # final form of the variables isn't determined yet.

        self.key_objname = self.root["instrument"]["keyword-fits-raw"]["object-name"]["name"]
        self.key_gain    = self.root["instrument"]["keyword-fits-raw"]["gain"]["name"]
        self.gain_type   = self.root["instrument"]["keyword-fits-raw"]["gain"]["type"]
        self.key_grism   = self.root["instrument"]["keyword-fits-raw"]["grism"]["name"]
        self.grism_type  = self.root["instrument"]["keyword-fits-raw"]["grism"]["type"]
        if self.key_grism is None:
            pass
        elif self.key_grism.lower() in ("", "none", "exclude"):
            self.key_grism = None
        self.key_mask    = self.root["instrument"]["keyword-fits-raw"]["mask"]["name"]
        if self.key_mask is None:
            pass
        elif self.key_mask.lower() in ("", "none", "exclude"):
            self.key_mask = None
        self.key_jday    = self.root["instrument"]["keyword-fits-raw"]["date-obs"]["name"]
        self.key_jday_unit = self.root["instrument"]["keyword-fits-raw"]["date-obs"]["type"]
        self.key_airmass = self.root["instrument"]["keyword-fits-raw"]["airmass"]["name"]
        self.key_ra      = self.root["instrument"]["keyword-fits-raw"]["right-ascension"]["name"]
        self.key_ra_unit = self.root["instrument"]["keyword-fits-raw"]["right-ascension"]["unit"]  # unused
        self.key_dec     = self.root["instrument"]["keyword-fits-raw"]["declination"]["name"]
        self.key_dec_unit= self.root["instrument"]["keyword-fits-raw"]["declination"]["unit"] # unused
        self.key_extname = self.root["instrument"]["keyword-fits-raw"]["chip-id"]["name"]

        self.value_objname = {}
        self.value_objname["reject"] = re.compile(
            self.root["instrument"]["object-name-mapping"]["reject"],
            re.IGNORECASE)
        self.value_objname["bias"]   = re.compile(
            self.root["instrument"]["object-name-mapping"]["bias"],
            re.IGNORECASE)
        self.value_objname["flat"]   = re.compile(
            self.root["instrument"]["object-name-mapping"]["flat"],
            re.IGNORECASE)
        self.value_objname["arc"]    = re.compile(
            self.root["instrument"]["object-name-mapping"]["arc"],
            re.IGNORECASE)
        self.value_objname["std"]    = re.compile(
            self.root["instrument"]["object-name-mapping"]["photstd"],
            re.IGNORECASE)
        self.value_objname["target"] = re.compile("", re.IGNORECASE)
        # Empty string is contained in all strings. Every string contains
        # "", but this value is checked for last, so an object becomes a
        # target if it fits nothing else.
        # WARNING: This is not very robust or elegant.

        associations = self.root["instrument"]["reduction-block-associations"] #TODO Dicts should also be
        self.diff_jday_bias = float( associations["calib"]["bias-max-diff-jday"] ) # made here...
        self.diff_jday_flat = float( associations["calib"]["flat-max-diff-jday"] )
        self.diff_jday_std    = float( associations["science"]["sci-std-jday-max-diff"] )
        # self.diff_jday_target = float( associations["science"]["sci-max-diff-jday"] )
        self.diff_airmass_std    = float( associations["science"]["sci-std-airmass-max-diff"] )
        # self.diff_airmass_target = float( associations["science"]["sci-max-diff-airmass"] )
        self.no_bias = (
            int(associations["calib"]["bias-min-good"]),
            int(associations["calib"]["bias-max-good"]),
        )
        self.no_flat = (
            int(associations["calib"]["flat-min-good"]),
            int(associations["calib"]["flat-max-good"]),
        )


    def find_files(self, p=False, show=False):
        """
        Makes a list of opened FITS-files.
        The path to this folder can be given as an argument, but it's not
        necessary if the path is already set.
        """
        if not p:
            p = self.path_raw

        for dirname, dirnames, filenames in os.walk(p):
            for filename in filenames:
                filepath = os.path.join(dirname, filename)
                if filepath.endswith(".fits") or filepath.endswith(".fit"):
                    self.infiles.append( fits.open(filepath) )
                    if show:
                        print "Added file: %s" % filename


    def extract_data(self, show=False):
        """ Reads data from files and sorts it into reduction blocks
        according to the info in the header.  Makes a list of ReductionBlock
        instances, which can be used further as desired. This is the main
        purpose of this module.
        """

        all_keys_ordered = ["reject", "bias", "flat", "arc", "std", "target"]
        # Important: "target" is the last key!
        # WARNING: This is not very robust or elegant. Be careful when
        # changing something.
        airmasses_keys_ordered = ["std", "target"]
        jdates = {}
        jdates["bias"]   = []
        jdates["flat"]   = []
        jdates["arc"]    = []
        jdates["std"]    = []
        jdates["target"] = []
        jdates_blocks = {}
        airmasses = {}
        airmasses["std"]    = []
        airmasses["target"] = []
        airmasses_blocks = {}
        RB_dict = {}

        to_delete = []
        for i, infile in enumerate(self.infiles):

            if self.key_objname not in infile[0].header:
                # If it is not an object file.
                print (
                    "Warning: File", infile,
                    "does not have the %s keyword and can not be handled."
                    "What kind of file is this?"
                    % (self.key_objname)
                )
                continue

            if self.key_extname in infile[0].header:
                if infile[0].header[self.key_extname] not in self.xml_i.chip_extname_to_use:
                    # Only use files with extname to be used,
                    # if extname is in primary header.
                    to_delete.append(i)
                    continue

            identified = False
            for key in all_keys_ordered:
                if self.value_objname[key].match(infile[0].header[self.key_objname]) is not None:
                    identified = True
                    if key == "reject": break
                    if show:
                        print "identified %6s: %s" \
                            % (key, infile[0].header[self.key_objname])
                    # Find the correct keyword.
                    jdates[key].append(
                        Time(
                            infile[0].header[self.key_jday],
                            format = self.key_jday_unit,
                            # Converts any time format to mjd.
                        ).mjd
                    )
                    if key in airmasses_keys_ordered:
                        airmasses[key].append( infile[0].header[self.key_airmass] )
                    break
            if not identified:
                print (
                    "Warning: File", infile,
                    "is of the object %s, which is not recognized. "
                    "What kind of file is this?"
                    % (infile[0].header[self.key_objname])
                )

        for index in to_delete[::-1]:
            del self.infiles[index]

        for key in jdates:
            jdates[key] = np.sort(jdates[key])
            jdates_blocks[key]  = [[]]
                # List of jdates within each reduction block.
            try:
                jdates_blocks[key][0].append(jdates[key][0])
                # Add the earliest jdate in the first block.
            except IndexError:
                print "key:", key
                print "jdates[key]:\n", jdates[key]
                if key == "std":
                    # Accept it and only issue warning if STD missing.
                    print "Warning: No standard stars are found."
                else:
                    # Raise error if anything else is missing.
                    raise IOError("No files with the key '%s' was found." % key)

            if key == "bias":
                for i in range(1, len(jdates[key])):
                    # Loop through rest of jdates.  If difference too large to
                    # be within same RB, make new block, then insert current
                    # value in current block.  Do this for bias and flat.
                    if jdates[key][i] - jdates_blocks[key][-1][0] > self.diff_jday_bias:
                        jdates_blocks[key].append([])
                    jdates_blocks[key][-1].append(jdates[key][i])
            elif key == "flat":
                for i in range(1, len(jdates[key])):
                    if jdates[key][i] - jdates_blocks[key][-1][0] > self.diff_jday_flat:
                        jdates_blocks[key].append([])
                    jdates_blocks[key][-1].append(jdates[key][i])
            else:
                # For arc, std, and target, just ALWAYS put them in
                # different RBs. (> 0) for i in range(1, len(jdates[key])):
                for i in range(1, len(jdates[key])):
                    if jdates[key][i] - jdates_blocks[key][-1][0] > 0:
                        jdates_blocks[key].append([])
                    jdates_blocks[key][-1].append(jdates[key][i])

        for infile in self.infiles:
            if self.key_objname in infile[0].header: # if it is an object file
                current_jdate = ""
                for key in jdates:
                    if self.value_objname[key].match(infile[0].header[self.key_objname]) is not None:
                        # Find the correct keyword.

                        for i in range(len(jdates_blocks[key])):
                            jday = Time(
                                infile[0].header[self.key_jday],
                                format = self.key_jday_unit,
                                # Converts any time format to mjd.
                            ).mjd
                            if jday <= jdates_blocks[key][i][-1]:
                                # if within boundaries of this block
                                current_jdate = str( np.mean(jdates_blocks[key][i]) )
                                break
                        break

                current_objname = infile[0].header[self.key_objname]
                current_gain = self._get_gain(infile)

                if self.value_objname["bias"].match(infile[0].header[self.key_objname]) is not None:
                    current_objtype = "bias"
                    current_grism = current_mask = ""
                elif self.value_objname["flat"].match(infile[0].header[self.key_objname]) is not None:
                    current_objtype = "flat"
                elif self.value_objname["arc"].match(infile[0].header[self.key_objname]) is not None:
                    current_objtype = "arc"
                elif self.value_objname["std"].match(infile[0].header[self.key_objname]) is not None:
                    current_objtype = "std"
                else:
                    current_objtype = "target"

                if not self.value_objname["bias"].match(infile[0].header[self.key_objname]) is not None:
                    current_grism = self._get_grism(infile)
                    if self.key_mask is None:
                        current_mask = "none"
                    else:
                        try:
                            current_mask  = str(infile[0].header[self.key_mask])
                        except KeyError:
                            raise KeyError(
                                "The mask keyword '%s' does not exist in the header of '%s'. "
                                "Set the correct name in <mask><name> in the "
                                "instrument XML-file or leave the field blank to "
                                "ignore masks (assumes every file has the same mask)."
                                % (self.key_grism, infile.filename())
                            )

                if current_objtype in airmasses_keys_ordered:
                    current_airmass = str(infile[0].header[self.key_airmass])
                    current_ra = Angle(
                        str(infile[0].header[self.key_ra]),
                        unit = "deg"
                    ).to("deg").value
                    current_dec = Angle(
                        str(infile[0].header[self.key_dec]),
                        unit = "deg"
                    ).to("deg").value
                else:
                    current_airmass = ""
                    current_ra = ""
                    current_dec = ""

                (RB_dict
                    .setdefault(current_objtype, {})
                    .setdefault(current_objname, {})
                    .setdefault(current_jdate, {})
                    .setdefault(current_gain, {})
                    .setdefault(current_grism, {})
                    .setdefault(current_mask, {})
                    .setdefault(current_airmass, {})
                    .setdefault(current_ra, {})
                    .setdefault(current_dec, []).append(infile.filename())
                )
        self.RBs = dict_to_RBs(RB_dict)


    def _get_gain(self, infile):
        """The gain string will be sorted as in the HDU, excluding the gains
        of chips that are not in xml_i.chip_extname_to_use. This is the same
        sorting as in xml_i.chips_list, that will be made in
        ReductionBlock.
        """
        gain = ""

        if self.gain_type == "fixed":
            if len(infile) == 1:  # Single HDU FITS.
                start = 0
            else:  # Several HDU FITS, first HDU does not contain image.
                start = 1
            for CCD in infile[start:]:
                if CCD.header[self.key_extname] not in self.xml_i.chip_extname_to_use:
                    continue
                for id_, chip in self.xml_i.chips.iteritems():
                    if CCD.header[self.key_extname] == chip.extname:
                        gain += str(chip.gain) + "-"
                        break
            return gain.strip("-")

        elif self.gain_type == "exact":
            try:
                for CCD in infile[1:]:
                    if CCD.header[self.key_extname] not in self.xml_i.chip_extname_to_use:
                        continue
                    gain += "%g-" % float(CCD.header[self.key_gain])
            except ValueError:
                print (
                    "Gain keyword '%s' in CCD header is not a float. "
                    "Should you be using gain type 'name' instead of 'exact'?. "
                    "Trying in primary header."
                    % str(CCD.header[self.key_gain])
                )
                gain = "%g-" % float(infile[0].header[self.key_gain])
                gain *= len(self.xml_i.chip_extname_to_use) - 1  # The gain is equal for each CCD.
            except KeyError:
                print "No gain keyword in CCD headers. Trying in primary header."
                gain = "%g-" % float(infile[0].header[self.key_gain])
                gain *= len(self.xml_i.chip_extname_to_use) - 1
            return gain.strip("-")

        elif self.gain_type == "name":
            try:
                for CCD in infile[1:]:
                    if CCD.header[self.key_extname] not in self.xml_i.chip_extname_to_use:
                        continue
                    success = False
                    for key in self.root["instrument"]["gain-name-mapping"].keys():
                        if key in str(CCD.header[self.key_gain]):
                            gain += "%g-" % float(self.root["instrument"]["gain-name-mapping"][key])
                            success = True
                            break  # Go to next CCD.
                    if not success:
                        raise KeyError  # Try primary header instead.
            except KeyError:
                success = False
                for key in self.root["instrument"]["gain-name-mapping"].keys():
                    if key in str(infile[0].header[self.key_gain]):
                        gain = "%g-" % float(self.root["instrument"]["gain-name-mapping"][key])
                        gain *= len(self.xml_i.chip_extname_to_use) - 1
                        success = True
                        break  # Finished
                if not success:
                    print (
                        "KeyError: "
                        "The gain '%s' was not recognized in the existing gain-names. "
                        "Maybe you need to add a new gain-name to the "
                        "gain-name-mapping list in the instrument XML file?\n"
                        "These are the existing gain-names:\n%s"
                        % (infile[0].header[self.key_gain],
                            self.root["instrument"]["gain-name-mapping"].keys(),
                        )
                    )
                    raise KeyError
            return gain.strip("-")


    def _get_grism(self, infile):
        """TODO: Write this docstring."""

        if self.grism_type == "fixed":
            return self.key_grism

        elif self.grism_type == "name":
            if self.key_grism is None:
                return "none"
            else:
                try:
                    return str(infile[0].header[self.key_grism])
                except KeyError:
                    raise KeyError(
                        "The grism keyword '%s' does not exist in the header of '%s'. "
                        "Set the correct name in <grism><name> in the "
                        "instrument XML-file or leave the field blank to "
                        "ignore grisms (assumes every file has the same grism)."
                        % (self.key_grism, infile.filename())
                    )
            # try:
                # for CCD in infile[1:]:
                    # if CCD.header[self.key_extname] not in self.xml_i.chip_extname_to_use:
                        # continue
                    # success = False
                    # for key in self.root["instrument"]["grism-name-mapping"].keys():
                        # if key in str(CCD.header[self.key_grism]):
                            # grism += "%g-" % float(self.root["instrument"]["grism-name-mapping"][key])
                            # success = True
                            # break  # Go to next CCD.
                    # if not success:
                        # raise KeyError  # Try primary header instead.
            # except KeyError:
                # success = False
                # for key in self.root["instrument"]["grism-name-mapping"].keys():
                    # if key in str(infile[0].header[self.key_grism]):
                        # grism = "%g-" % float(self.root["instrument"]["grism-name-mapping"][key])
                        # grism *= len(self.xml_i.chip_extname_to_use) - 1
                        # success = True
                        # break  # Finished
                # if not success:
                    # print (
                        # "KeyError: "
                        # "The grism '%s' was not recognized in the existing grism-names. "
                        # "Maybe you need to add a new grism-name to the "
                        # "grism-name-mapping list in the instrument XML file?\n"
                        # "These are the existing grism-names:\n%s"
                        # % (infile[0].header[self.key_grism],
                            # self.root["instrument"]["grism-name-mapping"].keys(),
                        # )
                    # )
                    # raise KeyError
            # return grism.strip("-")

        else:
            raise TypeError(
                "Grism type '%s' not recognized. "
                "Should be either 'name' or 'fixed'."
                % self.grism_type
            )


    def limit_reduction_block_size(self):
        """Since there is a maximum limit for the number of biases and
        flats. The RB-files for these will be read and modified if necessary.
        This method is used to limit the .txt version of Reduction Blocks only.
        """

        ok = True
        for dirname, dirnames, filenames in os.walk("blocks"):
            for filename in filenames:
                filepath = os.path.join(dirname, filename)
                if filepath.endswith(".txt"):

                    if filename.startswith("bias"):
                        f = open(filepath, "r")
                        lines = f.readlines()
                        f.close()
                        f = open(filepath, "w")
                        i = 0
                        for line in lines:
                            f.write(line)
                            i += 1
                            if i >= self.no_bias[1]:
                                # Maximum number of biases reached.
                                break
                        f.close()
                        if i < self.no_bias[0]:
                            # Not enough biases found.
                            if tkMessageBox.askyesno(
                                "Error",
                                "The reduction block '%s' only contains %d biases. "
                                "At least %d biases are required."
                                "\n\n"
                                "Continue running the program?\n"
                                "'No' will terminate."
                                % (filename, i, self.no_bias[0])
                            ):
                                ok = False
                            else:
                                sys.exit(1)

                    elif filename.startswith("flat"):
                        f = open(filepath, "r")
                        lines = f.readlines()
                        f.close()
                        f = open(filepath, "w")
                        i = 0
                        for line in lines:
                            f.write(line)
                            i += 1
                            if i >= self.no_bias[1]:
                                # Maximum number of flats reached.
                                break
                        f.close()
                        if i < self.no_flat[0]:
                            # Not enough biases found.
                            if tkMessageBox.askyesno(
                                "Error",
                                "The reduction block '%s' only contains %d flats. "
                                "At least %d flats are required."
                                "\n\n"
                                "Continue running the program?\n"
                                "'No' will terminate."
                                % (filename, i, self.no_flat[0])
                            ):
                                ok = False
                            else:
                                sys.exit(1)

        if ok:
            return True
        else:
            return False




if __name__ == "__main__":
    """
    Sample run.
    """

    machine = RBMake()
    machine.set_path_raw(
        os.path.expanduser("~/practice/GTC59-14A/"),
        "spextractor-generic.xml",
    )
    machine.set_path_rbs(
        os.path.expanduser("~/practice/test_results/"),
    )
    machine.read_xml()
    machine.find_files()
    machine.find_keywords()
    machine.extract_data()
