import numpy as np
from matplotlib import pyplot as plt

from Functions import polynomial, interpolate, RMSE, gauss



class FluxCalibrate:

    def __init__(self,
        filename_target = "target filename not given",
        filename_std = "std filename not given",
    ):
        self.filename_target = filename_target
        self.filename_std = filename_std
        self.data = [[], []] # placeholder for spectrum datasets
        self.data_interpolated = [[], [], []]
        self.polyfit_order = 6


    def set_data_raw(self, x, y=None):
        """
        Expects an array pair describing wavelengths and fluxes in a spectrum
        that is to be flux calibrated.
        x: array of wavelengths
        y: array of intensities for corresponding wavelengths
        If y is not given it is assumed that x is a length 2 list or tuple
        containging both x and y.
        """
        if y is not None:
            self.data[0] = [x, y]
        else:
            self.data[0] = [x[0], x[1]]

    def set_data_calibration(self, x, y=None):
        """
        Expects an array pair describing wavelengths and fluxes in a standard
        spectrum with known wavelengths used for calibrations.
        x: array of wavelengths
        y: array of intensities for corresponding wavelengths
        If y is not given it is assumed that x is a length 2 list or tuple
        containging both x and y.
        """
        if y is not None:
            self.data[1] = [x, y]
        else:
            self.data[1] = [x[0], x[1]]


    def _interpolate(self):

        data0 = interpolate(self.data[0][0], self.data[0][1], self.data[1][0])
        data1 = self.data[1][0], self.data[1][1]
        limits = data0[2]

        self.data_interpolated[0] = data0[0]
        self.data_interpolated[1] = data0[1]
        self.data_interpolated[2] = data1[1][limits[0] : limits[1]+1]


    def get_calibrate(self):
        """
        Returns the coefficients of the calibration polynomial.
        """
        self._interpolate()

        x = self.data_interpolated[0]
        y_ratio = self.data_interpolated[1] / self.data_interpolated[2]

        p_opt = np.polyfit(x, y_ratio, self.polyfit_order)

        return p_opt, RMSE(y_ratio, polynomial(x, p_opt))



if __name__ == "__main__":
    """
    Sample use of this module with simple dummy data.
    Useful for testing, debugging and demonstration.
    """

    data = [[np.linspace(500, 1000, 512)], [np.linspace(600, 1100, 1024)]]
    data[0].append(gauss(data[0][0], 1.50, 800, 50))
    data[1].append(gauss(data[1][0], 1.25, 800, 75))

    flux_calibrator = FluxCalibrate()
    flux_calibrator.set_data_raw(data[0][0], data[0][1])
    flux_calibrator.set_data_calibration(data[1][0], data[1][1])
    p_opt, error = flux_calibrator.get_calibrate()
    y_ratio = flux_calibrator.data_interpolated[1] \
            / flux_calibrator.data_interpolated[2]

    plt.hold("on")
    plt.plot(flux_calibrator.data[0][0], flux_calibrator.data[0][1], "ro",
             flux_calibrator.data[1][0], flux_calibrator.data[1][1], "ro")

    plt.plot(flux_calibrator.data_interpolated[0],
             flux_calibrator.data_interpolated[1], "b+",
             flux_calibrator.data_interpolated[0],
             flux_calibrator.data_interpolated[2], "b+")
    plt.title("'o'= sample data, '+'=interpolated data" % error)

    plt.figure()
    plt.plot( flux_calibrator.data_interpolated[0],
              y_ratio, "b",
              flux_calibrator.data_interpolated[0],
              polynomial(flux_calibrator.data_interpolated[0], p_opt), "r"
               )
    plt.title("RMSE = %f" % error)

    plt.show()

