import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits

import Functions as func


class ReductionBlock:
    """Object for holding reduction block information. This class has two
    main uses: It holds the list of filenames that represent ONE reduction
    block, and it can also read these files and thus hold the actual data
    from the files in this reduction block.

    A ReductionBlock instance can write itself into a file, and thus saving
    itself and will survive after the program terminates. The same,
    identical ReductionBlock instance can later be recreated from this file.

    The data is automatically loaded when, and only when, they are actively
    attempted accessed externally. Therefore it is very quick to create a
    ReductionBlock instance, and it can still be assumed that the data is
    always there, and the memory will only be used when you actually use it.
    """

    OBJTYPES = (
        "bias",
        "flat",
        "arc",
        "std",
        "target",
    )
    OBJTYPES_DISCARDED = (
        "discarded_bias",
        "discarded_flat",
        "discarded_arc",
        "discarded_std",
        "discarded_target",
    )

    area_sci  = []
    area_osc = []
    path = None
    instances = []  # Dynamic list containing all instances of this class.


    @classmethod
    def set_path(cls, path):
        """Path of the folder that is to contain the reduction block files.
        The folder will be made if it does not alread exist.
        """
        cls.path = path
        func.make_folder(path)


    @classmethod
    def write_to_files(cls, path=None):
        """Calls write_to_file for every RedectionBlock instance, i.e. makes
        all reduction block files.
        """
        if path is not None:
            cls.set_path(path)
        for RB in cls.instances:
            RB.write_to_file(path)


    def __init__(self,
        # Alternative 3:
        objtype = None,
        objname = None,
        gain = None,
        jdate = None,
        grism = None,
        maskname = None,
        airmass = None,
        ra = None,
        dec = None,
        # Alternative 1:
        keylist = None,
        # Alternative 2:
        infile = None,
        # Where to store self to file:
        path = None,

    ):
        """A ReductionBlock instance can be initialized in 3 different ways
        (in the following priority, if more than one method is attempted at
        once):
        1.  If keylist is given the other arguments are ignored. keylist is
            an ordered list of the first 6 arguments.
        2.  If infile is given the other arguments are ignored. infile is
            the filename of a .txt reduction block. The 6 arguments will be
            read from infile.name.
        3.  Give each of the 6 parameters as individual arguments (not all
            of them are relevant for all object types).
        """

        self.data_raw = [] # Raw image.
        self.data_sci = [] # Sliced and overscan corrected.
        self.data_osc = [] # Overscan.
        self.filenames_discarded = []


        if keylist is not None:
            self.keylist_to_params(keylist)
            self.filenames = []

        elif infile is not None:
            if isinstance(infile, basestring):
                filename = infile
                infile = open(filename, "r")
            elif isinstance(infile, file):
                filename = infile.name
            else:
                raise TypeError(
                    "infile must be a string (a filename), or a file object. "
                    "Got %s instead." % type(infile)
                )
            keylist = filename.rstrip(".txt").split("/")[~0].split("__")
            self.keylist_to_params(keylist)
            self.filenames = [line.rstrip() for line in infile.readlines()]
            # These are the filenames in the reduction block.

        else:
            self.objtype = objtype
            self.objname = objname
            self.jdate = float(jdate)
            self.gain = np.array(gain.split("-")).astype(float)
            self.grism = grism
            self.maskname = maskname
            self.airmass = func.xfloat(airmass)
            self.ra = func.xfloat(ra)
            self.dec = func.xfloat(dec)
            self.check_objtype()
            self.filenames = []

        # On class level:
        if path is not None:
            self.set_path(path)
        self.instances.append(self)


    def keylist_to_params(self, keylist):
        """Converts a list of arguments into splitting it up into its (up
        to) 8 individual arguments and assigning them individually.
        """
        for i in xrange(9):
            if not keylist[i]:
                keylist[i] = None
                # Set empty value to None.
        self.objtype = keylist[0]
        self.objname = keylist[1]
        self.jdate = float(keylist[2])
        self.gain = np.array(keylist[3].split("-")).astype(float)
        self.grism = keylist[4]
        self.maskname = keylist[5]
        self.airmass = func.xfloat(keylist[6])
        self.ra = func.xfloat(keylist[7])
        self.dec = func.xfloat(keylist[8])
        self.check_objtype()


    def check_objtype(self):
        """Checks that the initialized instance is of a valid type. If it is
        not, problems will probably arise somewhere when this instance is
        attempted used. This check is an extra safety measure in case
        ReductionBlock is used in a unintended way.
        """
        if (self.objtype not in self.OBJTYPES and
            self.objtype not in self.OBJTYPES_DISCARDED
        ):
            print (
                "Warning: %s is not a recognized objtype. "
                "Should be one of"
                % (self.objtype),
                self.OBJTYPES
            )


    def add_filenames(self, filenames):
        """Adds one or several filenames to this reduction block. This is
        how images are grouped together as one reduction block.
        filenames: string or sequence of strings
        """
        if isinstance(filenames, basestring):
            self.filenames.append(filenames)
        elif hasattr(filenames, "__iter__"):
            for filename in filenames:
                self.filenames.append(filename)
        else:
            raise TypeError(
                "Expected input is a string or sequence of strings of filenames."
            )


    def set_filenames(self, filenames):
        """Same as add_files, but clears self.filenames first."""
        self.filenames = []
        self.add_filenames(filenames)


    def get_write_name(self, extension=".txt"):
        """The full name of this reduction block. Contains all its
        parametsers. This is the filename it will have.
        """

        # Limit the number of decimal points of the floats:
        if self.jdate:
            jdate = "%.5f" % self.jdate
            # Need precision of .5 so that every second is unique.
        else:
            jdate = self.jdate
        if self.airmass:
            airmass = "%.3f" % self.airmass
        else:
            airmass = self.airmass
        if self.ra:
            ra = "%.3f" % self.ra
        else:
            ra = self.ra
        if self.dec:
            dec = "%.3f" % self.dec
        else:
            dec = self.dec

        # Write gain in the format 'xx-xx-etc.' since it is an array:
        gain = str(self.gain[0])
        for value in self.gain[1:]:
            gain += "-" + str(value)

        return "/%s__%s__%s__%s__%s__%s__%s__%s__%s%s" % (
            self.objtype,
            self.objname.replace("/", "_"),  # "/" not allowed in filenames.
            jdate,
            gain,
            self.grism,
            self.maskname,
            airmass,
            ra,
            dec,
            extension,
        )


    def write_to_file(self, path=None, sanity=True):
        """Makes a file containing this ReductionBlock. This is how the
        reduction blocked is stored beyond runtime for later use and
        examination.
        """
        if sanity and self.objtype in ("bias", "flat"):
            # Sanity check on biases and flats.
            self.sanity_check()
        if path is None:
            if self.path is None:
                raise IOError("No output path specified.")
            path = self.path
        outfile = open(
            path + self.get_write_name(),
            "w",
        )
        for filename in self.filenames:
            outfile.write("%s\n" % filename)
        outfile.close()


    def discard_to_file(self, path=None, force=False):
        """Makes a file containing the discarded images of this ReductionBlock.
        force: (bool) Create the discard RB even if no files were discarded.
        """

        if not self.filenames_discarded and not force:
            # No need to make the discard RB.
            return

        if path is None:
            if self.path is None:
                raise IOError("No output path specified.")
            path = self.path

        outfile = open(
            path + "/discarded_" + self.get_write_name().lstrip("/"),
            "w",
        )
        for filename in self.filenames_discarded:
            outfile.write("%s\n" % filename)
        outfile.close()


    def sanity_check(self):
        """A check on biases on flats to see that their values are within
        the accepted range.
        """

        if self.objtype == "bias":
            if not self.xml_i.bias_sigclip_check:
                # No sigma clipping check on biases.
                return
            if self.xml_i.bias_method in ("median", "med"):
                method = "median"
            elif self.xml_i.bias_method in ("mean", "average", "avg"):
                method = "mean"
            else:
                raise NotImplementedError(
                    "Only method 'median' and 'mean' supported so far."
                )
            for k in range(self.xml_i.bias_sigclip_iter):
                discard = []
                for j in range(len(self.get_data(imageno=0, sanity=False))):
                    if method == "median":
                        biaspoints = np.median(
                            self.get_data(CCDno=j, sanity=False), axis=(1,2)
                        )
                        centrum = np.median(biaspoints)
                    elif method == "mean":
                        biaspoints = np.mean(
                            self.get_data(CCDno=j, sanity=False), axis=(1,2)
                        )
                        centrum = np.mean(biaspoints)
                    std = np.sqrt(np.mean((biaspoints - centrum)**2))
                    for i, biaspoint in enumerate(biaspoints):
                        if not (
                            centrum - self.xml_i.bias_sigclip_thresh*std <
                            biaspoint <
                            centrum + self.xml_i.bias_sigclip_thresh*std
                        ):
                            # If the image is outside this range it is not accepted.
                            if not i in discard:
                                # Avoid adding duplicates, since we do it for
                                # all CCDs.
                                discard.append(i)
                for i in discard[::-1]:
                    self.filenames_discarded.append(self.filenames[i])
                    del self.filenames[i]
                    del self.data_raw[i]
                    del self.data_sci[i]
                    del self.data_osc[i]
                    print "Discarded a bias."
                    #TODO Make "discarded RB".

        elif self.objtype == "flat":
            if not self.xml_i.flat_linearity_check:
                # No saturation check on flats.
                return
            discard = []
            for i in range(len(self.get_data(CCDno=0, sanity=False))):
                for j in range(len(self.get_data(imageno=0, sanity=False))):
                    # Just to get the size/shape.
                    if (np.sum(
                            self.get_data(imageno=i, CCDno=j, sanity=False) >
                            0.9 * self.xml_i.chips_list[j].saturation
                        ) / self.get_data(imageno=i, CCDno=j, sanity=False).size >
                        self.xml_i.flat_linearity_lim
                    ):
                        # More than 0.1 of the pixels in this CCD has
                        # intensity over saturation limit. Discard this entire
                        # image. Break loop and not check rest of the CCDs in
                        # this image; it will be discarded anyway and it would
                        # cause duplicates in the discard list.
                        discard.append(i)
                        break
            for i in discard[::-1]:
                self.filenames_discarded.append(self.filenames[i])
                del self.filenames[i]
                del self.data_raw[i]
                del self.data_sci[i]
                del self.data_osc[i]
                print "Discarded a flat."
                #TODO Make "discarded RB".

        self.discard_to_file(self.path)


    def get_data(self, imageno=None, CCDno=None, sanity=True):
        """Returns the contents in self.data. If the data has not yet been
        loaded, load them first and then return. Because of this automatic
        loading system it can always be assumed that the data is there.
        By giving indexes to the optional parameters you can choose how much
        to return.
        return (default):
            list of images:
                each image is a list of CCDs:
                    each CCD is a 2D np.array representing an actual image
        """

        if sanity and self.objtype in ("bias", "flat"):
            # Sanity check on biases and flats.
            self.sanity_check()

        if len(self.data_sci) == 0:
            if self.filenames:
                self.load_data()
            else:
                raise IOError("No filename given. Cannot load data.")
        else:
            if len(self.data_sci) != len(self.filenames):
                print "Warning: Filenames have changed. Data reloaded."
                self.load_data()

        if imageno is not None:
            if CCDno is not None:
                return self.data_sci[imageno][CCDno] # Specific image and CCD.
            else:
                return self.data_sci[imageno] # Specific image.
        else:
            if CCDno is not None:
                return [image[CCDno] for image in self.data_sci] # Specific CCD.
            else:
                return self.data_sci # Everything.


    def load_data(self, crop=True, calibrate=True):
        """Reads the FITS-files and stores the raw data in a list of arrays.
        Also automatically crops out the science- and overscan areas and
        performs the overscan calibration so that only the relevant part of
        the files is accessed unless otherwise specified.
        """

        if not self.filenames:
            raise IOError("load_data attempted, but no filename is given.")
            return False
        for filename in self.filenames:
            self.data_raw.append([])
            HDU = fits.open(filename)
            jj = 0
            if len(HDU) == 1:  # Single HDU FITS.
                start = 0
            else:  # Several HDU FITS, first HDU does not contain image.
                start = 1
            for j, CCD in enumerate(HDU[start:]): # [1:] skips header.
                success = False
                if CCD.header[self.xml_i.key_extname] in self.xml_i.chip_extname_to_use:
                    for id_, chip in self.xml_i.chips.iteritems():
                        if CCD.header[self.xml_i.key_extname] == chip.extname:
                            self.data_raw[~0].append(CCD.data)
                            if self.xml_i.chips[id_] not in self.xml_i.chips_list:
                                self.xml_i.chips[id_].index = jj
                                self.xml_i.chips_list.append(self.xml_i.chips[id_])
                                    # A list pointing to the same chips as
                                    # the dict, but ordered according to their
                                    # order in the HDU.
                            jj += 1
                            success = True
                    if not success:
                        raise ValueError(
                            "Could not find the extname '%s' in this CCDs "
                            "FITS header."
                            % CCD.header[self.xml_i.key_extname]
                        )
            HDU.close()

        if crop:
            self.slice_data()
        if calibrate:
            self.overscan_calibrate()


    def slice_data(self):
        """Crops out the science quadrant of the image and crops out the
        overscan quadrant of the image. This method is intended to be called
        automatically when the images are loaded, but should it be called
        manually before the images are loaded they will be loaded automatically.
        """
        if len(self.data_raw) == 0:
            self.load_data(crop=False, calibrate=False)
        for i, image_raw in enumerate(self.data_raw):
            self.data_sci.append([])
            self.data_osc.append([])
            for j, chip in enumerate(self.xml_i.chips_list):
                self.data_sci[~0].append(
                    self.data_raw[i][j][
                        chip.area_sci[0] : chip.area_sci[0] + chip.area_sci[1],
                        chip.area_sci[2] : chip.area_sci[2] + chip.area_sci[3],
                    ]
                )
                self.data_osc[~0].append(
                    self.data_raw[i][j][
                        chip.area_osc[0] : chip.area_osc[0] + chip.area_osc[1],
                        chip.area_osc[2] : chip.area_osc[2] + chip.area_osc[3],
                    ]
                )


    def overscan_calibrate(self, show=False):
        """Performs overscan correction on the science quarant of the image.
        This method i intended to be called automatically when the images
        are loaded, but should it be called manually before the images are
        cropped this will be done automatically, as it strictly necessary to
        crop out the overscan to perform the calibration.
        If show is true each overscan and its polyfit is plotted.
        """
        if len(self.data_sci) == 0 or len(self.data_osc) == 0:
            self.slice_data()

        for i, image_osc in enumerate(self.data_osc):
            for j, CCD_osc in enumerate(image_osc):

                # Find in which direction the overscan is oriented:
                # (This is done more often than needed.)
                if self.xml_i.chips_list[j].area_osc[1] > self.xml_i.chips_list[j].area_osc[3]:
                    axis = 0  # Elongated in y-direction.
                else:
                    axis = 1  # Elongated in x-direction.

                current_overscan = np.mean(CCD_osc, axis=not axis)
                x_vals = np.linspace(
                        0,
                        current_overscan.size - 1,
                        current_overscan.size,
                )
                p_opt = np.polyfit(
                    x_vals,
                    current_overscan,
                    5,
                )
                current_poly = func.polynomial(x_vals, p_opt)

                if show:
                    plt.hold("on")
                    plt.plot(current_overscan, "+b")
                    plt.plot(current_poly,     "-r")
                    plt.title("%s, image=%d, CCD=%d, osc=%f, sci=%f" % (
                        self.objname,
                        i,
                        j,
                        current_overscan.mean(),
                        self.data_sci[i][j].mean(),
                    ))
                    plt.show()

                if axis == 0:
                    self.data_sci[i][j] -= current_poly[:, None]
                elif axis == 1:
                    self.data_sci[i][j] -= current_poly[:]


    def __str__(self):
        """A pretty display of all the metadata contained in this instance.
        If the data has not been loaded the data shape will be (0, 0).
        """
        s = "ReductionBlock:"
        s += "\n    " + "Parameters:"
        s += "\n        " + str(self.objtype)
        s += "\n        " + str(self.objname)
        s += "\n        " + str(self.gain)
        s += "\n        " + str(self.jdate)
        s += "\n        " + str(self.grism)
        s += "\n        " + str(self.maskname)
        s += "\n        " + str(self.airmass)
        s += "\n        " + str(self.ra)
        s += "\n        " + str(self.dec)
        s += "\n    " + "Filenames:"
        for filename in self.filenames:
            s += "\n        " + filename
        s += "\n    " + "Shape of raw data:"
        s += "\n        " + "%s" % str(np.shape(self.data_raw))
        s += "\n    " + "Shape of science data:"
        if self.data_sci:
            CCDdim = len(self.data_sci[0])
        else:
            CCDdim = 0
        s += "\n        " + "(%d, %d" % (len(self.data_sci), CCDdim)
        if CCDdim:
            for j, CCD in enumerate(self.data_sci[0]):
                s +=  "\n            CCD%2d: %s" % (j+1, str(CCD.shape))
            s += "\n        "
        s += ")"
        return s



if __name__ == "__main__":
    """Sample run."""

    pass
