class ReductionBlockSet:
    """Is to contain one ReductionBlock of each type: A target and the the
    best fit representative of each of the others.
    """

    instances = []  # Dynamic list containing all instances of this class.
    instances_target = []
    instances_std    = []

    def __init__(self,
        bias = None,
        flat = None,
        arc = [],
        std = None,
        target = None,
        is_std = None,
    ):
        """
        is_std: (bool) Defines this RBset as a standard star if True,
            as a target if False.
        """
        self.bias = bias
        self.flat = flat
        self.arc = list(arc)
        self.std = std
        self.target = target
        self.is_std = is_std
        # On class level:
        self.instances.append(self)
        if is_std is True:
            self.instances_std.append(self)
        elif is_std is False:
            self.instances_target.append(self)

    def turn_std(self):
        """Defines this RBset as a standard star."""
        self.instances_std.append(self)
        self.is_std = True

    def turn_target(self):
        """Defines this RBset as a standard star."""
        self.instances_target.append(self)
        self.is_std = False
