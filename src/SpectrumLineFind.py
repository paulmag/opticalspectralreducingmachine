import sys
import os
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib.widgets import Button

from astropy.io import fits
import xmltodict
import Tkinter
import tkMessageBox
Tkinter.Tk().withdraw()

import Functions as func



class SpectrumLineFind:
    """A pre-reduction step.  Finds the position (one coordinate) of the
    spectral line so that the Reduction module has a good initial guess.
    Also detects which CCD(s) the spectrum is located in and produces the
    lists of CCD(s) that is relevant for further reduction.
    """

    def __init__(self, filename="filename not given", axis_spec=None):
        self.filename = filename
        self.path_xml = None
        self.infiles = []
        self.target  = []
        self.root = None # root of the XML-tree
        self.line_position = []
        self.position_spec = None
        self.CCD_index_to_keep = []
        self.CCD_id_to_keep = []
        self.CCD_extname_to_keep = []
        self.explanation = (
            "You have chosen to always use interactive mode. "
            "Select the spectrum manually."
        )
        if axis_spec is not None:
            self.axis_spec = axis_spec
        if self.axis_spec not in (0, 1):
            raise ValueError(
                "<dispaxis> must be '0' or '1'. "
                "Which way is the spectrum trace oriented?"
            )

    def set_path_xml(self, p):
        """
        p = path of the instrument XML-file
        """
        self.path_xml = p


    def read_xml(self, p=False):
        """
        Makes a dictionairy containting the entire XML-file.
        The direct path to this file can be given as an argument, but it's not
        necessary if the path is already set.
        """
        if not p:
            p = self.path_xml

        infile = open(p, "r")
        tree = xmltodict.parse(infile)
        infile.close()
        self.root = tree["spextractor"]
        self.geo_chip = self.root["instrument"]["chip-geometry"]


    def set_shape(self):

        N = self.xml_i.N  # Full shape of instrument.

        # Start with one chip:
        target_ordered = [self.target[self.xml_i.chips_list[0].index]]
        chips_ordered = [self.xml_i.chips_list[0]]
        y_max = y_min = chips_ordered[0].coords[0]
        x_max = x_min = chips_ordered[0].coords[1]

        # Insert the rest sorted:
        for chip_new in self.xml_i.chips_list[1:]:
            for j, chip_old in enumerate(chips_ordered):
                if (chip_new.coords[0]*N[0] + chip_new.coords[1] <
                    chip_old.coords[0]*N[0] + chip_old.coords[1]
                ):
                    chips_ordered.insert(j, chip_new)
                    target_ordered.insert(j, self.target[chip_new.index])
                    break
                elif j == len(chips_ordered)-1:  # It goes behind the last one.
                    chips_ordered.append(chip_new)
                    target_ordered.append(self.target[chip_new.index])
                    break

            if chip_new.coords[0] > y_max:
                y_max = chip_new.coords[0]
            elif chip_new.coords[0] < y_min:
                y_min = chip_new.coords[0]
            if chip_new.coords[1] > x_max:
                x_max = chip_new.coords[1]
            elif chip_new.coords[1] < x_min:
                x_min = chip_new.coords[1]

        self.N = 1+y_max-y_min, 1+x_max-x_min
            # New shape if not all chips are used.
        self.target = target_ordered
        self.chips_ordered = chips_ordered


    def find_position(self, mode="semi", max_attempts=50):
        """Tries to automatically find the position of the spectrum in each
        CCD by searching for a bright line with the correct orientation.  If
        no line is found a graphical interface opens and the user is force
        to manually select where the spectrum is.  If more than one line is
        found the same thing happens, but the user gets the comfort of
        seeing the positions of the lines that was founds so that he may
        simply select one of them.
        """

        if "semi" in mode:
            mode = "semi"  # semi-auto
        elif ("manual" in mode) or ("interactive" in mode):
            mode = "manual"
        elif "auto" in mode:
            mode = "auto"

        x_vals = []
        y_vals = []
        self.eps = []
        self.xy_vals = x_vals, y_vals
        for Y in range(self.N[0]):
            for X in range(self.N[1]):
                y_vals.append(np.median(
                    self.target[Y*self.N[1] + X],
                    axis=(self.axis_spec),
                ))
                x_vals.append(np.linspace(
                    0,
                    y_vals[-1].size - 1,
                    y_vals[-1].size,
                ))
                # A peak must at least be 5 times std of noise:
                self.eps.append(func.tmedian(
                    y_vals[~0],
                    iterations = 7,
                    return_array = True
                ).std() * 5)
        y_vals_copy = []
        for value in y_vals:
            y_vals_copy.append(value.copy())
        CCDno = 0
        counter = 0
        while True:
            try:
                if counter >= max_attempts:
                    raise RuntimeError  # Cannot find any more peaks.
                counter += 1
                # Initial fit:
                p_opt, p_cov = curve_fit(
                    func.moffat,
                    x_vals[CCDno],
                    y_vals_copy[CCDno],
                    [   np.max(y_vals_copy[CCDno]),
                        x_vals[CCDno][np.argmax(y_vals_copy[CCDno])],
                        3.,
                        1.,
                        func.tmedian(y_vals[CCDno]),
                    ],
                )
                radius = max(3*p_opt[2], 7)  # Need minimum 7 points for the fit.
                treshold = func.treshold(
                    (np.floor(p_opt[1] - radius), np.ceil(p_opt[1] + radius)),
                    low = x_vals[CCDno][ 0],
                    high= x_vals[CCDno][~0],
                )
                # Refined fit:
                p_opt, p_cov = curve_fit(
                    func.moffat,
                    x_vals[CCDno][treshold[0]:treshold[1]],
                    y_vals_copy[CCDno][treshold[0]:treshold[1]],
                    list(p_opt) + [0],  # + [0] because we now also fit the slope.
                )
                if (p_opt[0] > self.eps[CCDno] and
                    x_vals[CCDno][0] < p_opt[1] < x_vals[CCDno][-1]
                    # A real peak must be >eps and within the image.
                ):
                    self.line_position.append([CCDno, p_opt[1], p_opt[0]])
                    # Subtract the current peak so that the next one can be
                    # found:
                    radius = 2 * p_opt[2]
                    treshold = func.treshold(
                        (np.floor(p_opt[1] - radius), np.ceil(p_opt[1] + radius)),
                        low = x_vals[CCDno][ 0],
                        high= x_vals[CCDno][~0],
                    )
                    y_vals_copy[CCDno][treshold[0]:treshold[1]] =  \
                        func.polynomial(
                            x_vals[CCDno][treshold[0]:treshold[1]],
                            (p_opt[5], p_opt[4]),
                        )
                else:
                    # This is considered a fit fail.
                    raise RuntimeError
            except RuntimeError:
                # if curve_fit fails
                if CCDno < self.N[0]*self.N[1] - 1:
                    # Go to next CCD.
                    CCDno += 1
                    counter = 0
                else:
                    if len(self.line_position) == 1:
                        self.position_spec = self.line_position[0][1]
                        Y = CCDno / self.N[1]
                        X = CCDno % self.N[1]
                        self.select_CCD(Y, X)
                        if mode == "manual":
                            self._make_window()
                    else:
                        if len(self.line_position) < 1:
                            self.explanation = (
                                "No spectrum was found. "
                                "You have to select it manually."
                            )
                            self._make_window()
                        elif len(self.line_position) > 1:
                            if mode == "auto":
                                CCDno = np.argmax(np.array(self.line_position)[:, 2])
                                self.position_spec = self.line_position[CCDno][1]
                                Y = CCDno / self.N[1]
                                X = CCDno % self.N[1]
                                self.select_CCD(Y, X)
                            else:
                                self.explanation = (
                                    "Several spectra was found. "
                                    "You have to select one of them manually."
                                )
                                self._make_window()
                    break

    def _make_window(self):
        """Makes the interactive window for manually selecting a
        spectrum line. This is made if the user requests interactivity
        or if the auto mode is unsuccessfull in finding a single
        spectrum line.
        """

        self.fig = plt.figure(figsize=(14,10))
        self.fig.suptitle("Select the trace of the spectrum of interest", size=18)
        cid = self.fig.canvas.mpl_connect('button_press_event', self)
        imgs_height = 0.82
            # Ratio of height of figure that is used for displaying the
            # images. The rest is used for informative text and the
            # 'finish'-button.
        stack_ratio = (not self.axis_spec) * 0.185, self.axis_spec * 0.185
            # Ratio of imgs_height that will be used for the 2D-plot of the
            # stacked images.
        b_size = 0.150, 0.025
        self.ax_finish = plt.axes([
            1 - b_size[0] - 0.02,
            imgs_height * 1.02,
            b_size[0],
            b_size[1],
        ])
        self.ax_reset = plt.axes([
            1 - b_size[0] - 0.02,
            imgs_height * 1.02 + 0.050,
            b_size[0],
            b_size[1],
        ])
        self.b_finish = Button(self.ax_finish, 'Done')
        self.b_finish.on_clicked(self._finish)
        self.b_reset = Button(self.ax_reset, 'Reset')
        self.b_reset.on_clicked(self._reset)
        self.text_info = self.fig.text(
            x = 0.02,
            y = imgs_height * 1.02 + 0.0,
            s = (
                "Filename of image: '%s'\n"
                "%s\n"
                "Any spectra already detected automatically are marked with red lines.\n"
                "Click on the position of the spectrum line you want to analyse.\n"
                "If it spans several CCDs this will be detected automatically.\n"
                "When you have made your selection click 'Done' to continue."
                % (self.filename, self.explanation)
            ),
        )
        self.text_pos = self.fig.text(
            x = 1 - b_size[0]*3 - 0.02,
            y = imgs_height * 1.02 + 0.040,
            s = "Current selection(s):",
        )
        self.text_pos = self.fig.text(
            x = 1 - b_size[0]*2 - 0.02,
            y = imgs_height * 1.02 + 0.055,
            s = "",
        )
        self.text_CCD = self.fig.text(
            x = 1 - b_size[0]*2 - 0.02,
            y = imgs_height * 1.02 + 0.025,
            s = "",
        )

        self.ax = []
        self.ax_stack = []
        for Y in range(self.N[0]):
            for X in range(self.N[1]):
                self.ax.append(self.fig.add_subplot(
                    self.N[1],
                    self.N[0]*2,
                    (Y*self.N[1] + X)*2 + 1,
                ))
                self.ax_stack.append(self.fig.add_subplot(
                    self.N[1],
                    self.N[0]*2,
                    (Y*self.N[1] + X + 1)*2,
                ))
                img = self.target[Y*self.N[1] + X]
                self.ax[~0].imshow(
                    img,
                    origin = "lower",
                    cmap = plt.cm.gray,
                    vmin = np.median(img),
                    vmax = np.median(img) + (img.max() - img.min()) * 0.01,
                    interpolation = "nearest",
                    aspect = "auto",
                )
                self.ax_stack[~0].plot(
                    self.xy_vals[    self.axis_spec][Y*self.N[1] + X],
                    self.xy_vals[not self.axis_spec][Y*self.N[1] + X],
                )
                self.fig.text(
                    x = (X + 0.42) / self.N[1],
                    y = (Y + 0.94 * imgs_height) / self.N[0],
                    s = "CCD id: %d" % self.chips_ordered[Y*self.N[1] + X].id_,
                    color = "r",
                    size = 20,
                )
                self.ax[~0].set_position([
                    float(X) / self.N[1],
                    float(Y) / self.N[0] * imgs_height,
                    (1. - stack_ratio[1]) / self.N[1],
                    (1. - stack_ratio[0]) / self.N[0] * imgs_height,
                ])
                self.ax_stack[~0].set_position([
                    (X +      self.axis_spec  - stack_ratio[1]) / self.N[1],
                    (Y + (not self.axis_spec) - stack_ratio[0]) / self.N[0] * imgs_height,
                    abs((not self.axis_spec) - stack_ratio[1]) / self.N[1],
                    abs(     self.axis_spec  - stack_ratio[0]) / self.N[0] * imgs_height,
                ])
                self.ax_stack[~0].set_xlim([
                    np.min(self.xy_vals[self.axis_spec][Y*self.N[1] + X]),
                    np.max(self.xy_vals[self.axis_spec][Y*self.N[1] + X]),
                ])
                self.ax_stack[~0].set_ylim([
                    np.min(self.xy_vals[not self.axis_spec][Y*self.N[1] + X]),
                    np.max(self.xy_vals[not self.axis_spec][Y*self.N[1] + X]),
                ])
                self.ax[~0].tick_params(labelbottom="off", labelleft="off")
                self.ax_stack[~0].tick_params(labelbottom="off", labelleft="off")
                for spine in self.ax[~0].spines.itervalues():
                    spine.set_visible(False)
                for spine in self.ax_stack[~0].spines.itervalues():
                    spine.set_visible(False)

        for CCDno, linepos, amplitude in self.line_position:
            if self.axis_spec == 0:  # vertical
                self.ax[CCDno].axvline(
                    linepos,
                    linewidth = 5,
                    color = "r",
                    linestyle = "-.",
                )
                self.ax_stack[CCDno].axvline(
                    linepos,
                    linewidth = 3,
                    color = "r",
                    linestyle = "-.",
                )
            elif self.axis_spec == 1:  # horizontal
                self.ax[CCDno].axhline(
                    linepos,
                    linewidth = 5,
                    color = "r",
                    linestyle = "-.",
                )
                self.ax_stack[CCDno].axhline(
                    linepos,
                    linewidth = 3,
                    color = "r",
                    linestyle = "-.",
                )

        self._update_window()
        plt.show()


    def __call__(self, event):
        """Help to find_position.  Is called when user clicks on canvas in
        interactive mode.
        """
        position    = event.ydata, event.xdata
        position_ax = event.inaxes
        for Y in range(self.N[0]):  # Find the selected CCD.
            for X in range(self.N[1]):
                if (event.inaxes == self.ax[Y*self.N[1] + X] or
                    event.inaxes == self.ax_stack[Y*self.N[1] + X]
                ):
                    self.select_CCD(Y, X) # save relevant CCD(s)
                    if self.axis_spec == 0:
                        self.position_spec = event.xdata # save relevant position
                    elif self.axis_spec == 1:
                        self.position_spec = event.ydata
                    self._update_window()
                    return


    def _update_window(self):
        if self.axis_spec == 0:
            orientation = "x"  # Orthogonal to spectrum line.
        elif self.axis_spec == 1:
            orientation = "y"
        if self.position_spec is None:
            position_spec_display = "no selection"
        else:
            position_spec_display = "%.2f" % self.position_spec
        self.text_pos.set_text(
            "%s-position: %s"
            % (orientation, position_spec_display)
        )
        self.text_CCD.set_text(
            "CCD id: %s" % str([self.chips_ordered[j].id_ for j in self.CCD_index_to_keep])
        )
        self.fig.canvas.draw()


    def _finish(self, event):
        """Call when calibrations is complete by a button click of
        the user. Closes
        the interactive window. Returns True if everything finishes
        successfully, False if not, and then the user must continue with the interactive mode until he finished successfully.
        event: The button-click event will be passed here.
        """

        if self.position_spec is None:
            tkMessageBox.showerror(
                "Error!",
                "You have not made a selection yet, so the program is currently in an unfinished state. Select the position of the spectrum line before proceeding. \n\nIf you cannot see it and do not know where it is you must figure out where it is by other means before you can proceed, or consider if this image actually contains a spectrum or not."
            )
            return False
        else:
            # Sucessfull finish.
            plt.close()
            return True


    def _reset(self, event):
        """Removes the current selection made by the user.
        This is not an import method(button) since he can only make one
        selection at a time anyway.
        event: The button-click event will be passed here.
        """
        self.position_spec = None
        self.CCD_index_to_keep = []
        self.CCD_id_to_keep = []
        self.CCD_extname_to_keep = []
        self._update_window()


    def select_CCD(self, Y, X):
        """
        (Y, X) is a CCD position.
        Ex: (Y=1, X=2) means the bottom CCD to the right.
        Usually there will only be 2 CCDs, but an arbitrary amount of CCDs
        is possible as long as they are placed in a rectangular grid.
        This method stores the IDs of the CCD(s) that contains the spectrum.
        """
        CCD_coor_to_keep = []
        self.CCD_index_to_keep = []

        if self.axis_spec == 0: # spectrum is in y-direction
            for i in range(1, self.N[0]+1):
                CCD_coor_to_keep.append((i, X+1))

        elif self.axis_spec == 1: # spectrum is in x-direction
            for i in range(1, self.N[1]+1):
                CCD_coor_to_keep.append((Y+1, i))

        if isinstance(self.geo_chip["chip"], (list, tuple)):
            for i in range(self.N[0]*self.N[1]):
                for coor in CCD_coor_to_keep:
                    if (int(self.geo_chip["chip"][i]["chip-y"]) == coor[0] and
                        int(self.geo_chip["chip"][i]["chip-x"]) == coor[1]
                    ):
                       self.CCD_index_to_keep.append(i)
        else:  # Just one CCD.  #TODO Is this necessary then?
            for coor in CCD_coor_to_keep:
                if (int(self.geo_chip["chip"]["chip-y"]) == coor[0] and
                    int(self.geo_chip["chip"]["chip-x"]) == coor[1]
                ):
                   self.CCD_index_to_keep.append(0)



if __name__ == "__main__":
    """Sample run."""

    machine = SpectrumLineFind()
    machine.set_path_raw(
        "../../practice/GTC59-14A/",
        "spextractor-generic.xml",
        # os.path.expanduser("~/practice/GTC59-14A/"),
        # "spextractor-generic.xml",
    )
    infile = fits.open(
        machine.path +
        "OB0001/object/0000692699-20140721-OSIRIS-OsirisLongSlitSpectroscopy.fits"
    )
    machine.objects.append([infile[1].data, infile[2].data])
    machine.read_xml()

    machine.find_position(0, mode="semi")
    print machine.position_spec
    print machine.CCD_id_to_keep
    print machine.CCD_extname_to_keep
