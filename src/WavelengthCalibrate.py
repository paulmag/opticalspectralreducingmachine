import sys
import os
import numpy as np
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
from matplotlib.widgets import Button
from itertools import combinations

import Tkinter
import tkMessageBox
Tkinter.Tk().withdraw()
from astroML.crossmatch import crossmatch

import Functions as func
from detect_peaks import detect_peaks



class WavelengthCalibrate:
    """
    Module used for interactively calibrating a spectrum with unknown
    wavelengths to a standard spectrum with known wavelengths.
    Calibration happens live and at any point the current version of the
    spectrum that was originally given as input may be extracted, but ideally
    the user should be given some kind of feedback when the current version is
    considered good enough.
    """

    def __init__(self, interactive=False, axis_spec=None):
        self.data = [[], []]  # Placeholder for spectrum datasets.
        self.peaks = [np.zeros((0, 3)), np.zeros((0, 3)), []]
        self.p_opt     = [[1, 0], [1, 0], [1, 0]]  # Current calibration.
        self.p_opt_inv = [[1, 0], [1, 0], [1, 0]]
        self.RMSE = None  # Before any error is calculated.
        self.polyfit_order = None  # Before any calibration is performed.
        self.polyfit_order_max = 12  # Max allowed order of calib polynomial.
        self.eps = [None, None] # placeholder for min selection limits
        self.calibrated = False
            # Turns True after first calibration.
            # When this is True the unit is in [aa] instead of pixelno.
        self.interactive = interactive
        self.grism = None
        self.gases = []  # Name of the gases used. Assumes they have same grism.
        self.gases_residuals = []
        if axis_spec is not None:
            self.axis_spec = axis_spec


    def start_calibration(self, interactive=None):
        """Generic way to perform wavelength calibration after the instance
        has been initialized with the input data. When finished the data can
        be extracted or written to file.
        interactive: (bool) If True, open an interactive window. If False,
            do it automatically and only open the window if something goes
            wrong. Default is False.
        """

        if interactive is None:
            interactive = self.interactive

        self._read_curves()

        if interactive:
            self.make_window()
        else:
            self._auto_match(find=True, repeat=5)

        if not self.interactive:
            # Only auto-finish if interactive is still False.
            self._finish()


    def make_window(self):
        """Makes an interactive plotting window which allows the user to
        manually control the calibration. This should be done after the data
        to be manipulated has been entered, and will be so automatically if
        star_calibration is called with interactive=True.
        """
        self.interactive = True

        ### Make the main figure:
        self.fig = plt.figure(figsize=(14,10))
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self)
        # self.fig.suptitle("Click to select calibration points")

        ### Make the two spectrum and a residual axes:
        self.ax = [
            self.fig.add_subplot(3,1,1),
            self.fig.add_subplot(3,1,2),
            self.fig.add_subplot(3,1,3),
        ]

        ax_size = 0.935, 0.250
        self.ax[0].set_position([0.040, 0.605+0.100, ax_size[0], ax_size[1]])
        self.ax[1].set_position([0.040, 0.160+0.100*2, ax_size[0], ax_size[1]])
        self.ax[2].set_position([0.040, 0.160, ax_size[0], 0.110])

        self.ax[0].set_title("Lamp spectrum to be calibrated")

        self.ax[1].set_title(
            "Standard laboratory spectrum, grism: %s, gas: %s"
            % (str(self.grism), str(self.gases))
        )
        self.ax[1].set_xlabel("Real wavelength [$\AA$]")

        self.ax[2].set_title("Differences between the two spectra")
        self.ax[2].set_xlabel("")
        self.ax[2].grid("on")

        ### Make info texts:
        self.text_select = self.fig.text(
            0.520, 0.075,
            "Selection mode: ON",
        )
        self.text_polyorder = self.fig.text(
            0.520, 0.050,
            "Polynomial order: None",
        )
        self.text_error = self.fig.text(
            0.520, 0.025,
            "RMSE: None",
        )
        self.log = ["...", "..."]
        self.text_log = (
            self.fig.text(
                0.040, 0.105,
                "Log: ...",
            ),
            self.fig.text(
                0.040, 0.120,
                "Log: ...",
            ),
        )

        ### Make the buttons:
        b_size = 0.150, 0.035

        self.ax_select   = plt.axes([ 0.040, 0.060, b_size[0], b_size[1] ])
        self.ax_untangle = plt.axes([ 0.040, 0.010, b_size[0], b_size[1] ])
        self.ax_help     = plt.axes([ 0.200, 0.060, b_size[0], b_size[1] ])
        self.ax_finish   = plt.axes([ 0.200, 0.010, b_size[0], b_size[1] ])
        self.ax_auto     = plt.axes([ 0.360, 0.060, b_size[0], b_size[1] ])
        self.ax_reset    = plt.axes([ 0.360, 0.010, b_size[0], b_size[1] ])
        self.ax_test1    = plt.axes([ 0.780, 0.060, b_size[0], b_size[1] ])
        self.ax_test2    = plt.axes([ 0.780, 0.010, b_size[0], b_size[1] ])

        self.b_select = Button(self.ax_select, 'Selection mode')
        self.b_select.on_clicked(self._toggle_selection_mode)

        self.b_untangle = Button(self.ax_untangle, 'Calibrate')
        self.b_untangle.on_clicked(self._untangle_selections)

        self.b_help     = Button(self.ax_help, 'Help')
        self.b_help.on_clicked(self._show_instructions)

        self.b_finish   = Button(self.ax_finish, 'I am finished')
        self.b_finish.on_clicked(self._finish)

        self.b_auto     = Button(self.ax_auto, 'Auto select')
        self.b_auto.on_clicked(self._auto_match)

        self.b_reset    = Button(self.ax_reset, 'Reset spectra')
        self.b_reset.on_clicked(self._reset)

        self.b_test1    = Button(self.ax_test1, 'Test button 1')
        self.b_test1.on_clicked(self._test1)

        self.b_test2    = Button(self.ax_test2, 'Test button 2')
        self.b_test2.on_clicked(self._test2)

        ### Selection placeholders:
        self.vlines = [[], []] # holder of plt.axvline objects

        ### Other:
        self.selection_mode = True # True is default, may start selecting

        self._update_residual_window()
        self._update_window() # draws any plots that may have been added already
        plt.show()


    def _update_window(self):
        """Refreshes the plots (the two top axes, containing the
        arcspectra) to display any changes. The plots will be drawn from
        scratch from the current state of the data. Should be called
        whenever any data is changed, i.e. after every selection,
        deselection and calibration.
        """

        if not self.interactive:
            # If this is called with interactive=False, do not do anything.
            return

        for ax_no in range(2): # Do everything for both axes.

            self.vlines[ax_no] = [] # Renew placeholder for vertical lines.

            for i in range(len(self.ax[ax_no].lines)):
                # Removes all plots without clearing axes.
                del self.ax[ax_no].lines[0]

            # Make new plots:
            self.ax[ax_no].plot(
                func.polynomial(self.data[ax_no][0], self.p_opt[ax_no]),
                self.data[ax_no][1],
                "b",
            )

            for peak in self.peaks[ax_no]:
                # Draw a vertical line and gauss overplot for each selection:
                self.vlines[ax_no].append(
                    self.ax[ax_no].axvline(
                        func.polynomial(peak[1], self.p_opt[ax_no]),
                        color="r",
                    )
                )
                treshold = peak[1] - 3 * peak[2], peak[1] + 3 * peak[2]
                x = np.linspace(treshold[0], treshold[1], 51)
                y = func.gauss(x, peak[0], peak[1], peak[2])
                self.ax[ax_no].plot(
                    func.polynomial(x, self.p_opt[ax_no]),
                    y,
                    "g",
                )

            if self.calibrated:
                ax_min = np.min([
                    func.polynomial(self.data[0][0][ 0], self.p_opt[0]),
                    func.polynomial(self.data[1][0][ 0], self.p_opt[1]),
                ])
                ax_max = np.max([
                    func.polynomial(self.data[0][0][~0], self.p_opt[0]),
                    func.polynomial(self.data[1][0][~0], self.p_opt[1]),
                ])
            else:
                ax_min = func.polynomial(self.data[ax_no][0][ 0], self.p_opt[ax_no])
                ax_max = func.polynomial(self.data[ax_no][0][~0], self.p_opt[ax_no])
            self.ax[ax_no].axis([
                ax_min,
                ax_max,
                0,
                1,
            ])

        if self.calibrated:
            self.ax[0].set_xlabel("Estimated wavelength [$\AA$]")
        else:
            self.ax[0].set_xlabel("pixel number")

        self.fig.canvas.draw()


    def _update_residual_window(self):
        """Updates the third ax, the one which contains the residual plot.
        This has its own method since it does not need to be called
        everytime time a selection is made, only when a new calibration
        happens. It would even cause an error it it was updated with every
        selection, because it implicitly necessary that all selections are
        paired up correctly.
        """

        if not self.interactive:
            return

        for i in range(len(self.ax[2].lines)):
            del self.ax[2].lines[0]
        self.ax[2].axhline(0, color="k")

        if self.peaks[2]:
            no_pairs = len(self.peaks[2])
            self.ax[2].plot(self.peaks[1][:no_pairs, 1], self.peaks[2][:no_pairs], "ro")
            ax_min = np.min([
                func.polynomial(self.data[0][0][ 0], self.p_opt[0]),
                func.polynomial(self.data[1][0][ 0], self.p_opt[1]),
            ])
            ax_max = np.max([
                func.polynomial(self.data[0][0][~0], self.p_opt[0]),
                func.polynomial(self.data[1][0][~0], self.p_opt[1]),
            ])
            self.ax[2].axis([
                ax_min,
                ax_max,
                - max(np.abs(self.peaks[2])) * 1.1,
                + max(np.abs(self.peaks[2])) * 1.1,
            ])
            text_error = "%f" % self.RMSE
            if len(self.peaks[0]) <= self.polyfit_order + 1:
                text_error += " (Polynomial order fits all points perfectly.)"
        else:
            self.ax[2].axis([
                self.data[1][0][ 0],
                self.data[1][0][~0],
                - 1,
                + 1,
            ])
            text_error = "None"

        self.text_error.set_text("RMSE: %s" % text_error)
        self.text_polyorder.set_text(
            "Polynomial order: %s"
            % str(self.polyfit_order)
        )


    def __call__(self, event):
        """
        Call means that a click event happened, i.e. the user clicked somewhere
        inside the figures.
        Left-click: Place a selection on the peak that was clicked.
        Right-click: Delete the nearest selection.
        This method will return unresponsive if selection_mode is turned
        off or if the click did not happen inside one of the figures.
        """

        if not self.selection_mode:
            return
        if   event.inaxes == self.ax[0]: # top axis: dataset
            ax_no = 0
        elif event.inaxes == self.ax[1]: # middle axis: standard calibration set
            ax_no = 1
        elif event.inaxes == self.ax[2]: # bottom axis: residuals
            ax_no = 2
        else:
            return

        # Convert click to original coordinates:
        event.xdata = func.polynomial(event.xdata, self.p_opt_inv[ax_no])

        if event.button == 1: # left-click
            if ax_no == 2:
                return
            self._select(
                ax_no,
                event.xdata,
                event.ydata,
                calibrate=True,
            )

        elif event.button == 3: # right-click
            if ax_no == 2:
                # Delete a selection pair:
                smallest = 1e9  # Arbitrary large value.
                for i in range(len(self.peaks[1])):
                    if abs(event.xdata - self.peaks[1][i, 1]) < smallest:
                        smallest = abs(event.xdata - self.peaks[1][i, 1])
                        x_del = self.peaks[1][i, 1]
                self._delete(
                    0,
                    func.polynomial(event.xdata, self.p_opt_inv[0]),
                )
                self._delete(1, x_del, calibrate=True)
            else:
                # Delete a specific peak:
                self._delete(ax_no, event.xdata)

        self._update_window()  # Update plots with current changes.
        self._update_residual_window()


    def _delete(self, ax_no, xdata=None, index=None, calibrate=False):
        """Deletes the selection which is nearest to xdata in ax_no.
        ax_no: (int) Which ax to delete in.
        xdata: (float) Position (wavelength of click).
        calibrate: (bool) Decide if automatically calibrate after deleting.
        """

        if len(self.peaks[ax_no]) == 0:
            # No peak to delete.
            return

        if index is None:
            # Find index of the selection closest to the click and delete it:
            smallest = np.inf  # Initial smalles value.
            for i in range(len(self.peaks[ax_no])):
                if abs(xdata - self.peaks[ax_no][i, 1]) < smallest:
                    smallest = abs(xdata - self.peaks[ax_no][i, 1])
                    index = i

        self.peaks[ax_no] = np.vstack((
            self.peaks[ax_no][: index],
            self.peaks[ax_no][index+1 :],
        ))

        # Initiate calibration if at least two selection pairs now exist:
        if calibrate:
            if (len(self.peaks[0]) == len(self.peaks[1]) and
                len(self.peaks[0]) >= 2
            ):
                self._calibrate()



    def _delete_all(self):
        """Removes all selections in all axes."""
        self.peaks = [np.zeros((0, 3)), np.zeros((0, 3)), []]
        self._update_window()
        self._update_residual_window()


    def _reset(self, event=None):
        """Reset calibrated wavelengths to original state. Useful if the
        user screwed up badly.
        """
        self.p_opt     = [[1, 0], [1, 0], [1, 0]]
        self.p_opt_inv = [[1, 0], [1, 0], [1, 0]]
        self.polyfit_order = None
        self.calibrated = False
        self._delete_all()


    def _test1(self, event=None):
        """Just a test event for a button."""
        tkMessageBox.showinfo(
            "Test 1",
            "This box is just for testing. Nothing interesting here."
        )

    def _test2(self, event=None):
        """Just a test event for a button."""
        tkMessageBox.showinfo(
            "Test 2",
            "There is nothing interesting here either. Sorry"
        )


    def _select(self, ax_no, xdata, ydata=0.5, calibrate=False, show=False):
        """Attempts to select a peak at the given location.
        ax_no: Which window (and thus which spectrum) is used.
        xdata: Initial wavelength guess for center of peak.
        ydata: Initial amplitude guess for peak. Is usually not critical, so
            may be omitted.
        calibrate: (bool) Automatically calibrate if successful fit.
        show: (bool) Print errormessage to explain why selection failed, if
            it fail.
        Returns True if a selection is made, False if not.
        """
        # Click position used as initial guess for a gaussian fit:
        p0 = [ydata, xdata, 2]
        try:
            p_opt, p_cov = curve_fit(
                func.gauss,
                self.data[ax_no][0],
                self.data[ax_no][1],
                p0,
            )
        except RuntimeError:
            msg = "RuntimeError: No peak found. No selection made."
            if show:
                print msg
            self._log_add(msg)
            return False

        # Peak is assumed to only exist within 3*std of first fit:
        treshold = p_opt[1] - 3 * p_opt[2], p_opt[1] + 3 * p_opt[2]
        treshold_index = (
            (np.abs(self.data[ax_no][0] - treshold[0])).argmin(),
            (np.abs(self.data[ax_no][0] - treshold[1])).argmin(),
        )

        # Do a second fit within treshold to avoid bias by neighbour peaks:
        p0 = p_opt
        try:
            p_opt, p_cov = curve_fit(
                func.gauss,
                self.data[ax_no][0][treshold_index[0] : treshold_index[1]],
                self.data[ax_no][1][treshold_index[0] : treshold_index[1]],
                p0,
            )
        except TypeError:
            msg = "TypeError: No proper peak found. No selection made."
            if show:
                print msg
            self._log_add(msg)
            return False
            #TODO Set a minimum limit on treshold instead so this does not
            # happen.
        except RuntimeError:
            msg = "RuntimeError: No peak found. No selection made."
            if show:
                print msg
            self._log_add(msg)
            return False

        # Check if this point has been selected before:
        for x_point in self.peaks[ax_no][:, 1]:
            if abs(p_opt[1] - x_point) < self.eps[0]:
                # It is so close it is assumed to be the same point.
                msg = (
                    "This point is selected before. "
                    "No new selection made."
                )
                if show:
                    print msg
                self._log_add(msg)
                return False

        # Check if a proper peak was found or just nothing:
        if p_opt[0] < self.eps[1]:
            # Peak so low it is assumed there is actually no peak here.
            msg = "The peak found is too small. No selection made."
            if show:
                print msg
            self._log_add(msg)
            return False

        # Check if peak is in totally wrong place:
        if abs(p_opt[1] - xdata) > 100:
            # Peak center so far away that it is probably something else.
            msg = "The peak found is in the wrong place. No selection made."
            if show:
                print msg
            self._log_add(msg)
            return False

        """
        # Check if peak has wrong size:
        if ydata / p_opt[0] > 1.5 or ydata / p_opt[0] < 0.6:
            if show:
            # Peak so low it is assumed there is actually no peak here.
                print "The peak found has wrong size. No selection made."
            return False
        """

        # Check if peak is too close to its nearest neighbour:
        if len(self.peaks[ax_no]) > 0:
            tol = 1  # sigma tolerance between peaks.
            tooclose = False
            smallest = 1e9  # Arbitrary large value.
            for i in range(len(self.peaks[ax_no])):
                if abs(p_opt[1] - self.peaks[ax_no][i, 1]) < smallest:
                    smallest = abs(p_opt[1] - self.peaks[ax_no][i, 1])
                    i_check = i
            other = self.peaks[ax_no][i_check]
            if other[1] < p_opt[1]:
                if other[1] + other[2] * tol > p_opt[1] - p_opt[2] * tol:
                    tooclose = True
            else:
                if other[1] - other[2] * tol < p_opt[1] + p_opt[2] * tol:
                    tooclose = True
            if tooclose:
                msg = (
                    "The peak found overlaps with another peak. "
                    "No selection made."
                )
                if show:
                    print msg
                self._log_add(msg)
                return False


        # Save selection:
        self._add_peak(ax_no, p_opt)
        msg = "Found peak in axis: %d, position: %.2f" % (ax_no, p_opt[1])
        if show:
            print msg
        self._log_add(msg)

        # Initiate calibration if at least two selection pairs now exist:
        if calibrate:
            if (len(self.peaks[0]) == len(self.peaks[1]) and
                len(self.peaks[0]) >= 2
            ):
                self._calibrate()
        return True


    def _add_peak(self, ax_no, p_opt):
        """Add a new selection to the list (array) of peaks. This method
        exists since you cannot directly append to an array.
        ax_no: (int) In which ax (which spectrum) the peak is.
        p_opt: (float, array-like, size=3) Peak parameters: A, x0, sigma
        """
        self.peaks[ax_no] = np.vstack((self.peaks[ax_no], p_opt))


    def _find_all(self, event=None, ax_no="both", update_window=False, show=False):
        """Automatically finds and selects all peaks in both spectra.
        ax_no: Which axes to find peaks in. Can be '0', '1' or 'both'.
        """

        if ax_no in ["both", "all"]:
            ax_nos = range(2)  # Do both.
        else:
            ax_nos = [int(ax_no)]  # Do one.

        for ax_no in ax_nos:
            argpeaks = detect_peaks(
                self.data[ax_no][1],
                mph=self.eps[1],
                mpd=2,
                show=False,
            )
            for i in argpeaks:
                x, y = self.data[ax_no][0][i], self.data[ax_no][1][i]
                # print "Try:", i, x, y
                self._select(ax_no, x, y, show=show)

        if update_window:
            self._update_window()



    def _find_all_old(self, ax_no="both", update_window=True, show=False):
        """This method is the deprecated version of _find_all.
        It is kept in the code for now for easy reference.

        Automatically finds and selects all peaks in both spectra.
        ax_no: Which axes to find peaks in. Can be '0', '1' or 'both'.
        """

        if ax_no in ["both", "all"]:
            ax_nos = range(2)  # Do both.
        else:
            ax_nos = [int(ax_no)]  # Do one.

        for ax_no in ax_nos:
            data_copy = self.data[ax_no][1].copy()
            while 1:
                i = np.argmax(data_copy)
                x, y = self.data[ax_no][0][i], self.data[ax_no][1][i]
                # print "Try:", i, x, y
                if y < self.eps[1]:
                    if show:
                        print "No more peaks can be found in this image."
                    break
                elif self._select(ax_no, x, y, show=show):
                    data_copy[i-2 : i+3] = 0
                else:
                    data_copy[i] = 0


    def _auto_match_init(self, nn=None, n=4, find=True):
        """Do an automatic first calibration. Should be able to find and
        match n pairs of peaks even if no prior calibration exists at all.
        n: (int) How many peaks in each group. F.ex. 3 means triples.
        N: (int) How many sections to split the spectra into.
            None => default value, depends on number of peaks
        find: (bool) Automatically find all peaks first.
        """

        if find:
            self._find_all()

        self._sort_selections()
        peaks_largest = [self.peaks[0][:,1], self.peaks[1][:,1]]
        N = np.array([len(self.peaks[0]), len(self.peaks[1])]) - n + 1
        best_peaks_all = [[], []]
        peaks_copy = list(self.peaks)
        self._delete_all()

        if nn is None:
            if min(N) > 30:
                nn = 3  # Can have more sections if there are many peaks.
            else:
                nn = 2

        weight_first = 1/3.  # Give smaller weight to first triplet gap.
        triplets = []  # Or quadruple, femtuple, etc.
        for l in range(2):
            triplets.append(np.zeros((N[l], n-1)))
            triplets[l][:,0] = (peaks_largest[l][1:N[l]+1] - peaks_largest[l][0:N[l]])
            triplets[l][:,0] *= weight_first / \
                np.median(peaks_largest[l][1:~0] - peaks_largest[l][0:~1])
            for t in range(1, n-1):
                triplets[l][:,t] = (
                    (peaks_largest[l][t+1:N[l]+t+1] - peaks_largest[l][t:N[l]+t]) /
                    (peaks_largest[l][t:N[l]+t] - peaks_largest[l][t-1:N[l]+t-1])
                )

        u = [np.zeros(N[0]), np.zeros(N[1])]  # Uniqueness lists.
        for l in range(2):
            for i in xrange(N[l]):
                r_best = np.inf
                for ii in xrange(N[l]):
                    if i == ii: continue
                    r = triplets[0][i] / triplets[0][ii]
                    r[np.where(r < 1)] **= -1
                    r = np.linalg.norm(r)
                    if r < r_best:
                        r_best = r
                u[l][i] = r_best

        residuals = np.zeros((N[0], N[1]))
        uresiduals = np.zeros((N[0], N[1]))
        for i in xrange(N[0]):
            for j in xrange(N[1]):
                residuals[i,j] = np.linalg.norm(triplets[0][i] - triplets[1][j])
                uresiduals[i,j] = abs(u[0][i] - u[1][j])

        correlations = (
            u[0][:,None] * u[1] / (
                residuals +
                uresiduals * (0.33 * residuals.mean() / uresiduals.mean())
            )
        )

        j_best = np.argmax(correlations, axis=1)
        corr_best = correlations[np.arange(N[0]), j_best]
        j_best = func.subsequence(j_best)

        i_list = []  # Peaks already chosen (since triplets may overlap).
        for i, j in enumerate(j_best):
            if np.isnan(j): continue
            for t in range(n):
                ii, jj = i+t, j+t
                if ii in i_list:
                    if not (corr_best[i] > corr_best[i-n+1:i]).all():
                        # This is a bad/fishy hack. If triplet i is better than
                        # the previous ones it will make new peaks in ADDITION
                        # to and in contradction with the already existing
                        # peaks.
                        continue  # Avoid duplicates.
                self._add_peak(0, peaks_copy[0][ii])
                self._add_peak(1, peaks_copy[1][jj])
                i_list.append(ii)

        self._calibrate(polyfit_order_max=1)
        self._remove_outliers()


    def _auto_match(self, event=None, init=None, find=True, repeat=3):
        """Automatically crossmatches all selected peaks and deletes the
        ones that no match is found for. A rough initial calibration is
        required for this to work, so it is reccommended to select three
        points manually first.
        event: The button-click event is passed here if this method is
            called by a button-click.
        init: (bool) Automatically perform initial _auto_match_init first,
            so that manual selection is not needed. If None, it will be done
            if needed.
        find: (bool) Automatically find all peaks first.
        """

        if find:
            self._find_all()
        if init is None:
            # Does not need initial calibration if it is already done
            # (either automatically somehow or manually by the user.
            init = not self.calibrated
        if init:
            self._auto_match_init(find=False)
            self._find_all()

        # Crossmatching:
        dists, inds = crossmatch(
            func.polynomial(self.peaks[0][:, 1, None], self.p_opt[0]),
            func.polynomial(self.peaks[1][:, 1, None], self.p_opt[1]),
            max_distance = 9,
        )

        # Reselect only the good ones:
        x_copy = self.peaks[0][:], self.peaks[1][:]
        self._delete_all()
        for i, j in enumerate(inds):
            if j >= len(x_copy[1]):
                continue  # No match.
            self._add_peak(0, x_copy[0][i])
            self._add_peak(1, x_copy[1][j])

        self._calibrate()
        self._remove_outliers()

        if repeat > 0:
            self._auto_match(init=False, repeat=repeat-1)
        else:
            self._untangle_selections()


    def _set_eps(self, show=False):
        """
        eps[0] is the minimum allowed x-distance difference between two
        selection points, to ensure the same point is not selected twice.
        This will be done by a maximum min limit. A better solution might be
        to always check if a selection is within the std of the neighbouring
        selections, but I don't think that is necessary.
        eps[1] is the minimum allowed peak of a gaussian fit, to ensure that it
        is actually fitting a peak and not just nothing.
        """
        # Must at least be a few pixels apart:
        self.eps[0] =  \
            2 * (self.data[0][0][-1] - self.data[0][0][0]) /  \
            self.data[0][0].size

        # Must at least be 10 times std of noise:
        self.eps[1] = func.tmedian(
            self.data[0][1],
            iterations=7,
            return_array=True
        ).std() * 10
        # self.eps[1] = 0.1  #gitignore

        # Visualize effect of different degrees of sigma clipping:
        if show:
            test = []
            for i in range(1,11):
                test.append(func.tmedian(
                    self.data[0][1],
                    iterations=i,
                    return_array=True
                ).std())
            plt.plot(range(1,11), test)
            plt.show()


    def set_data_raw(self, y, position_spec=None, radius=100, trim=2./3., grism=None, normalize=True):
        """ Expects an array describing fluxes in an arcspectrum OR an
        arcimage, which will then be stacked into a spectrum. Optional
        parameters can be given if you only want to stack a certain slice of
        the arcimage in a radius around where you know the spectrum is.
        x-array not necessary since it is only arbitrary pixel values
        anyway. If several arcspectra are set they are all added together
        (it is assumed that if several arclamps are used they do not have
        overlapping spectral lines).
        y: The spectrum, either 1D array of arcintensities or 2D array
            representing an arcimage.
        position_spec: (int) The cirka position of the spectrum line in the
            arcimage. This argument is highly recomended to specify.
        radius: (int) The number of pixels to include of each side of
            position_spec. If None, everything is used (not recomended.).
        trim: (float) The ratio of the slice to trim away in the trimmed mean.
        """

        if len(np.shape(y)) == 2:
            # This means that an image was provided. Extract spectrum from it:

            # Transpose the image if spectrum not oriented in y-direction:
            if self.axis_spec not in (0, 1):
                raise ValueError(
                    "<dispaxis> must be '0' or '1'. "
                    "Which way is the spectrum trace oriented?"
                )
            if self.axis_spec == 1:
                y = y.transpose()
            if position_spec is None:
                position_spec = radius = y.shape[1] / 2

            # Trimmed mean:
            trim = int(round(radius * trim))
            y = y[:, position_spec-radius : position_spec+radius]
            y = np.mean(np.sort(y, axis=1)[:, trim:-trim], axis=1)
            self.y = y

        if normalize:
            y = self._normalise(y)

        if not self.data[0]:
            # Initially set x-values to pixelno. After calibrations they
            # will change to wavelengths in aangstrom.
            x = np.linspace(0, np.size(y)-1, np.size(y)) # [pixelno] => [aa]
            self.data[0] = [x, y]

        else:
            self.data[0][1] += y
            # Interpolation should not be needed since these spectra are
            # directly reduced from the same image shape and not tampered with
            # any further, so they should represent the exact same
            # pixels/wavelengths.

        self._set_eps()


    def set_data_calibration(self, x=None, y=None, gas=None, grism=None, normalize=True):
        """Expects an array pair describing wavelengths and fluxes in a
        standard arcspectrum with known wavelengths used for calibrations.
        Alternatively, if you already have a discrete list of wavelengths
        where you know there are peaks (absorption lines) you only need to
        supply the x. If several arcspectra are set they are all added
        together (it is assumed that if several arclamps are used they do
        not have overlapping spectral lines).
        Another alternative is to just give the name of the gas and the
        grism and the correct calibration will be selected from a database
        automatically.
        x: (float, array-like) Array of wavelengths.
        y: (float, array-like) Array of corresponding intensities.
        gas: (string) F.ex.: "Ne" or "Hg".
        grism: (string) F.ex: "R1000R" or "R2500R".
        """

        if grism is not None:
            self.grism = grism
            # Assumes that the user will not provide different grisms,
            # since that would be kind of crazy.

        if x is None:
            if gas is None:
                gas = self._find_gas(grism, self.y)
            else:
                if gas not in self.gases:
                    self.gases.append(gas)
                    self._read_lines(gas)
        else:
            if y is None:
                x, y = self._make_artificial_peaks(x)
            if normalize:
                y = self._normalise(y)
            if not self.data[1]:
                self.data[1] = [x, y]
            else:
                self.data[1][1] += y
            #TODO Here it is assumed that every calib spectrum has the exact
            # same x-values (wavelength points). This is probably not the
            # case, so some interpolation should be used here insted.


    def _read_lines(self, gas=None, add_peaks=False):
        if gas is None:
            gases = self.gases
        else:
            gases = [gas]
        for gas in gases:
            infile = open(
                os.path.dirname(sys.argv[0]) +
                "/../data/arclamps/lines/%s/%s_%s.txt"
                % (self.instrument_name, gas, self.grism),
                "r",
            )
            lines = np.array([float(line) for line in infile.readlines()])
            infile.close()
            if add_peaks:
                for line in lines:
                    self._add_peak(1, [0., line, 1.])
        return lines


    def _read_curves(self, gas=None, accept_artificial=True):
        if gas is None:
            gases = self.gases
        else:
            gases = [gas]
        for gas in gases:
            try:
                infile = open(
                    os.path.dirname(sys.argv[0]) +
                    "/../data/arclamps/curves/%s/%s_%s_wave.txt"
                    % (self.instrument_name, gas, self.grism),
                    "r",
                )
                x_cal = np.array([float(line) for line in infile.readlines()])
                infile.close()
                infile = open(
                    os.path.dirname(sys.argv[0]) +
                    "/../data/arclamps/curves/%s/%s_%s_flux.txt"
                    % (self.instrument_name, gas, self.grism),
                    "r",
                )
                y_cal = np.array([float(line) for line in infile.readlines()])
                infile.close()
            except:
                print (
                    "IOError: There is no curves for "
                    "%s/%s_%s"
                    % (self.instrument_name, gas, self.grism)
                )
                if accept_artificial:
                    print "Creating artificial spectrum at position of lines..."
                    x_cal, y_cal = self._make_artificial_peaks(self._read_lines(gas))
                else:
                    sys.exit(1)
            self.set_data_calibration(x_cal, y_cal)


    def _find_gas(self, grism, data, N=None, n=4):
        """If it is unknown which gas is in the arcimage (this will often
        happen since many observatories does not put the information in the
        header) this method can be used. It will compare the peaks in the
        arcimage with the positions of known gases in a database and find
        which gas we have.
        grism: (string) F.ex: "R1000R" or "R2500R".
        # data: (float, array-like) Array of the intensities of the raw data.
        Returns the name of the correct gas (string).
        """

        self.peaks = [np.zeros((0, 3)), np.zeros((0, 3)), []]
        self._find_all(ax_no=0, update_window=False)
        self._sort_selections()

        infile = open(
            os.path.dirname(sys.argv[0]) + "/../data/arclamps/gases.txt",
            "r",
        )
        gases = [line.strip() for line in infile.readlines()]
        infile.close()
        gas_residuals = [900001] * len(gases)

        for gasno, gas in enumerate(gases):
            try:
                infile = open(
                    os.path.dirname(sys.argv[0]) +
                    "/../data/arclamps/lines/%s_%s.txt"
                    % (gas, grism),
                    "r",
                )
            except IOError:
                print (
                    "The gas-grism combination %s-%s does not exist in the "
                    "database. If it should be there you have to add it."
                    % (gas, grism)
                )
                continue
            lines = [float(line) for line in infile.readlines()]
            infile.close()

            peaks_largest = [self.peaks[0], lines]
            N0 = len(self.peaks[0]) - n + 1
            N1 = len(lines) - n + 1
            best_peaks_all = [[], []]

            if N is None:
                if min(N0, N1) > 30:
                    N = 3  # Can have more sections if there are many peaks.
                elif min(N0, N1) > 15:
                    N = 2
                else:
                    N = 1
                    if min(N0, N1) < 2:
                        n = 3
                        N0 = len(self.peaks[0]) - n + 1
                        N1 = len(lines) - n + 1

            for k in xrange(N):
                best_residual = 900001
                best_peaks = None

                for i in xrange(int(float(N0)/N * k), int(float(N0)/N * (k+1))):
                    peaks0 = np.array(peaks_largest[0][i:i+n])
                    for j in xrange(0, N1):
                        peaks1 = np.array(peaks_largest[1][j:j+n])

                        p_opt, residual, unused, unused, unused = np.polyfit(
                            peaks0[:, 1],
                            peaks1[:],  # Just lines, not full peaks.
                            1,
                            full = True,
                        )
                        residual = float(residual)
                        if residual < best_residual:
                            best_residual = residual
                            best_peaks = peaks0[:, 1], peaks1[:]

                best_peaks_all[0] += list(best_peaks[0])
                best_peaks_all[1] += list(best_peaks[1])

            best_p_opt, gas_residuals[gasno], unused, unused, unused = np.polyfit(
                best_peaks_all[0],
                best_peaks_all[1],
                max(1, N-1),  # At least order 1.
                full = True,
            )
            gas_residuals[gasno] = float(gas_residuals[gasno])

        self.peaks = [np.zeros((0, 3)), np.zeros((0, 3)), []]
        self.data[0] = []  # Reset.
        self.gases_residuals.append(gas_residuals)


    def pair_gases(self, grism):
        infile = open(
            os.path.dirname(sys.argv[0]) + "/../data/arclamps/gases.txt",
            "r",
        )
        gases = [line.strip() for line in infile.readlines()]
        infile.close()
        while len(self.gases_residuals) > 0:
            arcno = np.argmin(self.gases_residuals) / len(gases)
            gasno = np.argmin(self.gases_residuals.pop(arcno))
            self.gases_residuals = np.array(self.gases_residuals)
            try:
                self.gases_residuals[:, gasno] = 900001
                    # Eliminates this gas from being selected again.
            except IndexError:
                pass # Nothing left of the matrix, so finished!
            self.gases_residuals = list(self.gases_residuals)
            self.set_data_calibration(gas=gases[gasno], grism=grism)


    def _make_artificial_peaks(self, x_peaks):
        """Creates an artificial (but usable!) calibration spectrum based on
        the already known positions of spectrum lines. This can be used if
        you do not have a good calibration spectrum, and it can be used to
        MAKE a calibration spectrum.
        x_peaks: (float, array-like) List of absorption line positions.
        """

        # For each peak, make the width as large as possible without overlap:
        n = len(x_peaks)  # Number of peaks.
        sigma  = [(x_peaks[1] - x_peaks[0]) / 9.]
        for i in xrange(1, n-1):
            sigma += [
                min(x_peaks[i] - x_peaks[i-1], x_peaks[i+1] - x_peaks[i]) / 9.
            ]
        sigma += [(x_peaks[~0] - x_peaks[~1]) / 9.]
        sigma_max = 14.  # avoid unphysical peaks
        sigma = np.array(sigma)
        sigma[np.where(sigma > sigma_max)] = sigma_max

        # Make a wavelength range containing all peaks:
        N = 8001
        x = np.linspace(
            3500,
            11500,
            # x_peaks[ 0] - sigma[ 0] * 7,  #TODO This is better, but it does
            # x_peaks[~0] + sigma[~0] * 7,  # not work when several arc lists
            N,                              # are used at once.
        )

        # Add each peak:
        y = np.zeros(N)
        for i, x_peak in enumerate(x_peaks):
            y += func.gauss(x, A=1, x0=x_peak, sigma=sigma[i])
        return x, y


    def _normalise(self, y):
        """
        Set baseline to 0.
        Set highest peak to 1.
        Makes it easier to compare spectra to eachother.
        Only the intensities (y-direction) is scaled, and it does not matter
        to the result, since only the wavelengths (x-direction) is calibrated.
        """
        y -= func.tmedian(y)
        y /= np.max(y)
        return y


    def _toggle_selection_mode(self, event):
        """It is useful to turn selection mode off when you want to zoom or
        pan the plots. You cannot select or delete peaks while it is off.
        Just switch it to the other True/False option.
        Update the Selection mode ON/OFF text.
        event: The button-click event is passed here.
        """
        self.selection_mode = not self.selection_mode

        if self.selection_mode:
            self.text_select.set_text("Selection mode: ON")
        else:
            self.text_select.set_text("Selection mode: OFF")

        self.fig.canvas.draw()


    def _is_entangled(self):
        """This returns True if selections are NOT placed in the same order
        in the two axes. If calibration is attempted when _is_entangled
        returns True weird stuff will happen, therefore it shall not be
        allowed to do so.
        """
        if (self.peaks[0][:, 1].argsort() ==
            self.peaks[1][:, 1].argsort()
        ).all():
            return False
        else:
            return True


    def _untangle_selections(self, event=None):
        """Sorts the selections in the two axes into the same order, from
        left to right. This function should only be called actively by the
        user (via the untangle-button) or _auto_match.
        event: The button-click event is passed here if method is called by
            button-click.
        """

        if len(self.peaks[0]) != len(self.peaks[1]):
            # It does not make sense to pair two sets of unequal length.
            tkMessageBox.showerror(
                "Error",
                "Calibration not possible. "
                "You must select an equal number of calibration points."
            )
            return

        self._sort_selections()
        self._calibrate() # automatically _calibrate
        self._update_residual_window()
        self._update_window()


    def _sort_selections(self):
        """Sorts the selected peaks in both axes from left to right."""
        # Sort/untangle:
        indices_sorted = [
            self.peaks[0][:, 1].argsort(),
            self.peaks[1][:, 1].argsort(),
        ]
        # Remake the selections that are now untangled.:
        self.peaks[0][:] = self.peaks[0][indices_sorted[0]]
        self.peaks[1][:] = self.peaks[1][indices_sorted[1]]


    def _calibrate(self, polyfit_order_max=None, show=False):
        """When some pairs of calibration points are made a polynomial may
        be fitted through them and used to recalibrate the input spectrum.
        The calibrations will hopefully lead to a smaller RMS-error between
        the dataset and the calibrations spectrum. All different possible
        orders of the polynomial will be tried and the one with the best BIC
        will be used. Returns True if calibrations was successful, False if
        could not calibrate.
        polyfit_order_max: (int) How many different polynomial orders will
            be tried. If None, the default values is used.
        show: (bool) Plot and print detailed information regarding the polyfit.
        """

        if len(self.peaks[0]) != len(self.peaks[1]):
            tkMessageBox.showerror(
                "Error",
                "Calibration not possible. "
                "You must select an equal number of calibration points."
            )
            return False

        elif len(self.peaks[0]) < 2:
            tkMessageBox.showerror(
                "Error",
                "Calibration not possible. "
                "You need at least two sets of calibration points. "
                "Select some more."
            )
            return False

        elif self._is_entangled():
            tkMessageBox.showwarning(
                "Warning",
                "You have criss-crossed selections. "
                "If this is intentional press 'Calibrate'. "
                "Elsewise you must delete some selections."
            )
            return False

        # Do the fit:

        xdata = np.array(self.peaks[0][:, 1])
        ydata = np.array(self.peaks[1][:, 1])
        yerror = (
            func.polynomial(self.peaks[0][:, 1], self.p_opt[0]) -
            func.polynomial(self.peaks[1][:, 1], self.p_opt[1])
        )
        if show:
            plt.figure()
            plt.errorbar(
                xdata,
                ydata,
                yerror,
                fmt='.',
                # label='with 1.5 pixel error',
            )
            plt.legend(loc='lower right')
            plt.show()

        # Define the range of polynomial orders to probe:
        # The highest possible order is one less than the number of
        # calibration points, but it is limited to polyfit_order_max.
        if polyfit_order_max is None:
            polyfit_order_max = self.polyfit_order_max
        polyfit_order_max = min(len(self.peaks[0])-1, polyfit_order_max)

        # Plot the data and prepare a x-axis vector to overplot the fits
        # with the best models.
        if show:
            plt.figure()
            plt.errorbar(xdata, ydata, yerror, fmt='.')
            plt.ylim(0.9 * min(ydata), 1.1 * max(ydata))

        p_opt = func.get_best_polymodel(
            xdata, ydata, yerror, np.ones(yerror.size),
            order_min=1, order_max=polyfit_order_max,
        )
        p_opt_inv = func.get_best_polymodel(
            ydata, xdata, yerror, np.ones(yerror.size),
            order_min=len(p_opt)-1, order_max=len(p_opt)-1,
        )

        # Change spectrum and selections with the calibration polynomial:
        self.p_opt_backup = list(self.p_opt)
        self.p_opt_inv_backup = list(self.p_opt_inv)
        self.p_opt[0] = p_opt
        self.p_opt_inv[0] = p_opt_inv

        # Check if it is still monotonic:
        if not func.monotonic(func.polynomial(self.data[0][0], p_opt)):
            self.p_opt = list(self.p_opt_backup)
            self.p_opt_inv = list(self.p_opt_inv_backup)
            print (
                "Warning: This calibration  with order %d caused wrapping.\n"
                "Your selections may be bad. Trying again with order %d. "
                % (len(p_opt)-1, len(p_opt)-2)
            )
            self._calibrate(polyfit_order_max=len(p_opt)-2)

            #TODO Do not start interactive when this happens Is it safe?
            if not self.interactive and False:
                tkMessageBox.showwarning(
                    "Interactive mode started",
                    "A calibration  with order %d caused wrapping. "
                    "Trying again with order %d. "
                    "\n\n"
                    "This may mean that some of the automatic selections are "
                    "bad, so the interactive window has been opened for "
                    "manual check."
                    % (len(p_opt)-1, len(p_opt)-2)
                )
                self._reset()
                self.start_calibration(interactive=True)
            return False

        # Find differences between selection for residue plot
        self.polyfit_order = len(p_opt) - 1
        self.peaks[2] = [None] * len(self.peaks[0])
        for i in range(len(self.peaks[0])):
            self.peaks[2][i] = (
                func.polynomial(self.peaks[0][i, 1], self.p_opt[0]) -
                func.polynomial(self.peaks[1][i, 1], self.p_opt[1])
            )

        self.RMSE = func.RMSE(
            func.polynomial(self.peaks[0][:, 1], p_opt),
            self.peaks[1][:, 1],
        )

        self.calibrated = True
        return True


    def _remove_outliers(self, event=None, treshold=3):
        residuals = np.abs(self.peaks[2])
        std = np.sqrt(np.mean(residuals**2))
        to_delete = np.where(residuals > treshold * std)[0]
        for i in to_delete[::-1]:
            self._delete(0, index=i, calibrate=False)
            self._delete(1, index=i, calibrate=False)
            del self.peaks[2][i]
        self._calibrate()


    def _show_instructions(self, event):
        """Displays a message box with instructions on how to use the
        interactive plot.
        event: The button-click event.
        """
        tkMessageBox.showinfo(
            "Instructions",
            "Select pairs of peaks from the two spectra that you know comes from the same absorption lines."
            "\n\n"
            "Left-click selects the nearest peak."
            "\n\n"
            "Right-click deletes the nearest selection.\n\n"
            "The top spectrum will automatically _calibrate everytime you have  performed a valid pair of selections."
            "\n\n"
            "With the buttons from the Matplotlib-interface at the top-left corner you can reset view, undo/redo view, pan, zoom, zoom to rectangle, edit appearance, and save the figure."
            "\n\n"
            "'Selection mode' toggles the ability to perform selections on/off. You should turn it off when zooming and panning in the plots."
            "\n\n"
            "'Calibrate' sorts your selections if you have criss-crossed your selections in the two spectra and performs a calibration as long as there is an equal amount on selections in the two spectra."
            "\n\n"
            "'Help' opens this helptext."
            "\n\n"
            "Click 'I am finished' when you are satisfied with the current calibration and want to close the calibration window."
            "\n\n"
            "'Auto select' selects all peak pairs automatically. You can look at the result and click 'I am finished' if you are satisfied."
            "\n\n"
            "'Reset' will remove all selection and reset the spectrum to its original state.",
        )

    def _finish(self, event=None, show=False):
        """Call when calibrations is complete, either by a button click of
        the user or automatically if interactive mode is not used. Closes
        the interactive window. Returns True if everything finishes
        successfully, False if not, and then interactive mode will be turned
        on if it was not already.
        event: The button-click event will be passed here if method called
            by button-click.
        show: (bool) Display the result with a plot.
        """

        if not self._calibrate():
            if self._is_entangled():
                tkMessageBox.showerror(
                    "Error!",
                    "You have criss-crossed selections, so the program is currently in an unfinished state. If the current selections are correct press 'Calibrate' before finishing. Elsewise you must delete some selections."
                )
                #TODO The user gets TWO error messages here. Should only be one.
            if not self.interactive:
                self.make_window()
                tkMessageBox.showwarning(
                    "Interactive mode started!",
                    "The automatic calibration tried to finish in an "
                    "unfinished state. Something probably went wrong. "
                    "The interactive window has been opened for manual check."
                )
            return False

        if self.interactive:
            plt.close()
        if show:
            plt.figure()  #gitignore
            plt.plot(self.peaks[0][:, 1], self.peaks[0][:, 1], "bo")  #gitignore
            plt.show()  #gitignore
        return True


    def _log_add(self, msg):
        if not self.interactive:
            return
        self.log.append(msg)
        for i, text_log in enumerate(self.text_log):
            text_log.set_text("Log: %s" % self.log[~i])


    def get_wavelengths(self):
        """Gets the finished, calibrated wavelength array. This array can be
        used as the wavelengths for a corresponding flux array. Assumes that
        the calibration is finished when this is called!
        """
        return func.polynomial(self.data[0][0], self.p_opt[0])


    def write_to_file(self, path, flux=None, arcflux=False):
        """Can write wavelengths and/or fluxes of the (now) readily
        calibrated arcspectrum to file. Normally you only want wavelengths
        and fluxes of the a science target.  Saving the
        arc fluxes is useful if you are actually interested in the arcspectrum
        itself, which is the case when you are making the calibration
        arcspectra.
        path: (string) Where to save the spectrum.
        flux: (array-like, floats) The fluxes corresponding to the wavelenghts.
        arcflux: (bool) Save the arc fluxes instead of science fluxes.
        """
        # Automatically overwrites outfile if it already exists.
        if not arcflux:
            outfile = open(path, "w")
            for i, x, y in zip(
                xrange(len(self.data[0][0])),
                func.polynomial(self.data[0][0], self.p_opt[0]),
                flux,
            ):
                outfile.write("%d %s %s\n" % (i, repr(x), repr(y)))
            outfile.close()
        elif arcflux:
            outfile = open(path, "w")
            for y in self.data[0][1]:
                outfile.write("%s\n" % repr(y))
            outfile.close()


if __name__ == "__main__":
    """
    Sample use of this module with simple dummy data.
    Useful for testing, debugging and demonstration.
    """

    try:
        from astropy.io import fits
        HDU = fits.open("../data/arclamps/images/HgAr-Lamp-R2500R-ccd2.fits")
        y_dat = HDU[0].data
        HDU.close()
    except:
        # If the sample file not found, use this arbitrary flux:
        x_dat = np.linspace(4090, 9802, 512)
        y_dat = 2.0 * np.exp(- (x_dat - 7000)**2 / (2 * 30**2)) + 0.1 \
              + 0.8 * np.exp(- (x_dat - 7900)**2 / (2 * 25**2)) + 0.1 \
              + 0.5 * np.exp(- (x_dat - 8100)**2 / (2 * 25**2)) + 0.1

    try:
        infile = open("../data/arclamps/lines/HgAr_R2500R.txt", "r")
        x_cal = [float(line) for line in infile.readlines()]
        infile.close()
    except IOError:
        # If the sample file not found, use these arbitrary points:
        x_cal = [5000.0, 6900.0, 7000.0]

    wavelength_calibrator = WavelengthCalibrate()
    wavelength_calibrator.set_data_raw(y_dat, 400)
    wavelength_calibrator.set_data_calibration(x_cal)
    wavelength_calibrator.make_window()
    wavelength_calibrator.write_to_file("../data/arclamps/curves/", flux=True)
