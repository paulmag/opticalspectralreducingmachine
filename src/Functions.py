"""General handy functions.
Recommended namespace (for now at least): func
    ex: import Functions as func
"""

import os
import numpy as np


def to_list(x, dtype=None, separator=" "):
    """Converts any sequence or non-sequence into an array.

    The use of a numpy array is primarely used because then it is easy to
    also convert the type. Except for that it could just as well be a list.

    If x is a string, split the string with the separator.
    If x is a sequence, convert it into an array.
    If x is a non-sequence, put it into a size-1 array.

    x: (anything)  Something to be converted into an array.
    dtype: (type) What type of objects the array contains. F.ex. float. If
        None, numpy will interpret the type itself.
    separator: (string) If x is a string to be split, this is the separator.
        Usually a space or comma.
    """

    if isinstance(x, basestring):  # If x is a string.
        x = np.array(x.split(separator), dtype=dtype)
    elif isinstance(x, dict):  # If x is a dictionairy.
        x = np.array([x], dtype=dtype)
    else:
        try:
            iter(x)  # If x is a sequence.
            x = np.array(x, dtype=dtype)
        except TypeError:  # If x is a non-sequence.
            x = np.array([x], dtype=dtype)

    return x


def tmedian(x, treshold=3, iterations=5, return_array=False):
    """
    x: array-like sequence of numbers
    treshold: Trimming limit in units of std.
    return_array: Return the trimmed sequence itself instead of its median.
    Intention of function is to return the baseline/constant parameter in a
    dataset that consists of one or several peaks. It finds the median with
    several iterations, where more peaks are trimmed away with each iteration.
    """

    for i in range(iterations):
        x0 = np.median(x)
        std = np.sqrt(np.mean(abs(np.asarray(x) - x0)**2))
        # List only intensities within treshold*std of the median
        # (removes the peaks):
        x_trimmed = []
        for value in x:
            if (x0 - treshold * std < value < x0 + treshold * std):
                x_trimmed.append(value)
        x = x_trimmed

    if return_array:
        return np.asarray(x)
    else:
        return np.median(x)


def moffat(x, A=1, x0=0, HWHM=1, MI=1, p0=0, p1=0, params=None):
    """
    x: y = moffat(x)
    A: amplitude of peak
    x0: x-position of peak
    HWHM Half-width at half maximum
    MI: Moffat Index (M=1 is quite similar to gauss)
    p0, p1: linear
    params: List of arguments, can be used instead of giving args individually.
    """
    if params is not None:
        A = params[0]
        if len(params) >= 2:
            x0 = params[1]
            if len(params) >= 3:
                HWHM = params[2]
                if len(params) >= 4:
                    MI = params[3]
                    if len(params) >= 5:
                        p0 = params[4]
                        if len(params) >= 6:
                            p1 = params[5]
    u = (x - x0) / HWHM
    return A / (u**2 + 1)**MI + p0 + p1*x


def gauss(x, A=1, x0=0, sigma=1, p0=0, p1=0, params=None):
    """The gaussian function.
    x: y = gauss(x)
    A: amplitude of peak
    x0: x-position of peak
    sigma: standard deviation
    p0, p1: linear
    params: List of arguments, can be used instead of giving args individually.
    """
    if params is not None:
        A = params[0]
        if len(params) >= 2:
            x0 = params[1]
            if len(params) >= 3:
                sigma = params[2]
                if len(params) >= 4:
                    p0 = params[3]
                    if len(params) >= 5:
                        p1 = params[4]
    return A * np.exp( - 0.5 * ((x - x0) / sigma)**2 ) + p0 + p1*x


def gauss_m(x, A=1, x0=0, HWHM=1., p0=0, p1=0, params=None):
    """Gaussian function with the parameter HWHM instead of sigma (standard
    deviation).
    """
    if params is not None and len(params) >= 3:
        params[2] = params[2] / np.sqrt(2 * np.log(2))
        return gauss(x, params=params)
    else:
        sigma = HWHM / np.sqrt(2 * np.log(2))
        return gauss(x, A, x0, sigma, p0, p1)


def polynomial(x, params):
    """
    x: y = polynomial(x)
    params: List of coefficients. Highest order coeff first, order=0 is the last.
    """
    order = len(params) - 1
    x_new = 0
    for i in range(order + 1):
        x_new += params[i] * x**(order - i)
    return x_new


def RMSE(data1, data2):
    """
    Find the root-mean-square-error between two datasets that are assumed to
    be similar/represent the same data.
    data1 and data2 must be of the same shape.
    """
    return np.sqrt(np.mean( (  np.asarray(data1) - np.asarray(data2) )**2 ))


def interpolate(x1, y1, x2):
    """
    (x1, y1) is a set of xy-points.
    Find the interpolation y2 of y1 that fits together with x2 so that we get
    (x2, y2) which describes the same data as (x1, y1), but with a different
    spacing.
    """

    # Cut away the parts of x2 that might be "outside" the boundaries of x1:
    for i in range(x2.size):
        if x2[i] > x1[0]: # this will happen once and only once
            i_min = i
            break

    for i in range(x2.size-1 , -1, -1): # backwards loop
        if x2[i] < x1[-1]: # this will happen once and only once
            i_max = i
            break

    x2 = x2[i_min : i_max+1] # x2 is now within limits
    y2 = np.zeros(x2.size)

    j_start = 0
    for i in range(x2.size):
        for j in range(j_start, x1.size):
            if x1[j] < x2[i] and x2[i] < x1[j+1]: # x1[j] < x2[i] < x1[j+1]
                weight = (x2[i] - x1[j]) / (x1[j+1] - x1[j]) # weight neighbours
                y2[i] = (1 - weight) * y1[j] + weight * y1[j+1]
                j_start = j # keep iterating from the same place
                break

    return x2, y2, (i_min, i_max)


def make_folder(directory, warning=False):
    """Make the directory, but do nothing if it already exists. Optionally
    print a warning if it already exists, if there should be a reason for
    that.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        if warning:
            print "%s already exists." % (directory)


def treshold(x, low=None, high=None):
    """Caps an array with a lower and/or higher treshold.
    Assumes low < high if both is given.
    x: array-like object
    low: lowest allowed value in x
    high: highest allowed value in x
    """
    x = np.array(x)
    if low is not None:
        x[x<low] = low
    if high is not None:
        x[x>high] = high
    return x


def pretty_dict(d, indent=0, increment=4):
    """A pretty hierarchy print for dictionairies.
    Its intended use is for reduction blocks, which is why it is assumed
    that the leaf nodes are Images.
    """
    for key, value in d.iteritems():
        print ' ' * indent + str(key)
        if isinstance(value, dict):
            pretty_dict(value, indent+increment, increment)
        elif isinstance(value, (list, tuple)):
            print ' ' * (indent+increment) + str(["Image"]*len(value))
        elif isinstance(value, np.ndarray):
            print ' ' * (indent+increment) + "Image"
        else:
            print ' ' * (indent+increment) + str(value)


def transpose(nested):
    """Takes a nested list and reverses the nesting.
    F.ex.: The list [[a,b,c], [x,y,x]] becomes [[a,x], [b,y], [c,z]].
    The nested list must be rectangular, (like a matrix).
    Only the two first dimensions are affected. Whatever might be in the
    third dimension and beyond is unaffected.
    """
    # shape = np.shape(nested)  # Suddenly this stopped working...
    try:
        shape = len(nested), len(nested[0])
    except IndexError:
        raise TypeError("Need a nested list of at least 2 dimensions.")
    if len(shape) < 2:
        raise TypeError("Need a nested list of at least 2 dimensions.")
    nested_reshaped = []
    for j in xrange(shape[1]):
        nested_reshaped.append([])
        for i in xrange(shape[0]):
            nested_reshaped[~0].append(nested[i][j])
    return nested_reshaped


def xfloat(x):
    """More generic float converter that conserves None."""
    if x in [None, "NONE", "None", "none"]:
        return None
    else:
        return float(x)


def monotonic(x):
    """Returns True if the sequence is monotonic.
    x: array-like (1D)
    """
    if all(x[i] <= x[i+1] for i in xrange(len(x) - 1)):
        # Always ascending.
        return True
    elif all(x[i] >= x[i+1] for i in xrange(len(x) - 1)):
        # Always descending.
        return True
    else:
        # Not monotonic.
        return False


def BIC(model, params, xdata, ydata, yerror):
    """The Bayesian Information Crierion."""
    ymodel = model(xdata, params=params)
    return (
        ydata.size * np.log(
            1. / (ydata.size - 1) *
            np.sum(((ydata - ymodel) / yerror)**2)
        ) + len(params) * np.log(ydata.size)
    )


def get_best_polymodel(xdata, ydata, yerror, weights=None, order_min=1, order_max=15):
    if weights is None:
        weights = 1. / yerror
    BICs = []
    p_opts = []
    for order in xrange(order_min, order_max+1):
        p_opt = np.polyfit(xdata, ydata, deg=order, w=weights)
        BICs.append(BIC(
            polynomial,
            p_opt,
            xdata,
            ydata,
            yerror,
        ))
        p_opts.append(p_opt)
    return p_opts[np.argmin(BICs)]


def subsequence(seq, fullsize=True):
    """
    Credit:
    http://stackoverflow.com/questions/3992697/longest-increasing-subsequence
    """

    M = [None] * len(seq)    # offset by 1 (j -> j-1)
    P = [None] * len(seq)

    # Since we have at least one element in our list, we can start by
    # knowing that the there's at least an increasing subsequence of length one:
    # the first element.
    L = 1
    M[0] = 0

    # Looping over the sequence starting from the second element
    for i in range(1, len(seq)):
        # Binary search: we want the largest j <= L
        #  such that seq[M[j]] < seq[i] (default j = 0),
        #  hence we want the lower bound at the end of the search process.
        lower = 0
        upper = L

        # Since the binary search will not look at the upper bound value,
        # we'll have to check that manually
        if seq[M[upper-1]] < seq[i]:
            j = upper

        else:
            # actual binary search loop
            while upper - lower > 1:
                mid = (upper + lower) // 2
                if seq[M[mid-1]] < seq[i]:
                    lower = mid
                else:
                    upper = mid

            j = lower    # this will also set the default value to 0

        P[i] = M[j-1]

        if j == L or seq[i] < seq[M[j]]:
            M[j] = i
            L = max(L, j+1)

    # Building the result: [seq[M[L-1]], seq[P[M[L-1]]], seq[P[P[M[L-1]]]], ...]
    result = []
    pos = M[L-1]
    for _ in range(L):
        result.append(seq[pos])
        pos = P[pos]

    result = np.array(result[::-1])    # reversing

    if not fullsize:
        return result

    # Rebuild original sequence
    subseq = np.zeros(len(seq)) * np.nan
    for a in result:
        for i, b in enumerate(seq):
            if a == b:
                subseq[i] = a
            elif b > a:
                break
        if np.sum(subseq[np.where(subseq == a)].size) > 1:  # Remove duplicates.
            subseq[np.where(subseq == a)] = np.nan

    return subseq
