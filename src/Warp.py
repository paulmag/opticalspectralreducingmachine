import os
import numpy as np
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter1d, median_filter
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import astropy.io.ascii as asciitable
    # asciitable has been deprocated and merged into astropy
pylab.rcParams['figure.figsize'] = 16, 12

from scipy.interpolate import griddata
from scipy import stats
from skimage import io, transform
from astropy.io import fits
from skimage.measure import ransac
from skimage.transform import PolynomialTransform

import Functions as func
from WavelengthCalibrate import WavelengthCalibrate



class PolynomialTransform_3(PolynomialTransform):
    def estimate(*data):
        return PolynomialTransform.estimate(*data, order=3)



class Warp:

    def __init__(self, axis_spec=None):
        self.arcimage = None
        self.position_spec = None
        self.sources      = np.zeros((0, 2))
        self.destinations = np.zeros((0, 2))
        if axis_spec is not None:
            self.axis_spec = axis_spec
        if self.axis_spec not in (0, 1):
            raise ValueError(
                "<dispaxis> must be '0' or '1'. "
                "Which way is the spectrum trace oriented?"
            )


    def set_arcimage(self, arcimage, radius=100, trim=2./3., find=True, trace=False, show=False):
        """An image full of slightly curved lines that are supposed to be
        straight and perpendicular to the spectral line.
        """

        if self.axis_spec == 1:
            arcimage = arcimage.transpose()

        arcimage = median_filter(
                arcimage,
                size=(3, 3),
                mode="nearest",
            )

        # TODO axis might be 0.
        if self.position_spec is None:
            position_spec = radius = y.shape[1] / 2
        else:
            position_spec = self.position_spec
        # Trimmed mean:
        trim = int(round(radius * trim))
        y = arcimage[:, self.position_spec-radius : self.position_spec+radius]
        y = np.mean(np.sort(y, axis=1)[:, trim:-trim], axis=1)
        y -= func.tmedian(y)

        if self.arcimage is None:  # self.y is also None.
            self.arcimage = arcimage
            self.y = y
            self.N = np.shape(self.arcimage)  # (y,x)
            self.pix_vals = (
                np.linspace(0, self.N[0]-1, self.N[0]).astype(int),
                np.linspace(0, self.N[1]-1, self.N[1]).astype(int),
            )
            self.N_peaks = 0
            self.peaks = np.zeros((self.N[1], self.N_peaks, 4)) * np.nan
            self.eps = 0.0003, 0.0003  #TODO Set proper values here.

        elif np.shape(arcimage) == self.N:
            self.arcimage += arcimage
            self.y += y

        if find:
            self._find_lines(show=show, arcimage=arcimage)
        if trace:
            self.trace_lines(show=show)


    def set_position_spec(self, position_spec):
        self.position_spec = int(round(position_spec))


    def _add_peak(self, xdata, lineno, p_opt, initial=False):
        """Add a new selection to the list (array) of peaks. This method
        exists since you cannot directly append to an array.
        """
        if initial:
            self.N_peaks += 1
            new = np.zeros((self.N[1], 1, 4)) * np.nan
            new[xdata, 0] = np.hstack((p_opt, 0))
            self.peaks = np.hstack((self.peaks, new))
        else:
            self.peaks[xdata, lineno] = p_opt


    def _find_lines(self, arcimage=None, show=False):
        """Automatically finds and selects all peaks in both spectrums,
        using the peak finder in WavelengthCalibrate.
        """

        lFinder = WavelengthCalibrate(interactive=False, axis_spec=0)
        lFinder.set_data_raw(arcimage, self.position_spec, normalize=False)
        lFinder._find_all(ax_no=0, update_window=False)
        for p_opt in lFinder.peaks[0]:
            self._add_peak(
                xdata = self.position_spec,
                lineno = None,
                p_opt = p_opt,
                initial = True,
            )

        xdata = self.position_spec

        if show:
            plt.figure()
            plt.title("%d lines detected." % self.N_peaks)
            plt.plot (
                self.pix_vals[0],
                self.arcimage[:, xdata],# / np.max(self.arcimage[:, xdata]),
                ":k",
                label = "data",
            )
            for peak in self.peaks[xdata]:
                plt.plot (
                    self.pix_vals[0],
                    func.gauss(self.pix_vals[0], peak[0], peak[1], peak[2], peak[3]),
                    label = "detected peak",
                )
            plt.legend()
            plt.show()


    def trace_lines(self, step=10, find=False, show=False):
        radius = int(np.ceil(step))

        if find:
            _find_lines()
        self.peaks[self.position_spec-step, :] = self.peaks[self.position_spec, :]
        self.peaks[self.position_spec+step, :] = self.peaks[self.position_spec, :]

        for k in xrange(self.N_peaks):
            badcount = 0
            for i in self.pix_vals[1][self.position_spec+step :: +step]:
                if not self._select(
                    xdata = i,
                    ydata = 2*self.peaks[i-step][k][1] - self.peaks[i-2*step][k][1],
                    intensity = self.peaks[i-step][k][0],
                    lineno = k,
                    radius = radius,
                ):
                    # If peak was not found interpolate from the last two:
                    self.peaks[i, k] = self.peaks[i-step, k] + \
                        0.5 * (self.peaks[i-step, k] - self.peaks[i-2*step, k])
                    badcount += 1
                    if badcount >= 5:
                        break
            badcount = 0
            for i in self.pix_vals[1][self.position_spec-step :: -step]:
                if not self._select(
                    xdata = i,
                    ydata = 2*self.peaks[i+step][k][1] - self.peaks[i+2*step][k][1],
                    intensity = self.peaks[i+step][k][0],
                    lineno = k,
                    radius = radius,
                ):
                    # If peak was not found interpolate from the last two:
                    self.peaks[i, k] = self.peaks[i+step, k] + \
                        0.5 * (self.peaks[i+step, k] - self.peaks[i+2*step, k])
                    badcount += 1
                    if badcount >= 5:
                        break

        sources = []
        destinations = []
        for k in xrange(self.N_peaks):
            for i in xrange(self.N[1]):
                if np.isfinite(self.peaks[i, k, 1]):
                    sources.append([i, self.peaks[i, k, 1]])
                    destinations.append(
                        [i, self.peaks[self.position_spec, k, 1]]
                    )
        self.sources      = np.vstack([self.sources,      sources])
        self.destinations = np.vstack([self.destinations, destinations])

        if show:
            plt.figure()
            plt.imshow(
                self.arcimage,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = np.median(self.arcimage),
                vmax = np.median(self.arcimage) \
                    + (self.arcimage.max() - self.arcimage.min()) * 0.01,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(np.array(self.sources)[:, 0],      np.array(self.sources)[:, 1],'+r')
            plt.plot(np.array(self.destinations)[:, 0], np.array(self.destinations)[:, 1], '+b')
            plt.show()


    def save_positions(self, path):
        """"Write the positions and destinations to a file so the same
        warping can easily bea repeated.
        """
        positions = np.hstack((self.sources, self.destinations))
        outfile = open(path, "w")
        for pos in positions:
            outfile.write("%f %f %f %f\n" % tuple(pos))
        outfile.close()


    def read_positions(self, path):
        """"Read the positions and destinations from a file so the same
        warping can easily be repeated.
        """
        positions = []
        infile = open(path, "r")
        for line in infile:
            positions.append([float(value) for value in line.split()])
        infile.close()
        positions = np.array(positions)
        self.sources      = np.vstack([self.sources,      positions[:, 0:2]])
        self.destinations = np.vstack([self.destinations, positions[:, 2:4]])


    def _select(self,
        xdata,
        ydata,
        intensity=0.5,
        radius=5,
        lineno=None,
        initial=False,
        arcimage=None,
        show=False,
    ):
        """Attempts to select a peak at the given location.
        show: (bool) Print errormessage to explain why selection failed, if
            it fail.
        Returns True if a selection is made, False if not.
        """
        # Click position used as initial guess for a gaussian fit:
        p0 = [intensity, ydata, 2, 0]
        if arcimage is not None:
            data = np.median(
                arcimage[:, xdata-radius : xdata+radius+1],
                axis = 1,
            )
        else:
            data = np.median(
                self.arcimage[:, xdata-radius : xdata+radius+1],
                axis = 1,
            )
        try:
            p_opt, p_cov = curve_fit(
                func.gauss,
                self.pix_vals[0],
                data,
                p0,
            )
        except ValueError:
            #TODO 'data' is sometimes just NaNs. Why does this happen?
            # This was maybe just related to the wrong version of numpy problem.
            if show:
                print "ValueError: No peak found. No selection made."
                print "NaNs:", np.sum(np.isnan(data))
            return False
        except RuntimeError:
            if show:
                print "RuntimeError: No peak found. No selection made."
            return False

        # Peak is assumed to only exist within 3*std of first fit:
        treshold = p_opt[1] - 3 * p_opt[2], p_opt[1] + 3 * p_opt[2]
        treshold_index = (
            (np.abs(self.pix_vals[0] - treshold[0])).argmin(),
            (np.abs(self.pix_vals[0] - treshold[1])).argmin(),
        )

        # Do a second fit within treshold to avoid bias by neighbour peaks:
        p0 = p_opt
        if arcimage is not None:
            data = np.median(
                arcimage[:, xdata-radius : xdata+radius+1][treshold_index[0] : treshold_index[1]],
                axis = 1,
            )
        else:
            data = np.median(
                self.arcimage[:, xdata-radius : xdata+radius+1][treshold_index[0] : treshold_index[1]],
                axis = 1,
            )
        try:
            p_opt, p_cov = curve_fit(
                func.gauss,
                self.pix_vals[0][treshold_index[0] : treshold_index[1]],
                data,
                p0,
            )
        except TypeError:
            if show:
                print "TypeError: No proper peak found. No selection made."
            return False
            #TODO Set a minimum limit on treshold instead so this does not
            # happen.
        except RuntimeError:
            if show:
                print "RuntimeError: No peak found. No selection made."
            return False

        # Check if this point as been selected before:
        for x_point in self.peaks[xdata][:, 1]:
            if abs(p_opt[1] - x_point) < self.eps[0]:
                # It is so close it is assumed to be the same point.
                if show:
                    print (
                        "This point is selected before. "
                        "No new selection made."
                    )
                return False

        # Check if a proper peak was found or just nothing:
        if p_opt[0] < self.eps[1]:
            if show:
            # Peak so low it is assumed there is actually no peak here.
                print "The peak found is too small. No selection made."
            return False

        # Check if peak is in totally wrong place:
        if abs(p_opt[1] - ydata) > 100:
            if show:
            # Peak so low it is assumed there is actually no peak here.
                print "The peak found is in the wrong place. No selection made."
            return False

        # Check if peak is too close to its nearest neighbour:
        if len(self.peaks[xdata]) > 0 and initial:
            tol = 1  # sigma tolerance between peaks.
            tooclose = False
            smallest = 1e9  # Arbitrary large value.
            for i in range(len(self.peaks[xdata])):
                if abs(p_opt[1] - self.peaks[xdata][i, 1]) < smallest:
                    smallest = abs(p_opt[1] - self.peaks[xdata][i, 1])
                    i_check = i
            other = self.peaks[xdata][i_check]
            if other[1] < p_opt[1]:
                if other[1] + other[2] * tol > p_opt[1] - p_opt[2] * tol:
                    tooclose = True
            else:
                if other[1] - other[2] * tol < p_opt[1] + p_opt[2] * tol:
                    tooclose = True
            if tooclose:
                if show:
                    print (
                        "The peak found overlaps with another peak. "
                        "No selection made."
                    )
                return False

        # Check if the peak is similar to the real data:
        fit = func.gauss(
            self.pix_vals[0][treshold_index[0] : treshold_index[1]],
            p_opt[0],
            p_opt[1],
            p_opt[2],
            p_opt[3],
        )
        data = self.arcimage[:, xdata][treshold_index[0] : treshold_index[1]]
        diff = np.abs(fit - data) / fit
        if np.mean(diff) >= 0.5:
            if show:
                print (
                    "The peak found differs a lot from the data in the area. "
                    "No selection made."
                )
            return False

        # Save selection:
        self._add_peak(xdata, lineno, p_opt, initial)
        if show:
            print "Found peak at:", xdata, ydata, p_opt[1]

        return True


    def compute_transformation(self, find=False, trace=False, show=False):

        if find:
            self._find_lines(show=show)
        if trace:
            self.trace_lines(show=show)

        # We call RANSAC using the new function. I have set the min_samples, residual_thresholds and max_trials by error and trials...
        # Let's hope it works for all our cases...

        self.sources = np.array(self.sources)
        self.destinations = np.array(self.destinations)

        self.t, self.inliers = ransac(
            (self.destinations, self.sources),
            PolynomialTransform_3,
            min_samples = 50,
            residual_threshold = 1.0,
            max_trials = 100,
        )

        if show:
            arc_warped = transform.warp(
                self.arcimage,
                self.t,
                order = 5,
                mode = 'constant',
                cval = float('0'),
            )
            plt.figure()
            plt.imshow(
                arc_warped,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = np.median(self.arcimage),
                vmax = np.median(self.arcimage) \
                    + (self.arcimage.max() - self.arcimage.min()) * 0.01,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(self.sources[:, 0], self.sources[:, 1],'+r')
            plt.plot(self.destinations[:, 0], self.destinations[:, 1], '+b')
            plt.show()


    def apply_transformation_binary(self, image, show=False):

        if self.axis_spec == 1:
            image = image.transpose()
        self.N = np.shape(image)  # (y,x)

        points = np.array(np.where(image==False)).transpose()  # All points that are 0.
        points = np.fliplr(points)  # Need (x,y) format in transformation.
        points = self.t(points)  # Transform the points.
        points = np.fliplr(points)  # Back to (y,x).
        precise = points % points.astype(int) == 0  # Map of points that are accurate (not floats).
        for i, p in enumerate(precise):
            point = points[i]
            if (p == False).all():
                np.vstack((points, point+1, [point+1, point], [point, point+1]))
            elif p[0] == False:
                np.vstack((points, [point+1, point  ]))
            elif p[1] == False:
                np.vstack((points, [point  , point+1]))

        img_warped = np.ones(image.shape)
        for point in points:
            try:
                img_warped[point[0], point[1]] = 0
            except IndexError:
                print point, "is now outside the image."

        if show:
            plt.figure()
            plt.imshow(
                image,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = 0,
                vmax = 1,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(self.sources[:, 0], self.sources[:, 1],'+r')
            plt.plot(self.destinations[:, 0], self.destinations[:, 1], '+b')

            plt.figure()
            plt.imshow(
                img_warped,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = 0,
                vmax = 1,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(self.sources[:, 0], self.sources[:, 1],'+r')
            plt.plot(self.destinations[:, 0], self.destinations[:, 1], '+b')
            plt.show()

        if self.axis_spec == 1:
            img_warped = img_warped.transpose()
        self.image_warped = img_warped
        return img_warped


    def apply_transformation(self, image, show=False):
        # Warping the image
        # order=5 means Bi-quintic interpolation
        # cvla=0 means value outside the image boundaries will be set to 0

        if self.axis_spec == 1:
            image = image.transpose()
        self.N = np.shape(image)  # (y,x)

        img_max = np.max(image)
        image = np.array(image) / img_max
        img_warped = transform.warp(
            image,
            self.t,
            order = 5,
            mode = 'constant',
            cval = float('0'),
        )

        if show:
            plt.figure()
            plt.imshow(
                image,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = np.median(image),
                vmax = np.median(image) \
                    + (image.max() - image.min()) * 0.01,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(self.sources[:, 0], self.sources[:, 1],'+r')
            plt.plot(self.destinations[:, 0], self.destinations[:, 1], '+b')

            plt.figure()
            plt.imshow(
                img_warped,
                cmap = 'gray',
                interpolation = 'nearest',
                vmin = np.median(image),
                vmax = np.median(image) \
                    + (image.max() - image.min()) * 0.01,
            )
            plt.xlim(0, self.N[1])
            plt.ylim(self.N[0], 0)
            plt.plot(self.sources[:, 0], self.sources[:, 1],'+r')
            plt.plot(self.destinations[:, 0], self.destinations[:, 1], '+b')
            plt.show()

        img_warped *= img_max
        if self.axis_spec == 1:
            img_warped = img_warped.transpose()
        self.image_warped = img_warped
        return img_warped


    def get_warped(self):
        """Get the last warped image."""
        return self.image_warped

        # Eventually save the warped image to disk
        # if os.path.isfile('/Users/hbouy/work/paul/warp/scikit/warped.fits'):
            # os.remove('/Users/hbouy/work/paul/warp/scikit/warped.fits')
        # hdu = fits.PrimaryHDU(img_warped*maxi)
        # hdulist = fits.HDUList([hdu])
        # hdulist.writeto('/Users/hbouy/work/paul/warp/scikit/warped.fits')


    """### Estimating the added noise ###

    # First we compute the inverse transformation
    invt, inliers = ransac((sources,destinations), PolyTF_3, min_samples=20,
                                   residual_threshold=1.0, max_trials=100)

    # and apply it to the warped image
    img_unwarped = transform.warp(img_warped, invt, order=5, mode='constant',cval=float('0'))

    # Plot
    plt.figure()
    plt.imshow(img_unwarped, cmap='gray', interpolation='nearest',clim=(0,1.0))
    plt.plot(sources[:,0],sources[:,1],'+r')
    plt.xlim(0,img_warped.shape[1])
    plt.ylim(0,img_warped.shape[0])
    plt.show()


    # Then we compute the ratio of the original image and the unwarped image
    # Note that we are interested above all in the region near the scientific target.
    # In the case of this dataset, the scientific target is located around x=480
    # you will of course use the value that you get from the tracing (just take the median value over all rows)
    # I use an arbirtray window of +-30 pixels on each side. We could instead use the same window as used to extract the spectrum.
    # Although we probably still dont know how much it is? cannot remember...

    ratio = img_unwarped[:,450:510]/orig[:,450:510]

    # We compute the error in %
    error=np.abs(1.-ratio[np.isfinite(ratio)].flatten())*100.

    #maxi=np.max(ratio[np.isfinite(ratio)])
    #median=np.median(ratio[np.isfinite(ratio)])
    #stddev=np.std(ratio[np.isfinite(ratio)])
    #print( maxi, median,stddev)

    # Plot distribution of errors
    plt.hist(error,400, normed=True, cumulative=True,color='r', alpha=0.5,histtype='stepfilled')
    plt.xlim(0,5.)
    plt.xlabel('Error in %')
    plt.ylabel('Cumulative Distribution')

    # Compute how many sources have error <2%
    print 'A total of',float(np.sum(error <= 2))/float(error.size)*100,"% of the sources have an error smaller than 2%"

    # So we see that the added noise is small!!"""


    """# Eventually save the unwarped images
    if os.path.isfile('/Users/hbouy/work/paul/warp/scikit/unwarped.fits'):
        os.remove('/Users/hbouy/work/paul/warp/scikit/unwarped.fits')
    hdu = fits.PrimaryHDU(img_unwarped*np.max(f[0].data))
    hdulist = fits.HDUList([hdu])
    hdulist.writeto('/Users/hbouy/work/paul/warp/scikit/unwarped.fits')"""
