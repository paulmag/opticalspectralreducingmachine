import os

from astropy.io import fits

from ReductionBlock import ReductionBlock


class RBRead:

    def __init__(self):
        self.RBs = {}

    def set_path(self, path):
        """Path of the folder that contains the reduction blocks."""
        self.path = path

    def extract_RB(self):
        """Makes a dict of lists of ReductionBlock instances."""
        infiles, filenames = self.find_files(self.path)
        for infile, filename in zip(infiles, filenames):
            RB = ReductionBlock(infile=infile)
            self.RBs.setdefault(RB.objtype, []).append(RB)

    def find_files(self, path=None):
        """
        Makes a list of opened RB-txt-files.
        The path to this folder can be given as an argument, but it's not
        necessary if the path is already set.
        """
        if path is None:
            path = self.path
        infiles = []
        for dirname, dirnames, filenames in os.walk(path):
            for filename in filenames:
                filepath = os.path.join(dirname, filename)
                if filepath.endswith(".txt"):
                    infiles.append(open(filepath))
        return infiles, filenames


if __name__ == "__main__":
    """
    Sample run.
    """

    reader = RBRead()
    reader.set_path(os.path.expanduser(
        "~/practice/test_results/GTC_LDB_Pleiades/rbs")
    )
    reader.extract_RB()
