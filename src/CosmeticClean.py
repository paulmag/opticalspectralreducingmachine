import os
import numpy as np

from astropy.io import fits

import Functions as func



class CosmeticClean:
    """A bias_master and flat_master pair makes one CosmeticClean instance,
    which can be used to clean an image or a list of images.
    """

    def __init__(self):
        self.target = None
        self.bias_master = None
        self.flat_master = None

    def set_target(self, image):
        """Sets the target image to be cleaned (the science target or an
        STD). Should be a list of CCDs (2D arrays).
        """
        self.target = image

    def set_bias(self, image):
        """Sets the master bias. Should be a list of CCDs (2D arrays)."""
        self.bias_master = image

    def set_flat(self, image):
        """Sets the master flat. Should be a list of CCDs (2D arrays)."""
        self.flat_master = image

    def clean(self):
        """TODO Description."""
        for j, CCD in enumerate(self.target):
            self.target[j] = \
                (CCD - self.bias_master[j]) / self.flat_master[j]


    def write_to_file(self, path):
        """Save this object(s) to a file(s)."""
        hdus = \
            [fits.PrimaryHDU(None)] + \
            [fits.ImageHDU(CCD) for CCD in self.target]
        hdulist = fits.HDUList(hdus)
        if os.path.exists(path):
            # If the filename already existed the file will be overwritten:
            os.remove(path)
        hdulist.writeto(path)


if __name__ == "__main__":
    pass
