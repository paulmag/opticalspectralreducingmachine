import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.coordinates import SkyCoord, match_coordinates_sky
import difflib

import Functions as func
from XML import XMLInstrument as XMLI, XMLActions as XMLA
from ReductionBlock import ReductionBlock
from ReductionBlockSet import ReductionBlockSet
from RBMake import RBMake
from RBRead import RBRead
import cosmics
from CosmeticMake import CosmeticMake
from CosmeticClean import CosmeticClean
from SpectrumLineFind import SpectrumLineFind
from Warp import Warp
from Reduction import Reduction
from WavelengthCalibrate import WavelengthCalibrate
from FluxCalibrate import FluxCalibrate


def make_RB():
    #TODO Makes them all (calibration and science RBs) in one bunch. Is
    # supposed to separate them.
    rbMaker = RBMake()
    rbMaker.xml_i = xml_i
    rbMaker.set_path_raw(xml_a.path_raw)
    rbMaker.set_path_xml(path_i)
    rbMaker.set_path_rbs(xml_a.path_rbs)
    rbMaker.read_xml()
    rbMaker.find_files()
    rbMaker.find_keywords()
    rbMaker.extract_data()
    RBs = {}
    for RB in rbMaker.RBs:
        RBs.setdefault(RB.objtype, []).append(RB)

    # Remove the target and std images that does not have grism or mask:
    # (Need to remove from two lists.)
    not_use = (None, "OPEN", "NOMASK")
    to_remove = []
    for i, RB in enumerate(RBs["target"]):
        if RB.grism in not_use or RB.maskname in not_use:
            to_remove.append(i)
    for i in to_remove[::-1]:
        # Deletes from the back to not change the indexing.
        del RBs["target"][i]
    to_remove = []
    for i, RB in enumerate(RBs["std"]):
        if RB.grism in not_use or RB.maskname in not_use:
            to_remove.append(i)
    for i in to_remove[::-1]:
        # Deletes from the back to not change the indexing.
        del RBs["std"][i]
    to_remove = []
    for i, RB in enumerate(ReductionBlock.instances):
        if RB.objtype in ["target", "std"]:
            if (RB.grism in not_use or RB.maskname in not_use):
                to_remove.append(i)
    for i in to_remove[::-1]:
        del ReductionBlock.instances[i]

    # RBs now contain all the correct (virtual) reduction blocks.
    ReductionBlock.write_to_files(xml_a.path_rbs)
    RBs["star"] = RBs["target"] + RBs["std"]
    return RBs


def read_RB():
    rbReader = RBRead()
    rbReader.set_path(xml_a.path_rbs)
    try:
        rbReader.extract_RB()
    except UnboundLocalError:
        print (
            "There is no reduction blocks in '%s'."
            % xml_a.path_rbs
        )
        sys.exit(1)
    RBs = rbReader.RBs
    RBs["star"] = RBs["target"] + RBs["std"]
    return RBs


def group_RBs(RBs):

    no_of_targets = len(RBs["target"])
    counter_targets = 0
    for target in RBs["star"]:
        if counter_targets < no_of_targets:
            RB_set = ReductionBlockSet(target=target)
            RB_set.turn_target()
        elif xml_a.do_cal_flux is None:
            continue
        else:
            RB_set = ReductionBlockSet(target=target)
            RB_set.turn_std()
        counter_targets += 1

        ### Find best bias:
        diff_best = 9001  # Arbitrary too large number.
        for ii, bias in enumerate(RBs["bias"]):
            if (target.gain != bias.gain).any():
                continue
            diff_current = abs(target.jdate - bias.jdate)
            if diff_current < diff_best:
                diff_best = diff_current
                closest_bias = ii
        RB_set.bias = RBs["bias"][closest_bias]
        #TODO Am I supposed to to check this:?
        if abs(target.jdate - RB_set.bias.jdate) > xml_i.jdate_limit_bias:
            print "Warning: There is a large time between target and bias."

        ### Find best flat:
        mask_cutoff = 1.0  # Only accept perfect match for maskname.
        closest_flat = None
        while mask_cutoff >= 0:
            # Two attempts. If no flat found on first attempt try again with
            # less strict requirements.
            diff_best = 9001  # Arbitrary too large number.
            for ii, flat in enumerate(RBs["flat"]):
                if (target.gain != flat.gain).any():
                    continue
                if target.grism != flat.grism:
                    continue
                if difflib.get_close_matches(
                    target.maskname,
                    [flat.maskname],
                    n = 1,
                    cutoff = mask_cutoff,
                ) == []:
                    # Only accept similar mask, within current cutoff limit.
                    continue
                diff_current = abs(target.jdate - flat.jdate)
                if diff_current < diff_best:
                    diff_best = diff_current
                    closest_flat = ii
            if closest_flat is None:
                mask_cutoff -= 0.1
                # print (  # This warning is kind of useless. Commented for now.
                    # "No flat with correct mask was found. "
                    # "Trying again with smaller cutoff: %g." % mask_cutoff
                # )
                continue
            else:
                RB_set.flat = RBs["flat"][closest_flat]
                break
            #TODO Am I supposed to to check this:?
            if abs(target.jdate - RB_set.flat.jdate) > xml_i.jdate_limit_flat:
                print "Warning: There is a large time between target and flat."

        ### Find best arc:
        infile = open(
            os.path.dirname(sys.argv[0]) + "/../data/arclamps/gases.txt",
            "r",
        )
        gases = [line.strip() for line in infile.readlines()]
        infile.close()
        for ii, arc in enumerate(RBs["arc"]):
            if target.grism != arc.grism:
                continue
            diff_current = abs(target.jdate - arc.jdate)
            if diff_current <= xml_i.jdate_limit_arc:
                RB_set.arc.append(RBs["arc"][ii])
            else:
                continue
            objname_gases = arc.objname.replace(xml_i.map_arc, "")
                # Removes the word "ArcLamp" (f.ex.) from the objname, so
                # that it should only contain the gas name(s) of the arclamp.
            arc.gas = []
            for gas in gases:
                if gas in objname_gases:
                    arc.gas.append(gas)
                    objname_gases = objname_gases.replace(gas, "")
                        # Remove from list for next iteration.
                else:
                    continue
            if len(arc.gas) == 0:
                print "Warning: The arc '%s' has no gases associated with it." \
                    % arc.filenames[0]
        if len(RB_set.arc) == 0:
            print "Warning: No arc RBs are associated with the star '%s'." \
                % target.filenames[0]
        else:
            print "%d arc RBs are associated with the star '%s'." \
                % (len(RB_set.arc), target.filenames[0])


    ### Find best standard star:
    if xml_a.do_cal_flux is None:
        # Exclude the use of a standard star and skip this section.
        return
    for RB_set_target in ReductionBlockSet.instances_target:
        target = RB_set_target.target
        diff_best = 9001  # Arbitrary too large number.
        ratio = 5.0
        # Scaling factor for jdate and airmass. ratio = number of jdays per
        # airmass unit.
        for ii, RB_set_std in enumerate(ReductionBlockSet.instances_std):
            std = RB_set_std.target
            if target.grism != std.grism:
                continue
            diff_current = np.sqrt(
                ((target.jdate - std.jdate) * ratio)**2 +
                (target.airmass - std.airmass)**2
            )
            if diff_current < diff_best:
                diff_best = diff_current
                closest_std = ii
        RB_set_target.std = ReductionBlockSet.instances_std[closest_std]
        #TODO Am I checking the correct things here:?
        if (abs(target.jdate - RB_set_target.std.target.jdate) >
            xml_i.jdate_limit_std
        ):
            print "Warning: There is a large time between target and std."
        if (abs(target.airmass - RB_set_target.std.target.airmass) >
            xml_i.airmass_limit_std
        ):
            print "Warning: There is a large airmass between target and std."

        infile = open(
            os.path.dirname(sys.argv[0]) + "/../data/stds/stars.txt",
            "r",
        )
        stars = []
        coords = []
        equinoxes = []  #TODO Unused.
        for line in infile.readlines():
            line = line.split()
            stars.append(line[0])
            coords.append((float(line[1]), float(line[2])))
            equinoxes.append(line[3])  #TODO Unused.
        coords = np.array(coords)
        infile.close()

        this = SkyCoord(
            ra = RB_set_target.std.target.ra,
            dec = RB_set_target.std.target.dec,
            unit = "deg",
            frame = "fk5",      #TODO Just assume this so far.
            equinox = "J2000",  #TODO
        )
        database = SkyCoord(
            ra = coords[:, 0],
            dec = coords[:, 1],
            unit = "deg",
            equinox = "J2000",  #TODO What if they have different equinoxes?
        )

        index, angle_diff, unused = match_coordinates_sky(this, database)
        # index, angle_diff = int(index), float(a / "deg")  # Convert back to numbers.
        if angle_diff.arcminute > 10:
            #TODO Limit is supposed to be 7, not 10, but that was too strict.
            raise Exception(
                "Could not find standard star %s in database.\n"
                "The closest star is %f arcminutes away."
                % (RB_set_target.std.target.objname, angle_diff.arcminute)
            )
        RB_set_target.std.target.star = stars[index]


def do_cosmicray_clean(show=False):

    for RB in ReductionBlockSet.instances:

        img = RB.target.get_data(imageno=0)
            #TODO Assumes just one spectrum for now.
        mask_cosmicray = []

        for j, CCD in enumerate(img):
            if show:
                plt.figure()
                plt.imshow(
                    CCD,
                    vmin = 0,
                    vmax = np.median(CCD) + (CCD.max() - CCD.min()) * 0.01,
                    cmap = plt.cm.gray,
                )
                plt.title("Image before")
            # Build the object :
            c = cosmics.cosmicsimage(
                CCD,
                gain = RB.target.gain[j],
                readnoise = xml_i.chips_list[j].readoutnoise,  #TODO Actually this depends on the gain.
                sigclip = xml_i.cray_sigclip,
                sigfrac = xml_i.cray_sigfrac,
                objlim = xml_i.cray_objlim,
            )
            # There are other options, check the manual...

            # Run the full artillery :
            c.run(maxiter=xml_i.cray_niter)
            if show:
                plt.figure()
                plt.imshow(
                    c.cleanarray,
                    vmin = 0,
                    vmax = np.median(CCD) + (CCD.max() - CCD.min()) * 0.01,
                    cmap = plt.cm.gray,
                )
                plt.title("Image after")
                plt.figure()
                plt.imshow(c.mask, cmap=plt.cm.gray)
                plt.title("The cosmic ray mask")
                plt.show()

            mask_cosmicray.append((c.mask == False).astype(int))
                # == False inverses the mask so that bad pixels are 0.
                # astropy needs int, not bool.

        RB.mask_cosmicray = mask_cosmicray

        hdus = \
            [fits.PrimaryHDU(None)] + \
            [fits.ImageHDU(CCD) for CCD in mask_cosmicray]
        hdulist = fits.HDUList(hdus)
        path = xml_a.path_cal + RB.target.get_write_name(".cosray.fits")
        if os.path.exists(path):
            # If the filename already existed the file will be overwritten:
            os.remove(path)
        hdulist.writeto(path)

            # Write the cleaned image into a new FITS file, conserving
            # the original header:
            # cosmics.tofits("clean.fits", c.cleanarray, header)


def make_cosmetic():
    for RB in ReductionBlockSet.instances:
        cMaker = CosmeticMake()
        cMaker.xml_i = xml_i
        cMaker.biases = RB.bias.get_data()
        cMaker.flats = RB.flat.get_data()
        cMaker.set_method("mean")
        cMaker.make_master("bias")
        cMaker.make_master("flat")
        cMaker.write_to_file(
            master_type = "bias",
            path = xml_a.path_cal + RB.bias.get_write_name(".fits"),
        )
        cMaker.write_to_file(
            master_type = "flat",
            path = xml_a.path_cal + RB.flat.get_write_name(".fits"),
        )
        cMaker.write_to_file(
            master_type = "mask",
            # Same params as the flat RB. Just with '.badpix' appended:
            path = xml_a.path_cal + RB.flat.get_write_name(".badpix.fits"),
        )
        RB.bias.master = cMaker.bias_master
        RB.flat.master = cMaker.flat_master
        RB.mask_badpixel = cMaker.mask


def read_cosmetic(bias=True, flat=True, warped=False):

    if warped:
        print "Warning: This should lead to some error."
        extension = ".warped.fits"
        errortext = " Maybe you forgot to process warping?"
    else:
        extension = ".fits"
        errortext = " Maybe you forgot to process instrumental calibration?"

    for RB in ReductionBlockSet.instances:
        try:
            if bias:
                HDU = fits.open(xml_a.path_cal + RB.bias.get_write_name(extension))
                RB.bias.master = [CCD.data for CCD in HDU[1:]]
                HDU.close()
            if flat:
                HDU = fits.open(xml_a.path_cal + RB.flat.get_write_name(extension))
                RB.flat.master = [CCD.data for CCD in HDU[1:]]
                HDU.close()
        except IOError as err:
            if not err.args:
                err.args=('',)
            err.args = ((str(err.args[0]) + errortext,) + err.args[1:])
            raise err


def read_weights(warped=True):

    if warped:
        extension = ".weight.warped.fits"
        errortext = " Maybe you forgot to process warping?"
    else:
        extension = ".fits"
        errortext = " Maybe you forgot to process instrumental calibration?"

    for RB in ReductionBlockSet.instances:
        try:
            if warped:
                HDU = fits.open(xml_a.path_cal + RB.target.get_write_name(extension))
                RB.flat.master = [CCD.data for CCD in HDU[1:]]
                HDU.close()
            else:
                # This is the same as read_cosmetic(flat=True, bias=False)
                HDU = fits.open(xml_a.path_cal + RB.flat.get_write_name(extension))
                RB.flat.master = [CCD.data for CCD in HDU[1:]]
                HDU.close()
        except IOError as err:
            if not err.args:
                err.args=('',)
            err.args = ((str(err.args[0]) + errortext,) + err.args[1:])
            raise err


def read_badpixel_mask(warped=False):

    for RB in ReductionBlockSet.instances:
        if warped:
            # Same params as the flat or target RB.
            # Just append '.badpix' to the filename.
            filename = RB.target.get_write_name(".badpix.warped.fits")
            errortext = " Maybe you forgot to process warping?"
        else:
            filename = RB.flat.get_write_name(".badpix.fits")
            errortext = " Maybe you forgot to process instrumental calibration?"
        try:
            HDU = fits.open(xml_a.path_cal + filename)
            RB.mask_badpixel = [CCD.data for CCD in HDU[1:]]
            HDU.close()
        except IOError as err:
            if not err.args:
                err.args=('',)
            err.args = ((str(err.args[0]) + errortext,) + err.args[1:])
            raise err


def read_cosmicray_mask(warped=False):

    if warped:
        extension = ".cosray.warped.fits"
        errortext = " Maybe you forgot to process warping?"
    else:
        extension = ".cosray.fits"
        errortext = " Maybe you forgot to process cosmic rays?"

    for RB in ReductionBlockSet.instances:
        try:
            HDU = fits.open(
                # Same params as the target RB. Just append '.mask' to the
                # filename and look in the calibration folder, not science:
                xml_a.path_cal +
                RB.target.get_write_name(extension)
            )
            RB.mask_cosmicray = [CCD.data for CCD in HDU[1:]]  # Or add new mask.
            HDU.close()
        except IOError as err:
            if not err.args:
                err.args=('',)
            err.args = ((str(err.args[0]) + errortext,) + err.args[1:])
            raise err


def do_cal_cosmetic():
    for RB in ReductionBlockSet.instances:
        cCleaner = CosmeticClean()
        cCleaner.set_target(RB.target.get_data(0))
        cCleaner.set_bias(RB.bias.master)
        cCleaner.set_flat(RB.flat.master)
        cCleaner.clean()
        cCleaner.write_to_file(
            xml_a.path_sci + RB.target.get_write_name(".fits")
        )
        RB.target.data_sci_clean = cCleaner.target

        # Do again for arcs:
        for i, arc in enumerate(RB.arc):
            cCleaner = CosmeticClean()
            cCleaner.set_target(RB.arc[i].get_data(0))
            cCleaner.set_bias(RB.bias.master)
            cCleaner.set_flat(RB.flat.master)
            cCleaner.clean()
            cCleaner.write_to_file(
                xml_a.path_cal + RB.arc[i].get_write_name(".fits")
            )
            RB.arc[i].data_sci_clean = cCleaner.target


def read_images_clean(target=True, arc=True, warped=False):

    if warped:
        extension = ".warped.fits"
        errortext = " Maybe you forgot to process warping?"
    else:
        extension = ".fits"
        errortext = " Maybe you forgot to process cosmetic?"

    for RB in ReductionBlockSet.instances:
        try:
            if target:
                HDU = fits.open(xml_a.path_sci + RB.target.get_write_name(extension))
                RB.target.data_sci_clean = [CCD.data for CCD in HDU[1:]]
                HDU.close()
            if arc:
                for i, arc in enumerate(RB.arc):
                    HDU = fits.open(xml_a.path_cal + RB.arc[i].get_write_name(extension))
                    RB.arc[i].data_sci_clean = [CCD.data for CCD in HDU[1:]]
                    HDU.close()
        except IOError as err:
            if not err.args:
                err.args=('',)
            err.args = ((str(err.args[0]) + errortext,) + err.args[1:])
            raise err


def do_findtrace():
    ReductionBlockSet.instances[0].target.get_data()
        # Needs to be called at least once.
        #TODO That is a silly, unelegant way to do it!
    for RB in ReductionBlockSet.instances:
        lFinder = SpectrumLineFind(RB.target.filenames[0], axis_spec=xml_i.axis_spec)
        lFinder.set_path_xml(path_i)
        lFinder.xml_i = xml_i
        lFinder.target = RB.target.data_sci_clean
        lFinder.set_shape()
        lFinder.read_xml()
        lFinder.find_position(mode=xml_a.mode_findtrace)
        RB.target.position_spec = lFinder.position_spec
        RB.CCD_nos_to_keep = lFinder.CCD_index_to_keep
        outfile = open(
            xml_a.path_sci + RB.target.get_write_name(".position.txt"),
            "w",
        )
        outfile.write(str(lFinder.CCD_index_to_keep) + "\n")
        outfile.write(str(lFinder.position_spec))
        outfile.close()


def read_findtrace():
    for RB in ReductionBlockSet.instances:
        try:
            infile = open(
                xml_a.path_sci + RB.target.get_write_name(".position.txt"),
                "r",
            )
            RB.CCD_nos_to_keep = [
                int(CCDno) for CCDno in
                infile.readline().strip().lstrip("[").rstrip("]").split(",")
            ]
            RB.target.position_spec = float(infile.readline())
            infile.close()
        except IOError as err:
            print "Maybe you forgot to process find trace?"
            raise err


def do_warp(cosmicray=False):
    for RB in ReductionBlockSet.instances:

        warper = Warp(axis_spec=xml_i.axis_spec)
        path = xml_a.path_cal + RB.target.get_write_name(".arcpos.txt")

        try:
            print "Trying to read positions from file..."
            warper.read_positions(path)
            print "... file reading complete."
        except IOError:
            print "... could not find file with arcpositions."
            warper.set_position_spec(RB.target.position_spec)
            for i, RB_arc in enumerate(RB.arc):
                print "Finding lines in an arc..."
                warper.set_arcimage(RB_arc.data_sci_clean[RB.CCD_nos_to_keep[0]])
            print "Tracing all the lines..."
            warper.trace_lines()
            print "Saving positions to file..."
            warper.save_positions(path)

        print "Computing the transformation..."
        warper.compute_transformation()

        print "Applying transformation on target..."
        img_warped = warper.apply_transformation(
            RB.target.data_sci_clean[RB.CCD_nos_to_keep[0]]
        )
        RB.target.data_sci_clean[RB.CCD_nos_to_keep[0]] = img_warped

        print "Applying transformation on weights..."
        RB.flat.master_warped = list(RB.flat.master)
            # list() is used to make a copy instead of a pointer.
        RB.flat.master_warped[RB.CCD_nos_to_keep[0]] = warper.apply_transformation(
            RB.flat.master[RB.CCD_nos_to_keep[0]]
        )

        for i, RB_arc in enumerate(RB.arc):
            print "Applying transformation on an arc..."
            RB.arc[i].data_sci_clean_warped = list(RB.arc[i].data_sci_clean)
                # list() is used to make a copy instead of a pointer.
            RB.arc[i].data_sci_clean_warped[RB.CCD_nos_to_keep[0]] = warper.apply_transformation(
                RB_arc.data_sci_clean[RB.CCD_nos_to_keep[0]]
            )

        print "Applying transformation on mask_badpixel..."
        RB.mask_badpixel_warped = list(RB.mask_badpixel)
            # list() is used to make a copy instead of a pointer.
        RB.mask_badpixel_warped[RB.CCD_nos_to_keep[0]] = warper.apply_transformation_binary(
            RB.mask_badpixel[RB.CCD_nos_to_keep[0]].astype(float)
        )
        # RB.mask_badpixel[RB.CCD_nos_to_keep[0]] = \
            # (RB.mask_badpixel[RB.CCD_nos_to_keep[0]] >= 1).astype(int)

        if cosmicray:
            print "Applying transformation on mask_cosmicray..."
            RB.mask_cosmicray_warped = list(RB.mask_cosmicray)
                # list() is used to make a copy instead of a pointer.
            RB.mask_cosmicray_warped[RB.CCD_nos_to_keep[0]] = warper.apply_transformation_binary(
                RB.mask_cosmicray[RB.CCD_nos_to_keep[0]].astype(float)
            )
            # RB.mask_cosmicray[RB.CCD_nos_to_keep[0]] = \
                # (RB.mask_cosmicray[RB.CCD_nos_to_keep[0]] >= 1).astype(int)

        # Save warped image:
        #TODO It is done in a strange way with a different module... Also,
        # all CCDs are saved again, but only the necessary CCD is changed, so
        # obsolete data is produced.

        print "Saving the warped target..."
        saver = CosmeticClean()
        saver.set_target(RB.target.data_sci_clean)
        saver.write_to_file(
            xml_a.path_sci + RB.target.get_write_name(".warped.fits")
        )
        print "Saving the warped weights..."
        saver = CosmeticClean()
        saver.set_target(RB.flat.master_warped)
        saver.write_to_file(
            xml_a.path_cal + RB.target.get_write_name(".weight.warped.fits")
        )
        print "Saving the warped arcs..."
        for i, RB_arc in enumerate(RB.arc):
            saver = CosmeticClean()
            saver.set_target(RB_arc.data_sci_clean_warped)
            saver.write_to_file(
                xml_a.path_cal + RB_arc.get_write_name(".warped.fits")
            )
        print "Saving the warped mask_badpixel..."
        saver = CosmeticClean()
        saver.set_target([CCD.astype(int) for CCD in RB.mask_badpixel_warped])
        saver.write_to_file(
            xml_a.path_cal + RB.target.get_write_name(".badpix.warped.fits")
        )
        if cosmicray:
            print "Saving the warped mask_cosmicray..."
            saver = CosmeticClean()
            saver.set_target([CCD.astype(int) for CCD in RB.mask_cosmicray])
            saver.write_to_file(
                xml_a.path_cal + RB.target.get_write_name(".cosray.warped.fits")
            )

    for RB in ReductionBlockSet.instances:
        RB.flat.master[RB.CCD_nos_to_keep[0]] = \
            RB.flat.master_warped[RB.CCD_nos_to_keep[0]]
        for i, RB_arc in enumerate(RB.arc):
            RB.arc[i].data_sci_clean[RB.CCD_nos_to_keep[0]] = \
                RB.arc[i].data_sci_clean_warped[RB.CCD_nos_to_keep[0]]
        RB.mask_badpixel[RB.CCD_nos_to_keep[0]] = \
            RB.mask_badpixel_warped[RB.CCD_nos_to_keep[0]]
        if cosmicray:
            RB.mask_cosmicray[RB.CCD_nos_to_keep[0]] = \
                RB.mask_cosmicray_warped[RB.CCD_nos_to_keep[0]]


def do_reduce_spec():
    method = xml_a.mode_reduce_spec
    for RB in ReductionBlockSet.instances:
        print "Extracting spectrum from '%s'." % RB.target.filenames[0]

        for CCDno in RB.CCD_nos_to_keep:
            reducer = Reduction(RB.target.filenames[0], axis_spec=xml_i.axis_spec)
            reducer.set_image(RB.target.data_sci_clean[CCDno])
            weights = RB.flat.master[CCDno] * RB.mask_badpixel[CCDno]
            if xml_a.do_cosmicray_clean is not None:
                weights *= RB.mask_cosmicray[CCDno]
            reducer.set_weights(weights)
            reducer.set_aperture_radius(
                xml_i.aperture_radius,
                xml_i.aperture_unit,
            )
            reducer.set_sky_radius(
                (xml_i.sky_inner_radius, xml_i.sky_outer_radius),
                (xml_i.sky_inner_unit, xml_i.sky_outer_unit),
            )
            reducer.locate_spectrum(RB.target.position_spec, method=method)
            reducer.write_to_file(
                xml_a.path_sci + RB.target.get_write_name(".flux_adu.txt"),
                method=method,
            )
            RB.target.flux = reducer.get_spectrum(method=method)
                #TODO Assumes just one spectrum for now.


def do_cal_wave(auto_find_gas=False):

    interactive = xml_a.interactive_cal_wave
    for RB in ReductionBlockSet.instances:
        wCalibrator = WavelengthCalibrate(interactive=interactive, axis_spec=xml_i.axis_spec)
        wCalibrator.instrument_name = xml_i.instrument_name

        if auto_find_gas:
            for RB_arc in RB.arc:
                arcimage = RB_arc.data_sci_clean[RB.CCD_nos_to_keep[0]]
                wCalibrator.set_data_raw(arcimage, RB.target.position_spec)
                wCalibrator.set_data_calibration(grism=RB_arc.grism)
            for RB_arc in RB.arc:
                arcimage = RB_arc.data_sci_clean[RB.CCD_nos_to_keep[0]]
                wCalibrator.set_data_raw(arcimage, RB.target.position_spec)
            wCalibrator.pair_gases(grism=RB_arc.grism)

        else:
            gases = []
            for RB_arc in RB.arc:
                arcimage = RB_arc.data_sci_clean[RB.CCD_nos_to_keep[0]]
                wCalibrator.set_data_raw(arcimage, RB.target.position_spec)
                grism = RB_arc.grism  # Assumes the grism is the same.
                for gas in RB_arc.gas:
                    if gas not in gases:
                        gases.append(gas)
            for gas in gases:
                wCalibrator.set_data_calibration(grism=grism, gas=gas)
            if not gases:
                raise ValueError(
                    "Attempted to wavelength calibrate with arc with no "
                    "affiliated gases."
                )

        wCalibrator.start_calibration()
        wCalibrator.write_to_file(
            xml_a.path_sci + RB.target.get_write_name(".wave.txt"),
            flux = RB.target.flux,
        )
        RB.target.wave = wCalibrator.get_wavelengths()


def read_spec(wave=True, flux=True, fluxcal=False):
    """
    wave: (bool) Read wavelengths.
    flux: (bool) Read fluxes.
    fluxcal: (bool) Read the already fluxcalibrated fluxes (only if flux=True).
    """
    for RB in ReductionBlockSet.instances:
        if flux:
            if fluxcal:
                extension = ".flux.txt"
            else:
                extension = ".flux_adu.txt"
            infile = open(
                xml_a.path_sci + RB.target.get_write_name(extension),
                "r",
            )
            RB.target.flux = np.array([float(line.split()[~0]) for line in infile])
            infile.close()
        if wave:
            infile = open(
                xml_a.path_sci + RB.target.get_write_name(".wave.txt"),
                "r",
            )
            RB.target.wave = np.array([float(line.split()[1]) for line in infile])
            infile.close()


def do_cal_flux(show=True):
    for RB in ReductionBlockSet.instances_target:
        fCalibrator = FluxCalibrate(RB.target.filenames[0], RB.std.target.filenames[0])
        fCalibrator.set_data_raw(
            RB.std.target.wave,
            RB.std.target.flux * (
                RB.target.gain[RB.CCD_nos_to_keep[0]] /
                RB.std.target.gain[RB.std.CCD_nos_to_keep[0]]
            ),
        )
        infile = open(
            os.path.dirname(sys.argv[0]) + "/../data/stds/%s.txt"
            % (RB.std.target.star),
            "r",
        )
        xy_cal = np.array([
            [float(value) for value in line.split()] for
            line in infile.readlines()
        ]).transpose()
        infile.close()
        fCalibrator.set_data_calibration(xy_cal)
        p_opt, error = fCalibrator.get_calibrate()
        calibpoly = func.polynomial(RB.target.wave, p_opt)
        RB.target.flux /= calibpoly
        if show:
            flux_old = RB.target.flux / calibpoly
            flux_old *= np.max(RB.target.flux) / np.max(flux_old)
            plt.plot(RB.target.wave, flux_old, "b",
                label="before fluxcal (scaled to fit image)")
            plt.plot(RB.target.wave, RB.target.flux, "r", label="after fluxcal")
            plt.title(
                "Target: '%s'\nSTD: '%s'"
                % (fCalibrator.filename_target, fCalibrator.filename_std)
            )
            plt.legend()
            plt.xlabel("[aa]")
            plt.ylabel("[Jy]")
            plt.show()

        path = xml_a.path_sci + RB.target.get_write_name(".flux.txt")
        outfile = open(path, "w")
        for i, x, y in zip(
            xrange(len(RB.target.flux)),
            RB.target.wave,
            RB.target.flux,
        ):
            outfile.write("%d %s %s\n" % (i, repr(x), repr(y)))
        outfile.close()


if __name__ == "__main__":

    # Attempt to read input file and tell the user if the input is wrong:
    try:
        path_a = sys.argv[1]
    except IndexError:
        print (
            "usage: python %s submit_file.xml\n"
            "  Specify the (path)name of a submit xml file. "
            % sys.argv[0]
        )
        sys.exit(1)
    try:
        xml_a = XMLA(path_a)
    except IOError:
        print "No such file: '%s'" % sys.argv[1]
        print (
            "usage: python %s submit_file.xml\n"
            "  Specify the (path)name of a submit xml file. "
            % sys.argv[0]
        )
        if sys.argv[1] in (
            "help", "Help", "-h", "-H", "--help", "man", "manual"
        ):
            print (
                "  See the usermanual for details on how to use spextractor:\n"
                "  TODO: Link to usermanual should be here."
            )
        sys.exit(1)
    except Exception:
        print (
            "usage: python %s submit_file.xml\n"
            "  Specify the (path)name of a submit xml file. "
            % sys.argv[0]
        )
        raise

    path_i = "./spextractor-%s.xml" % xml_a.instrument_name
    xml_i = XMLI(path_i)
    # func.make_folder(xml_a.path_log)

    # ReductionBlock.area_sci = xml_i.area_sci
    # ReductionBlock.area_osc = xml_i.area_osc
    ReductionBlock.flat_min = xml_i.flat_min
    ReductionBlock.flat_max = xml_i.flat_max
    ReductionBlock.xml_i = xml_i

    if xml_a.make_RB_cal or xml_a.make_RB_sci:
        func.make_folder(xml_a.path_rbs)
        RBs = make_RB()
        group_RBs(RBs)
    else:
        RBs = read_RB()
        group_RBs(RBs)

    if xml_a.do_cosmicray_clean:
        func.make_folder(xml_a.path_cal)
        do_cosmicray_clean()

    if xml_a.make_cosmetic:
        func.make_folder(xml_a.path_cal)
        cMakers = make_cosmetic()


    if xml_a.do_cal_cosmetic:
        if not xml_a.make_cosmetic:
            read_cosmetic()
        # func.make_folder(xml_a.path_std)
        func.make_folder(xml_a.path_sci)
        do_cal_cosmetic()


    if xml_a.do_findtrace:
        if not xml_a.do_cal_cosmetic:
            read_images_clean(target=True, arc=False)
        do_findtrace()


    if xml_a.do_warp:
        if not xml_a.do_findtrace:
            read_findtrace()
        if not xml_a.make_cosmetic and not xml_a.do_cal_cosmetic:
            read_cosmetic(flat=True, bias=False)
        if not xml_a.make_cosmetic:
            read_badpixel_mask()
        if (not xml_a.do_cosmicray_clean) and \
            (xml_a.do_cosmicray_clean is not None):
            read_cosmicray_mask()
        if not xml_a.do_cal_cosmetic:
            read_images_clean()
        do_warp(cosmicray=(xml_a.do_cosmicray_clean is not None))
            # Warp mask_cosmicray only if it is not excluded (None).

    if xml_a.do_reduce_spec:
        if not xml_a.do_findtrace:
            read_findtrace()
        if not xml_a.do_warp and (xml_a.do_warp is not None):
            read_weights(warped=True)
            read_images_clean(target=True, arc=False, warped=True)
            read_badpixel_mask(warped=True)
            if xml_a.do_cosmicray_clean is not None:
                read_cosmicray_mask(warped=True)
        else:  # xml_a.do_warp is None ('exclude')
            if not xml_a.make_cosmetic:
                read_weights(warped=False)
            if not xml_a.do_cal_cosmetic:
                read_images_clean(target=True, arc=False)
                read_badpixel_mask()
            if not xml_a.do_cosmicray_clean:
                if xml_a.do_cosmicray_clean is not None:
                    read_cosmicray_mask()
        do_reduce_spec()

    if xml_a.do_cal_wave:
        if not xml_a.do_findtrace:
            read_findtrace()
        if not xml_a.do_reduce_spec:
            read_spec(wave=False, flux=True)
        if not xml_a.do_warp and (xml_a.do_warp is not None):
            read_images_clean(target=False, arc=True, warped=True)
        else:  # xml_a.do_warp is None ('exclude')
            if not xml_a.do_cal_cosmetic:
                read_images_clean(target=False, arc=True)
        do_cal_wave()

    if xml_a.do_cal_flux:
        if not xml_a.do_findtrace:
            # Need this to choose correct CCD, which is important since
            # CCDs may have different gains.
            read_findtrace()
        if not xml_a.do_reduce_spec:
            #TODO Assumes there is only one spectrum per target, i.e. that only
            # one CCD is used.
            read_spec(wave=False, flux=True)
        if not xml_a.do_cal_wave:
            read_spec(wave=True, flux=False)
        do_cal_flux()
