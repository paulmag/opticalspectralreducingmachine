class CCD:

    def __init__(self, id_=None):
        self.id_ = id_
        self.index = None  # Python list index, based on the HDU.
        self.extname = None
        self.gain = None
        self.saturation = None
        self.readoutnoise = None
        self.area_sci = None
        self.area_osc = None
        self.coords = None, None
