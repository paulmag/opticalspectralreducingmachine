from Reduction import Reduction
from astropy.io import fits
#import matplotlib.pyplot as plt

infile = fits.open("../../practice/GTC39-11B/OB0010/std_clean_tmean/03")
infile.info()
#data = infile[0].data[:, 150:450]
data = infile[0].data[:, 260:300]

reducer = Reduction()
reducer.set_image(data)
reducer.locate_spectrum()
reducer.show_fit( 500)
reducer.show_fit(1000)
reducer.show_fit(1500)

#fits.PrimaryHDU(reducer.image_stacked).writeto(
#    "../../practice/GTC39-11B/OB0010/image_std03_stacked.fits")
