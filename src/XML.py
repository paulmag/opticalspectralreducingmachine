import os

import xmltodict

from CCD import CCD


class XML:
    """Represents a generic XML-file."""

    def __init__(self, path):
        infile = open(path, "r")
        self.tree = xmltodict.parse(infile)
        infile.close()
        self.read()

    def read(self):
        """To be overwritten by subclasses.
        Is intented to be called once and will extract all neccessary
        variables from the XML-file.
        """
        pass

    positive = "yes", "true"
    negative = "no", "false"
    exclude  = "exclude", "never"

    @classmethod
    def extract_booleans(self, d):
        """Description."""
        for key, value in d.iteritems():
            if isinstance(value, dict):
                self.extract_booleans(value)
            else:
                if value in self.positive:
                    d[key] = True
                elif value in self.negative:
                    d[key] = False
                elif value in self.exclude:
                    d[key] = None
        return d


class XMLActions(XML):
    """Represents the users input file which the user uses to tell the
    program what he wants to do (which calibrations to perform).
    """

    def read(self):

        actions = self.tree["spextractor"]["action"]
        paths = self.tree["spextractor"]["directory-structure"]
        self.instrument_name = actions["instrument-name"]

        actions = self.extract_booleans(actions)
        self.make_RB_cal = actions["create-instrumental-calibration-block"]["active"]
        self.make_RB_sci = actions["create-scientific-reduction-block"]["active"]
        self.do_cosmicray_clean = actions["process-cosmic-rays"]["active"]
        self.make_cosmetic = actions["process-instrumental-calibration"]["active"]
        self.do_cal_cosmetic = actions["process-cosmetic"]["active"]
        self.do_reduce_spec = actions["process-extract"]["active"]
        self.mode_reduce_spec = actions["process-extract"]["reduction-mode"]
        self.do_findtrace = actions["process-find-trace"]["active"]
        self.mode_findtrace = actions["process-find-trace"]["mode"]
        self.do_warp = actions["process-warp"]["active"]
        self.do_cal_wave = actions["process-wavecal"]["active"]
        self.interactive_cal_wave = actions["process-wavecal"]["interactive"]
        self.do_cal_flux = actions["process-fluxcal"]["active"]

        self.path_raw = os.path.expanduser(paths["raw-images"])
        self.path_cal = os.path.expanduser(paths["reduced-calib"])
        self.path_std = os.path.expanduser(paths["reduced-std"])
        self.path_sci = os.path.expanduser(paths["reduced-science"])
        self.path_rbs = os.path.expanduser(paths["reduction-block"])
        self.path_log = os.path.expanduser(paths["logbook"])


class XMLInstrument(XML):
    """Represents the XML-file with information about the instrument used,
    like what does it call the keywords in the FITS-headler, how is the
    CCD-layout.
    """

    def read(self):
        self.root = self.tree["spextractor"]

        self.N = (
            int(self.root["instrument"]["chip-geometry"]["nb-chip-y"]),
            int(self.root["instrument"]["chip-geometry"]["nb-chip-x"]),
            # NB: y-coordinate first: (y,x)
        )

        self.find_chips()
        self.find_quadrants()
        self.find_RB_associations()
        self.find_spec_extraction_rules()
        self.find_cosmicray_rules()
        self.find_instrumental_calibration_reduction()

        self.instrument_name = self.root["instrument"]["name"]
        self.map_arc = self.root["instrument"]["object-name-mapping"]["arc"]
        if self.root["instrument"]["default-fov"]["dispaxis"] in ("x", "X"):
            # slit is in x-direction => spectrum is in y-direction
            self.axis_spec = 0
        elif self.root["instrument"]["default-fov"]["dispaxis"] in ("y", "Y"):
            # slit is in y-direction => spectrum is in x-direction
            self.axis_spec = 1
        else:
            raise ValueError(
                "<dispaxis> must be 'x' or 'y'. "
                "Which way is the slit oriented?"
            )


    def find_chips(self):
        """ Note that xmltodict produces an OrderedDict, so when f.ex. the
        tag <chip> appears several times since there are more than
        one chip each of them can be accessed by list indexing ([0], [1],
        etc.), so the CCD-chips can be indexed in the same order as they are
        listed in the XML file.
        """

        geo_chip = self.root["instrument"]["chip-geometry"]["chip"]
        self.chips = {}  # dict with CCD instances. The ID is used as key.
        self.chips_list = []
            # list with the same CCD instances. It is filled in ReductionBlock.

        if self.N[0] * self.N[1] == 1: # just one CCD
            id_ = int(geo_chip["id"])
            self.chips[id_] = CCD(id_)  # Make new chip with this ID.
            self.chips[id_].extname = geo_chip["extname"]
            self.chips[id_].gain = float(geo_chip["gain"])
            self.chips[id_].saturation = int(geo_chip["saturation"])
            self.chips[id_].readoutnoise = float(geo_chip["read-out-noise"])
            self.chips[id_].coords = int(geo_chip["chip-y"]), int(geo_chip["chip-x"])
        else: # several CCDs
            for j in range(self.N[0] * self.N[1]): # for each CCD-chip
                id_ = int(geo_chip[j]["id"])
                self.chips[id_] = CCD(id_)  # Make new chips with this ID.
                self.chips[id_].extname = geo_chip[j]["extname"]
                self.chips[id_].gain = float(geo_chip[j]["gain"])
                self.chips[id_].saturation = int(geo_chip[j]["saturation"])
                self.chips[id_].readoutnoise = float(geo_chip[j]["read-out-noise"])
                self.chips[id_].coords = int(geo_chip[j]["chip-y"]), int(geo_chip[j]["chip-x"])

        self.key_extname = self.root["instrument"]["keyword-fits-raw"]["chip-id"]["name"]
        chip_id_to_use = self.root["instrument"]["chip-geometry"]["process-only-chip-id"]
        chip_extname_to_use = []
        if chip_id_to_use == "." or "all" in chip_id_to_use:
            chip_id_to_use = self.chips.keys()
        else:
            chip_id_to_use = [int(id_) for id_ in chip_id_to_use.split("|")]
        for id_, chip in self.chips.iteritems():
            if id_ in chip_id_to_use:
                chip_extname_to_use.append(chip.extname)

        self.chip_id_to_use = chip_id_to_use
        self.chip_extname_to_use = chip_extname_to_use


    def find_quadrants(self):
        """ Note that xmltodict produces an OrderedDict, so when f.ex. the
        tag <quadrant-chip> appears several times since there are more than
        one chip each of them can be accessed by list indexing ([0], [1],
        etc.), so the CCD-chips can be indexed in the same order as they are
        listed in the XML file.
        find_chips must be called first!
        """

        geo_quad = self.root["instrument"]["quadrant-geometry"]["quadrant-chip"]

        if self.N[0] * self.N[1] == 1: # just one CCD
            id_ = int(geo_quad["pattern-chip-id"])
            self.chips[id_].area_sci = [
                int(geo_quad["quadrant-science"]["quadrant"]["y-init"]),
                int(geo_quad["quadrant-science"]["quadrant"]["y-length"]),
                int(geo_quad["quadrant-science"]["quadrant"]["x-init"]),
                int(geo_quad["quadrant-science"]["quadrant"]["x-length"]),
            ]
            self.chips[id_].area_osc = [
                int(geo_quad["quadrant-overscan"]["quadrant"]["y-init"]),
                int(geo_quad["quadrant-overscan"]["quadrant"]["y-length"]),
                int(geo_quad["quadrant-overscan"]["quadrant"]["x-init"]),
                int(geo_quad["quadrant-overscan"]["quadrant"]["x-length"]),
            ]
        else: # several CCDs
            for j in range(self.N[0] * self.N[1]): # for each CCD-chip
                id_ = int(geo_quad[j]["pattern-chip-id"])
                self.chips[id_].area_sci = [
                    int(geo_quad[j]["quadrant-science"]["quadrant"]["y-init"]),
                    int(geo_quad[j]["quadrant-science"]["quadrant"]["y-length"]),
                    int(geo_quad[j]["quadrant-science"]["quadrant"]["x-init"]),
                    int(geo_quad[j]["quadrant-science"]["quadrant"]["x-length"]),
                ]
                self.chips[id_].area_osc = [
                    int(geo_quad[j]["quadrant-overscan"]["quadrant"]["y-init"]),
                    int(geo_quad[j]["quadrant-overscan"]["quadrant"]["y-length"]),
                    int(geo_quad[j]["quadrant-overscan"]["quadrant"]["x-init"]),
                    int(geo_quad[j]["quadrant-overscan"]["quadrant"]["x-length"]),
                ]


    def find_RB_associations(self):
        res_cal = self.root["instrument"]["reduction-block-associations"]["calib"]
        res_sci = self.root["instrument"]["reduction-block-associations"]["science"]
        self.jdate_limit_bias = float(res_cal["bias-max-diff-jday"])
        self.jdate_limit_dark = float(res_cal["dark-max-diff-jday"])
        self.jdate_limit_flat = float(res_cal["flat-max-diff-jday"])
        self.jdate_limit_arc  = float(res_cal["arc-max-diff-jday"])
        self.jdate_limit_std   = float(res_sci["sci-std-jday-max-diff"])
        self.airmass_limit_std = float(res_sci["sci-std-airmass-max-diff"])


    def find_spec_extraction_rules(self):
        self.aperture_radius  = float(self.root["spectrum-extraction"]["aperture-radius"])
        self.sky_inner_radius = float(self.root["spectrum-extraction"]["sky-inner-radius"])
        self.sky_outer_radius = float(self.root["spectrum-extraction"]["sky-outer-radius"])
        self.aperture_unit  = self.root["spectrum-extraction"]["aperture-radius-unit"]
        self.sky_inner_unit = self.root["spectrum-extraction"]["sky-inner-radius-unit"]
        self.sky_outer_unit = self.root["spectrum-extraction"]["sky-outer-radius-unit"]


    def find_cosmicray_rules(self):
        self.cray_sigclip = float(self.root["cosmic-ray-detection"]["sigclip"])
        self.cray_sigfrac = float(self.root["cosmic-ray-detection"]["sigfrac"])
        self.cray_objlim  = float(self.root["cosmic-ray-detection"]["objlim"])
        self.cray_niter = int(self.root["cosmic-ray-detection"]["niter"])


    def find_instrumental_calibration_reduction(self):
        stack_opt = self.root["stack-instrumental-calibration-option"]

        self.bias_method = stack_opt["bias"]["stack"]["method"]
        self.bias_sigclip_check = stack_opt["bias"]["stack"]["sigma-clipping"]
        self.bias_sigclip_thresh = float(stack_opt["bias"]["stack"]["sigma-clipping-threshold"])
        self.bias_sigclip_iter = int(stack_opt["bias"]["stack"]["sigma-clipping-iter"])

        self.dark_method = stack_opt["dark"]["stack"]["method"]
        self.dark_sigclip_check = stack_opt["dark"]["stack"]["sigma-clipping"]
        self.dark_sigclip_thresh = float(stack_opt["dark"]["stack"]["sigma-clipping-threshold"])
        self.dark_sigclip_iter = int(stack_opt["dark"]["stack"]["sigma-clipping-iter"])

        self.flat_method = stack_opt["flat"]["stack"]["method"]
        self.flat_min = int(stack_opt["flat"]["stack"]["flat-min-value"])
        self.flat_max = int(stack_opt["flat"]["stack"]["flat-max-value"])
        self.flat_linearity_check = stack_opt["flat"]["stack"]["check-non-linearity"]
        self.flat_linearity_lim = float(stack_opt["flat"]["stack"]["max-frac-non-linear"])
        self.flat_sigma_check = stack_opt["flat"]["stack"]["reject-high-stddev"]
        self.flat_sigma_lim = float(stack_opt["flat"]["stack"]["stddev-max-diff"])

        self.badpixel_treshold = float(
            stack_opt["bad-pixel"]["bad-pixel-threshold"]
        )
        self.badpixel_dilateradius = float(
            stack_opt["bad-pixel"]["bad-pixel-dilate-radius"]
        )
