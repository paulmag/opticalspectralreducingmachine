<?xml version="1.0"?>
<spextractor xmlns:xinc="http://www.w3.org/2001/XInclude">
    <!-- ============================================================ -->
    <!-- ============================================================ -->
    <!-- GTC-OSIRIS Instrument -->
    <!-- 2x2 binning -->
    <!-- ============================================================ -->
    <!-- ============================================================ -->

    <!-- ============================================================ -->
    <!-- INSTRUMENT PROPERTIES -->
    <!-- ============================================================ -->
    <instrument>

        <!-- ============================================================ -->
        <!-- Name given to recognize an instrument wih a specific mode -->
        <!-- ============================================================ -->
        <name>gtc-osiris</name>

        <!-- ============================================================ -->
        <!-- Location of the telescope -->
        <!-- ============================================================ -->
        <telescope>
            <name>gtc</name>
            <xinc:include parse="xml" xpointer="xpointer(/spextractor/defs/def[@name='earth-location-gtc']/*)"/>
        </telescope>

        <!-- ============================================================ -->
        <!-- Default Field of View (pixel size and orientation) -->
        <!-- ============================================================ -->
        <default-fov>
            <pixscale>0.000066111</pixscale>   <!-- pixel size in decimal degrees -->
            <dispaxis>x</dispaxis>            <!-- dispersion axis -->
        </default-fov>

        <!-- ============================================================ -->
        <!-- Raw Keyword mapping -->
        <!-- ============================================================ -->
        <keyword-fits-raw>
            <delete>
                <name>void</name>
            </delete>
            <filter1>
                <name>FILTER1</name>
            </filter1>
            <filter2>
                <name>FILTER2</name>
            </filter2>
            <object-name>
                <name>OBJECT</name>
            </object-name>
            <exposure-time>
                <name>EXPTIME</name>
                <unit>sec</unit>
            </exposure-time>
            <date-obs>
                <name>MJD-OBS</name> <!-- possibilities: MJD-OBS, DATE-OBS -->
                <type>mjd</type> <!-- choice of isot, jd, mjd -->
           </date-obs>
            <grism>
                <name>GRISM</name>
                <type>name</type> <!-- choice of name, fixed -->
            </grism>
            <mask>
                <name>maskname</name>
            </mask>
            <gain>
                <name>ch</name>
                <type>fixed</type> <!-- choice of name, exact, fixed -->
            </gain>
            <airmass>
                <name>AIRMASS</name>
            </airmass>
            <chip-id>
                <name>EXTNAME</name>
            </chip-id>
            <right-ascension>
                <name>RADEG</name>
                <unit>deg</unit>  <!-- deg or sexagesimal -->
            </right-ascension>
            <declination>
                <name>DECDEG</name>
                <unit>deg</unit>  <!-- deg or sexagesimal -->
            </declination>
            <equinox>
                <name>EQUINOX</name>
                <unit>year</unit>  <!-- year -->
            </equinox>
        </keyword-fits-raw>

        <!-- ============================================================ -->
        <!-- Reduction block associations -->
        <!-- ============================================================ -->

        <reduction-block-associations>
            <calib>
                <flat-min-good>3</flat-min-good>
                <flat-max-good>10</flat-max-good>
                <flat-max-diff-jday>15.0</flat-max-diff-jday>
                <dark-min-good>5</dark-min-good>
                <dark-max-good>10</dark-max-good>
                <dark-max-diff-jday>3.0</dark-max-diff-jday>
                <bias-min-good>3</bias-min-good>
                <bias-max-good>10</bias-max-good>
                <bias-max-diff-jday>15.0</bias-max-diff-jday>
                <arc-max-diff-jday>1.0</arc-max-diff-jday>
            </calib>
            <science>
                <sci-std-airmass-max-diff>0.3</sci-std-airmass-max-diff>
                <sci-std-jday-max-diff>1.</sci-std-jday-max-diff>
            </science>
        </reduction-block-associations>

        <!-- ============================================================ -->
        <!-- Structure of the raw files (multiple-file or multi-extension-file -->
        <!-- ============================================================ -->
        <raw-file-description>
            <exposure-time-precision>0.1</exposure-time-precision>
            <relative-index-time>0.0</relative-index-time>
            <raw-fits-type>multi-hdu</raw-fits-type> <!-- multi-file  or multi-hdu -->
            <nb-hdu>3</nb-hdu>
        </raw-file-description>

        <!-- ============================================================ -->
        <!-- Object-Name mapping for the instrumental calibration -->
        <!-- ============================================================ -->
        <object-name-mapping>
            <flat>SpectralFlat</flat>
            <bias>bias</bias>
            <dark>dark</dark>
            <arc>ArcLamp</arc>
            <photstd>SPSTD|SpectroPhotSTD</photstd>
            <reject>test|focus|beta-light</reject>
        </object-name-mapping>

        <!-- ============================================================ -->
        <!-- Grism name mapping to rename the raw grism values -->
        <!-- ============================================================ -->
        <grism-name-mapping>
             <R300B>R300B</R300B>
             <R300R>R300R</R300R>
             <R500B>R500B</R500B>
             <R500R>R500R</R500R>
             <R1000B>R1000B</R1000B>
             <R1000R>R1000R</R1000R>
             <R2000B>R2000B</R2000B>
             <R2500U>R2500U</R2500U>
             <R2500V>R2500V</R2500V>
             <R2500R>R2500R</R2500R>
             <R2500I>R2500I</R2500I>
        </grism-name-mapping>

        <!-- ============================================================ -->
        <!-- Gain name mapping to rename the raw gain values -->
        <!-- Ignored if gain type was set to exact or fixed -->
        <!-- ============================================================ -->

        <gain-name-mapping>
            <SPEED100>1.18</SPEED100>
            <SPEED200>0.95</SPEED200>
        </gain-name-mapping>

        <!-- ============================================================ -->
        <!-- Layout of the chips -->
        <!-- ============================================================ -->
        <chip-geometry>
            <nb-chip-x>2</nb-chip-x>
            <nb-chip-y>1</nb-chip-y>
            <chip>
                <id>1</id>
                <extname>CCD_1_L1_1</extname>
                <chip-x>1</chip-x>
                <chip-y>1</chip-y>
                <naxis1>1049</naxis1>
                <naxis2>2051</naxis2>
                <gain>0.95</gain>         <!-- used only if gain type was set to fixed -->
                <saturation>63000</saturation>
                <read-out-noise>3.5</read-out-noise>
            </chip>
            <chip>
                <id>2</id>
                <extname>CCD_2_L2_1</extname>
                <chip-x>2</chip-x>
                <chip-y>1</chip-y>
                <naxis1>1049</naxis1>
                <naxis2>2051</naxis2>
                <gain>0.95</gain>       <!-- used only if gain type was set to fixed -->
                <saturation>63000</saturation>
                <read-out-noise>3.5</read-out-noise>
            </chip>
            <process-only-chip-id>.</process-only-chip-id>
        </chip-geometry>

        <!-- ============================================================ -->
        <!-- Layout of the electronic quadrants in each chip -->
        <!-- ============================================================ -->
        <quadrant-geometry>

            <!-- Definition for chip 1 -->
            <!-- ============================================================ -->
            <quadrant-chip>
                <pattern-chip-id>1</pattern-chip-id>
                <quadrant-overscan>
                    <nb-quadrant>1</nb-quadrant>
                    <quadrant>
                        <pattern-quadrant-id>1</pattern-quadrant-id>
                        <x-init>3</x-init>
                        <y-init>5</y-init>
                        <x-length>17</x-length>
                        <y-length>2040</y-length>
                    </quadrant>
                </quadrant-overscan>
                <quadrant-science>
                    <nb-quadrant>1</nb-quadrant>
                    <quadrant>
                        <pattern-quadrant-id>1</pattern-quadrant-id>
                        <x-init>330</x-init>
                        <y-init>5</y-init>
                        <x-length>715</x-length>
                        <y-length>2040</y-length>
                    </quadrant>
                </quadrant-science>
            </quadrant-chip>

            <!-- Definition for chip 2 -->
            <!-- ============================================================ -->
            <quadrant-chip>
                <pattern-chip-id>2</pattern-chip-id>
                <quadrant-overscan>
                    <nb-quadrant>1</nb-quadrant>
                    <quadrant>
                        <pattern-quadrant-id>1</pattern-quadrant-id>
                        <x-init>3</x-init>
                        <y-init>5</y-init>
                        <x-length>18</x-length>
                        <y-length>2040</y-length>
                    </quadrant>
                </quadrant-overscan>
                <quadrant-science>
                    <nb-quadrant>1</nb-quadrant>
                    <quadrant>
                        <pattern-quadrant-id>1</pattern-quadrant-id>
                        <x-init>27</x-init>
                        <y-init>5</y-init>
                        <x-length>936</x-length>
                        <y-length>2040</y-length>
                    </quadrant>
                </quadrant-science>
            </quadrant-chip>
        </quadrant-geometry>
    </instrument>

    <!-- ============================================================ -->
    <!-- INSTRUMENTAL CALIBRATION REDUCTION -->
    <!-- ============================================================ -->
    <stack-instrumental-calibration-option>

        <!-- Option for bias stacking -->
        <!-- ============================================================ -->
        <bias>
            <stack>
                <method>mean</method>  <!-- any of median, mean, mode -->
                <sigma-clipping>yes</sigma-clipping>  <!-- apply sigma-clipping to reject outliers? -->
                <sigma-clipping-threshold>3.0</sigma-clipping-threshold> <!-- sigma-clipping threshold, in sigmas -->
                <sigma-clipping-iter>3</sigma-clipping-iter> <!-- sigma-clipping iterations -->
            </stack>
        </bias>

        <!-- Option for dark stacking -->
        <!-- ============================================================ -->
        <dark>
            <stack>
                <method>mean</method>  <!-- any of median, mean, mode -->
                <sigma-clipping>yes</sigma-clipping>  <!-- apply sigma-clipping to reject outliers? -->
                <sigma-clipping-threshold>3.0</sigma-clipping-threshold> <!-- sigma-clipping threshold, in sigmas -->
                <sigma-clipping-iter>3</sigma-clipping-iter> <!-- sigma-clipping iterations -->
            </stack>
        </dark>

        <!-- Option for flat field stacking -->
        <!-- ============================================================ -->
        <flat>
            <stack>
                <method>mean</method>  <!-- any of median, mean, mode -->
                <flat-min-value>8000</flat-min-value> <!-- min. value for a flat to be included -->
                <flat-max-value>58000</flat-max-value> <!-- min. value for a flat to be included -->
                <check-non-linearity>yes</check-non-linearity>  <!-- flag saturated pixels, i.e pixels above <flat-max-value>? -->
                <max-frac-non-linear>0.1</max-frac-non-linear> <!--maximum fraction of saturated pixels? -->
                <reject-high-stddev>yes</reject-high-stddev> <!-- compare individual standard deviation? -->
                <stddev-max-diff>3.0</stddev-max-diff> <!-- max. diff. of indiv. standard deviation (in sigma of the entire RB) -->
            </stack>
        </flat>


        <!-- Option to create a bad pixel mask from the master flat field -->
        <!-- ============================================================ -->
        <bad-pixel>
            <bad-pixel-threshold>5.0</bad-pixel-threshold>  <!-- threshold, in sigma of the pixel intensity distribution -->
            <bad-pixel-dilate-radius>2.0</bad-pixel-dilate-radius>  <!-- radius of the dilation disk in pixels -->
        </bad-pixel>

    </stack-instrumental-calibration-option>


    <!-- ============================================================ -->
    <!-- COSMIC RAY REJECTION -->
    <!-- ============================================================ -->
    <cosmic-ray-detection>
        <laplacian-to-noise>4.5</laplacian-to-noise> <!-- laplacian-to-noise limit -->
        <minimum-contrast>3.0</minimum-contrast> <!-- minimum contrast between laplacian and fine structure -->
        <sigclip>8.0</sigclip>
        <sigfrac>0.5</sigfrac> <!-- fractional detection limit for neighbouring pixels -->
        <objlim>1.0</objlim>
        <niter>4</niter> <!-- number of iterations -->
    </cosmic-ray-detection>


    <!-- ============================================================ -->
    <!-- DISTORTION CORRECTION -->
    <!-- ============================================================ -->
    <distortion>
        <distortion-grid-sampling>5</distortion-grid-sampling> <!-- grid sampling in pixels -->
        <distortion-poly-order>3</distortion-poly-order> <!-- maximum order of the polynomial fit -->
        <distortion-resampling-order>3</distortion-resampling-order> <!-- resampling mode -->
        <distortion-ransac-min-samples>50</distortion-ransac-min-samples> <!-- minimum number of data points to fit a model in the ransack call -->
        <distortion-ransac-threshold>1.0</distortion-ransac-threshold> <!-- maximum threshold in the ransack call -->
        <distortion-ransac-max-trials>100</distortion-ransac-max-trials> <!-- maximum number of trials in the ransack call -->
    </distortion>

    <!-- ============================================================ -->
    <!-- SPECTRUM DETECTION -->
    <!-- ============================================================ -->
    <!-- units can be 'ADU' or 'sigmas' -->
    <spectrum-detection>
        <detection-threshold>20.0</detection-threshold>
        <detection-threshold-unit>sigma</detection-threshold-unit>
    </spectrum-detection>

    <!-- ============================================================ -->
    <!-- SPECTRUM EXTRACTION -->
    <!-- ============================================================ -->
    <!-- units can be 'FWHM' or 'pixels' -->
    <spectrum-extraction>
        <aperture-radius>3.0</aperture-radius>
        <aperture-radius-unit>FWHM</aperture-radius-unit>
        <sky-inner-radius>5.0</sky-inner-radius>
        <sky-inner-radius-unit>FWHM</sky-inner-radius-unit>
        <sky-outer-radius>7.0</sky-outer-radius>
        <sky-outer-radius-unit>FWHM</sky-outer-radius-unit>
    </spectrum-extraction>


    <!-- ============================================================ -->
    <!-- SPECTRUM NORMALIZATION / NOT IMPLEMENTED YET -->
    <!-- ============================================================ -->
    <spectrum-normalization>
        <void></void>
    </spectrum-normalization>

    <!-- ============================================================ -->
    <!-- DEFINITIONS for internal xinclude                            -->
    <!-- ============================================================ -->
    <defs>
      <!-- ============================================================ -->
        <def name="earth-location-gtc">
            <latitude>28.7566</latitude>
            <longitude>-17.8919</longitude>
        </def>
      <!-- ============================================================ -->

    </defs>
</spextractor>
